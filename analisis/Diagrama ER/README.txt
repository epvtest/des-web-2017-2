Diagrama ER Inicial: Es el diagrama inicial.

Diagrama ER Final: Es el diagrama propuesto por el estudiante.

Observaciones:

- Los campos que empiecen con "lt_" se relacionan con tablas de listas tipo.
- Los campos que empiecen con "fec_" son datos de tipo fecha.
- Los campos que empiecen con "ind_" son datos de tipo boolean.

Estructura com�n de las tablas tipo:
- Id INTEGER: identificador �nico del registro.
- C�digo VARCHAR(50): c�digo �nico del registro.
- Nombre VARCHAR(50): nombre del registro.
- Descripci�n VARCHAR(1000): descripci�n de lo que representa el registro.
- Prioridad INT: orden en la lista.