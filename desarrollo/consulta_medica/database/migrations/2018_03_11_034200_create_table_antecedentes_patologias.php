<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAntecedentesPatologias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('antecedentes_patologias', function (Blueprint $table) {
            $table->increments('id');
            $table->string('antecedentes_madre', 200)->nullable();
            $table->string('antecedentes_padre', 200)->nullable();
            $table->string('antecedentes_abuelos', 200)->nullable();
            $table->string('otros', 200)->nullable();
            $table->integer('id_recien_nacido')->unsigned();
            $table->timestamps();
            $table->foreign('id_recien_nacido')->references('id')->on('recien_nacido');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('antecedentes_patologias', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('antecedentes_patologias');
    }
}
