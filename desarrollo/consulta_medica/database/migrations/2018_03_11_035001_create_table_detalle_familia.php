<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetalleFamilia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_familia', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lt_programa_social')->unsigned();
            $table->string('otro_programa_social', 50)->nullable();
            $table->string('distrito_procedencia', 30);
            $table->integer('lt_permanencia')->unsigned();
            $table->string('observaciones', 1000)->nullable();
            $table->integer('id_ficha_familiar')->unsigned();
            $table->timestamps();
            $table->foreign('lt_programa_social')->references('id')->on('lt_programa_social');
            $table->foreign('lt_permanencia')->references('id')->on('lt_permanencia');
            $table->foreign('id_ficha_familiar')->references('id')->on('ficha_familiar');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_familia', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('detalle_familia');
    }
}
