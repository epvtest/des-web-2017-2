<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLtOcupacionJefeFamilia extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lt_ocupacion_jefe_familia', function (Blueprint $table) {
            $table->increments('id');
            $table->string('codigo', 50)->unique();
            $table->string('nombre', 50);
            $table->string('descripcion', 1000);
            $table->integer('prioridad');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lt_ocupacion_jefe_familia', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('lt_ocupacion_jefe_familia');
    }
}
