<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableIndicadorRiesgo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('indicador_riesgo', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('ind_hijos_sin_partida_nacimiento');
            $table->boolean('ind_ninhos_desercion_escolar');
            $table->boolean('ind_lactancia_exclusiva');
            $table->boolean('ind_planificacion_familiar');
            $table->boolean('ind_alimentacion_comp_deficiente');
            $table->boolean('ind_papanicolau');
            $table->boolean('ind_diabetes_hipertension');
            $table->boolean('ind_dependencia_funcional');
            $table->boolean('ind_gestante');
            $table->boolean('ind_parto_domiciliario');
            $table->boolean('ind_vacunas_completas');
            $table->boolean('ind_leishmaniasis');
            $table->boolean('ind_chagas');
            $table->boolean('ind_estres');
            $table->boolean('ind_depresion');
            $table->boolean('ind_abandono');
            $table->boolean('ind_violencia_familiar');
            $table->boolean('ind_discapacidad_evidentes');
            $table->boolean('ind_madre_adolescente');
            $table->boolean('ind_desnutricion');
            $table->boolean('ind_tuberculosis');
            $table->boolean('ind_esquizofrenia');
            $table->boolean('ind_cancer');
            $table->boolean('ind_alcoholismo_drogadiccion');
            $table->boolean('ind_conducta_sexual_riesgo');
            $table->boolean('ind_delincuencia_pandillaje');
            $table->integer('id_ficha_familiar')->unsigned();
            $table->timestamps();
            $table->foreign('id_ficha_familiar')->references('id')->on('ficha_familiar');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('indicador_riesgo', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('indicador_riesgo');
    }
}
