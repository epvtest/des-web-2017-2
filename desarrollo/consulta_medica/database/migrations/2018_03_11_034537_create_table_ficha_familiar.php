<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFichaFamiliar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ficha_familiar', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('numero');
            $table->boolean('ind_visita_domiciliaria');
            $table->date('fec_verificacion');
            $table->integer('id_trabajador')->unsigned();
            $table->string('observaciones', 1000)->nullable();
            $table->integer('id_establecimiento_salud')->unsigned();
            $table->timestamps();
            $table->foreign('id_trabajador')->references('id')->on('usuario');
            $table->foreign('id_establecimiento_salud')->references('id')->on('establecimiento_salud');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ficha_familiar', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('ficha_familiar');
    }
}
