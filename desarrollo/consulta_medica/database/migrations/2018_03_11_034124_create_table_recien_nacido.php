<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRecienNacido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recien_nacido', function (Blueprint $table) {
            $table->increments('id');
            $table->string('aspecto_general', 100);
            $table->string('desarrollo', 50)->nullable();
            $table->string('signos_vitales', 50)->nullable();
            $table->string('piel', 50)->nullable();
            $table->string('cabeza', 50)->nullable();
            $table->string('boca', 50)->nullable();
            $table->string('nariz', 50)->nullable();
            $table->string('ojos', 50)->nullable();
            $table->string('oidos', 50)->nullable();
            $table->string('cabellos', 50)->nullable();
            $table->string('torax', 50)->nullable();
            $table->string('dorso', 50)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recien_nacido', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('recien_nacido');
    }
}
