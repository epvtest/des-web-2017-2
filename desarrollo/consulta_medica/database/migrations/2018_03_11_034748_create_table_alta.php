<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAlta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alta', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lt_condicion_alta')->unsigned();
            $table->integer('lt_condicion_paciente')->unsigned();
            $table->integer('lt_destino_paciente')->unsigned();
            $table->integer('id_establecimiento_destino')->unsigned();
            $table->string('servicio_transferir', 100)->nullable();
            $table->integer('numero_cama');
            $table->date('fecha');
            $table->timeTz('hora');
            $table->integer('id_historia_clinica')->unsigned();
            $table->timestamps();
            $table->foreign('lt_condicion_alta')->references('id')->on('lt_condicion_alta');
            $table->foreign('lt_condicion_paciente')->references('id')->on('lt_condicion_paciente');
            $table->foreign('lt_destino_paciente')->references('id')->on('lt_destino_paciente');
            $table->foreign('id_establecimiento_destino')->references('id')->on('establecimiento_salud');
            $table->foreign('id_historia_clinica')->references('id')->on('historia_clinica');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('alta', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('alta');
    }
}
