<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTratamiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tratamiento', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 200);
            $table->integer('id_consulta')->unsigned();
            $table->timestamps();
            $table->foreign('id_consulta')->references('id')->on('consulta');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tratamiento', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('tratamiento');
    }
}
