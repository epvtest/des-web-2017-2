<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMedico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medico', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_cmp', 6);
            $table->date('fec_emision_licencia');
            $table->boolean('jubilado');
            $table->integer('id_persona')->unsigned();
            $table->timestamps();
            $table->foreign('id_persona')->references('id')->on('persona');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('medico', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('medico');
    }
}
