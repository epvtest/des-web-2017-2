<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEducacionFamiliar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('educacion_familiar', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fec_nutricion_saludable')->nullable();
            $table->date('fec_ambiente_saludable')->nullable();
            $table->date('fec_cultura_paz')->nullable();
            $table->date('fec_pautas_crianza')->nullable();
            $table->date('fec_habilidades_vida')->nullable();
            $table->date('fec_salud_sexual')->nullable();
            $table->date('fec_seguridad_vial')->nullable();
            $table->date('fec_prev_enf_preval')->nullable();
            $table->integer('id_ficha_familiar')->unsigned()->nullable();
            $table->timestamps();
            $table->foreign('id_ficha_familiar')->references('id')->on('ficha_familiar');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('educacion_familiar', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('educacion_familiar');
    }
}
