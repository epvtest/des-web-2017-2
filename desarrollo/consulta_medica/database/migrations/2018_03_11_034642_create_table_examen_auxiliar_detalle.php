<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExamenAuxiliarDetalle extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examen_auxiliar_detalle', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lt_clave')->unsigned();
            $table->string('valor', 50);
            $table->integer('id_examen_auxiliar')->unsigned();
            $table->timestamps();
            $table->foreign('lt_clave')->references('id')->on('lt_clave');
            $table->foreign('id_examen_auxiliar')->references('id')->on('examen_auxiliar');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('examen_auxiliar_detalle', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('examen_auxiliar_detalle');
    }
}
