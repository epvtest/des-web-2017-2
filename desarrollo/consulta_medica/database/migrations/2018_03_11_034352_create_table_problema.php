<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProblema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('problema', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lt_tipo_problema')->unsigned();
            $table->date('fecha');
            $table->boolean('ind_activo');
            $table->string('observacion', 1000)->nullable();
            $table->integer('id_paciente')->unsigned();
            $table->timestamps();
            $table->foreign('lt_tipo_problema')->references('id')->on('lt_tipo_problema');
            $table->foreign('id_paciente')->references('id')->on('paciente');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('problema', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('problema');
    }
}
