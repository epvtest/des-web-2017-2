<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDiagnostico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diagnostico', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lt_tipo_diagnostico')->unsigned();
            $table->string('descripcion', 200);
            $table->string('codigo_cie_10', 4);
            $table->integer('id_consulta')->unsigned();
            $table->timestamps();
            $table->foreign('lt_tipo_diagnostico')->references('id')->on('lt_tipo_diagnostico');
            $table->foreign('id_consulta')->references('id')->on('consulta');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('diagnostico', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('diagnostico');
    }
}
