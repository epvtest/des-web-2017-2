<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEstablecimientoSalud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('establecimiento_salud', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre', 100);
            $table->integer('lt_categoria')->unsigned();
            $table->integer('lt_red_salud')->unsigned();
            $table->timestamps();
            $table->foreign('lt_categoria')->references('id')->on('lt_categoria');
            $table->foreign('lt_red_salud')->references('id')->on('lt_red_salud');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('establecimiento_salud', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('establecimiento_salud');
    }
}
