<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTenenciaBienes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tenencia_bienes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lt_combustible_cocina')->unsigned();
            $table->boolean('ind_radio');
            $table->boolean('ind_televisor');
            $table->boolean('ind_refrigeradora');
            $table->boolean('ind_cocina');
            $table->boolean('ind_telefono_fijo');
            $table->boolean('ind_telefono_celular');
            $table->boolean('ind_lavadora');
            $table->boolean('ind_computadora');
            $table->boolean('ind_horno_microondas');
            $table->boolean('ind_suministro_electrico');
            $table->string('suministro_electrico', 15)->nullable();
            $table->decimal('consumo_electrico', 6, 2)->nullable();
            $table->integer('mes_periodo_consumo')->nullable();
            $table->integer('anho_periodo_consumo')->nullable();
            $table->integer('id_vivienda')->unsigned();
            $table->timestamps();
            $table->foreign('lt_combustible_cocina')->references('id')->on('lt_combustible_cocina');
            $table->foreign('id_vivienda')->references('id')->on('vivienda');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tenencia_bienes', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('tenencia_bienes');
    }
}
