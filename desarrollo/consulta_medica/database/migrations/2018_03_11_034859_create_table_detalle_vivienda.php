<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetalleVivienda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_vivienda', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lt_material_paredes')->unsigned();
            $table->string('otro_material_paredes', 50)->nullable();
            $table->integer('lt_material_pisos')->unsigned();
            $table->string('otro_material_pisos', 50)->nullable();
            $table->integer('lt_material_techo')->unsigned();
            $table->string('otro_material_techo', 50)->nullable();
            $table->integer('lt_abastecimiento_agua')->unsigned();
            $table->integer('lt_servicio_sanitario')->unsigned();
            $table->integer('habitaciones');
            $table->integer('id_vivienda')->unsigned();
            $table->timestamps();
            $table->foreign('lt_material_paredes')->references('id')->on('lt_material_paredes');
            $table->foreign('lt_material_pisos')->references('id')->on('lt_material_pisos');
            $table->foreign('lt_material_techo')->references('id')->on('lt_material_techo');
            $table->foreign('lt_abastecimiento_agua')->references('id')->on('lt_abastecimiento_agua');
            $table->foreign('lt_servicio_sanitario')->references('id')->on('lt_servicio_sanitario');
            $table->foreign('id_vivienda')->references('id')->on('vivienda');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_vivienda', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('detalle_vivienda');
    }
}
