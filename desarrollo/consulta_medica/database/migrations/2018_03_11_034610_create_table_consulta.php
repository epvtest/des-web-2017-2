<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableConsulta extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consulta', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fec_consulta');
            $table->string('motivo', 100);
            $table->string('tiempo_enfermedad', 50);
            $table->string('apetito', 100)->nullable();
            $table->string('sed', 100)->nullable();
            $table->string('suenho', 100)->nullable();
            $table->string('estado_animo', 100)->nullable();
            $table->string('orina', 100)->nullable();
            $table->string('deposiciones', 100)->nullable();
            $table->date('fec_proxima_cita');
            $table->string('observaciones', 1000)->nullable();
            $table->integer('id_medico')->unsigned();
            $table->integer('id_historia_clinica')->unsigned();
            $table->timestamps();
            $table->foreign('id_medico')->references('id')->on('medico');
            $table->foreign('id_historia_clinica')->references('id')->on('historia_clinica');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consulta', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('consulta');
    }
}
