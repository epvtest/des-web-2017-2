<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGestacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gestacion', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('ind_complicado');
            $table->date('fec_concepcion');
            $table->integer('duracion_embarazo');
            $table->string('patologias', 200)->nullable();
            $table->integer('id_recien_nacido')->unsigned();
            $table->foreign('id_recien_nacido')->references('id')->on('recien_nacido');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gestacion', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('gestacion');
    }
}
