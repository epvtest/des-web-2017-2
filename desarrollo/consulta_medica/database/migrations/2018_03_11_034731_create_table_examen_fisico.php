<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExamenFisico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examen_fisico', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('temperatura', 4, 2);
            $table->string('presion_arterial', 10);
            $table->integer('frecuencia_cardiaca');
            $table->integer('frecuencia_respiratoria');
            $table->decimal('talla', 3, 2);
            $table->decimal('peso', 5, 2);
            $table->decimal('indice_masa_corporal', 4, 2);
            $table->integer('id_consulta')->unsigned();
            $table->timestamps();
            $table->foreign('id_consulta')->references('id')->on('consulta');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('examen_fisico', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('examen_fisico');
    }
}
