<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaciente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paciente', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_seguro', 30);
            $table->integer('id_persona')->unsigned();
            $table->integer('lt_seguro')->unsigned();
            $table->timestamps();
            $table->foreign('id_persona')->references('id')->on('persona');
            $table->foreign('lt_seguro')->references('id')->on('lt_seguro');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('paciente', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('paciente');
    }
}
