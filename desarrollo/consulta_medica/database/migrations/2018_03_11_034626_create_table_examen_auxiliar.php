<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExamenAuxiliar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('examen_auxiliar', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion', 200);
            $table->date('fec_emision');
            $table->integer('id_medico')->unsigned();
            $table->integer('id_consulta')->unsigned();
            $table->timestamps();
            $table->foreign('id_medico')->references('id')->on('medico');
            $table->foreign('id_consulta')->references('id')->on('consulta');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('examen_auxiliar', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('examen_auxiliar');
    }
}
