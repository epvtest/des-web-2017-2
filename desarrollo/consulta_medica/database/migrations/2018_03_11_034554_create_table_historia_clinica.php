<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHistoriaClinica extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('historia_clinica', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_hc', 11);
            $table->boolean('ind_emergencia');
            $table->string('motivo_emergencia', 100)->nullable();
            $table->integer('lt_causa_ext_emergencia')->unsigned();
            $table->boolean('ind_referencia');
            $table->date('fec_referencia')->nullable();
            $table->string('motivo_referencia', 100)->nullable();
            $table->string('acompanhante', 100);
            $table->integer('id_ficha_familiar')->unsigned();
            $table->integer('id_paciente')->unsigned();
            $table->timestamps();
            $table->foreign('lt_causa_ext_emergencia')->references('id')->on('lt_causa_ext_emergencia');
            $table->foreign('id_ficha_familiar')->references('id')->on('ficha_familiar');
            $table->foreign('id_paciente')->references('id')->on('paciente');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('historia_clinica', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('historia_clinica');
    }
}
