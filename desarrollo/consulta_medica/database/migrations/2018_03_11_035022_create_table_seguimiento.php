<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSeguimiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seguimiento', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fec_visita');
            $table->string('nombre_visitador', 100);
            $table->string('problemas_identificados', 255);
            $table->string('recomendaciones', 255)->nullable();
            $table->boolean('ind_cumple_recomendaciones');
            $table->integer('id_ficha_familiar')->unsigned();
            $table->timestamps();
            $table->foreign('id_ficha_familiar')->references('id')->on('ficha_familiar');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('seguimiento', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('seguimiento');
    }
}
