<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetalleSocioeconomico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalle_socioeconomico', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('anho');
            $table->integer('lt_estado_civil')->unsigned();
            $table->integer('lt_grupo_familiar')->unsigned();
            $table->integer('lt_tenencia_vivienda')->unsigned();
            $table->integer('lt_consumo_agua')->unsigned();
            $table->integer('lt_eliminacion_excretas')->unsigned();
            $table->integer('lt_energia_electrica')->unsigned();
            $table->integer('lt_nivel_instruccion_madre')->unsigned();
            $table->integer('lt_ocupacion_jefe_familia')->unsigned();
            $table->integer('lt_ingreso_familiar')->unsigned();
            $table->integer('lt_personas_dormitorio')->unsigned();
            $table->integer('id_ficha_familiar')->unsigned();
            $table->timestamps();
            $table->foreign('lt_estado_civil')->references('id')->on('lt_estado_civil');
            $table->foreign('lt_grupo_familiar')->references('id')->on('lt_grupo_familiar');
            $table->foreign('lt_tenencia_vivienda')->references('id')->on('lt_tenencia_vivienda');
            $table->foreign('lt_consumo_agua')->references('id')->on('lt_consumo_agua');
            $table->foreign('lt_eliminacion_excretas')->references('id')->on('lt_eliminacion_excretas');
            $table->foreign('lt_energia_electrica')->references('id')->on('lt_energia_electrica');
            $table->foreign('lt_nivel_instruccion_madre')->references('id')->on('lt_nivel_instruccion_madre');
            $table->foreign('lt_ocupacion_jefe_familia')->references('id')->on('lt_ocupacion_jefe_familia');
            $table->foreign('lt_ingreso_familiar')->references('id')->on('lt_ingreso_familiar');
            $table->foreign('lt_personas_dormitorio')->references('id')->on('lt_personas_dormitorio');
            $table->foreign('id_ficha_familiar')->references('id')->on('ficha_familiar');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detalle_socioeconomico', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('detalle_socioeconomico');
    }
}
