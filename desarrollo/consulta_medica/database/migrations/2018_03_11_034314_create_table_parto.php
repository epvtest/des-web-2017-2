<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableParto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parto', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('ind_eutocico');
            $table->string('duracion_periodo_expulsivo', 50);
            $table->integer('lt_distocico')->unsigned();
            $table->string('solucion_distocico', 20)->nullable();
            $table->string('pre_parto', 100);
            $table->integer('lt_anestesia')->unsigned();
            $table->string('duracion_parto', 50);
            $table->string('signos_sufrimiento_fetal', 200)->nullable();
            $table->string('intervencion_medicacion', 200)->nullable();
            $table->string('observacion', 1000)->nullable();
            $table->integer('id_obstetriz')->unsigned();
            $table->integer('id_medico')->unsigned();
            $table->integer('id_recien_nacido')->unsigned();
            $table->timestamps();
            $table->foreign('lt_distocico')->references('id')->on('lt_distocico');
            $table->foreign('lt_anestesia')->references('id')->on('lt_anestesia');
            $table->foreign('id_obstetriz')->references('id')->on('medico');
            $table->foreign('id_medico')->references('id')->on('medico');
            $table->foreign('id_recien_nacido')->references('id')->on('recien_nacido');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('parto', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('parto');
    }
}
