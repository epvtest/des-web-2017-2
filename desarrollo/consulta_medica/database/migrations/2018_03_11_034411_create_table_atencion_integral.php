<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAtencionIntegral extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('atencion_integral', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lt_tipo_atencion')->unsigned();
            $table->string('descripcion', 1000);
            $table->date('fecha');
            $table->string('lugar', 100);
            $table->integer('id_paciente')->unsigned();
            $table->timestamps();
            $table->foreign('lt_tipo_atencion')->references('id')->on('lt_tipo_atencion');
            $table->foreign('id_paciente')->references('id')->on('paciente');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('atencion_integral', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('atencion_integral');
    }
}
