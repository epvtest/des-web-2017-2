<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePersona extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persona', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lt_tipo_identificacion')->unsigned();
            $table->string('numero_identificacion', 20);
            $table->string('nombre', 30);
            $table->string('ape_paterno', 30);
            $table->string('ape_materno', 30)->nullable();
            $table->string('ape_casada', 30)->nullable();
            $table->integer('lt_sexo')->unsigned();
            $table->integer('lt_estado_civil')->unsigned();
            $table->date('fec_nacimiento');
            $table->string('telefono', 15)->nullable();
            $table->integer('lt_ocupacion')->unsigned();
            $table->integer('lt_grupo_sanguineo')->unsigned();
            $table->integer('lt_rh')->unsigned();
            $table->integer('lt_parentesco_jefe_hogar')->unsigned();
            $table->boolean('ind_victima_violacion_ddhh');
            $table->boolean('ind_provision_anticonceptivos');
            $table->integer('lt_nivel_educativo')->unsigned();
            $table->string('centro_educativo', 100)->nullable();
            $table->integer('ingresos_mensuales');
            $table->foreign('lt_tipo_identificacion')->references('id')->on('lt_tipo_identificacion');
            $table->foreign('lt_sexo')->references('id')->on('lt_sexo');
            $table->foreign('lt_estado_civil')->references('id')->on('lt_estado_civil');
            $table->foreign('lt_ocupacion')->references('id')->on('lt_ocupacion');
            $table->foreign('lt_grupo_sanguineo')->references('id')->on('lt_grupo_sanguineo');
            $table->foreign('lt_rh')->references('id')->on('lt_rh');
            $table->foreign('lt_parentesco_jefe_hogar')->references('id')->on('lt_parentesco_jefe_hogar');
            $table->foreign('lt_nivel_educativo')->references('id')->on('lt_nivel_educativo');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('persona', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('persona');
    }
}
