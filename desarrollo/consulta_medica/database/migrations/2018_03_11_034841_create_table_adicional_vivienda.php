<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdicionalVivienda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adicional_vivienda', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('ind_perros');
            $table->boolean('ind_gatos');
            $table->boolean('ind_vacas');
            $table->boolean('ind_cabras');
            $table->boolean('ind_carnero');
            $table->boolean('ind_cerdo');
            $table->boolean('ind_aves');
            $table->boolean('ind_cuyes');
            $table->boolean('ind_chirimachas');
            $table->boolean('ind_caballos');
            $table->boolean('ind_burros');
            $table->boolean('ind_otros_animales');
            $table->integer('lt_disposicion_basura')->unsigned();
            $table->integer('id_vivienda')->unsigned();
            $table->timestamps();
            $table->foreign('lt_disposicion_basura')->references('id')->on('lt_disposicion_basura');
            $table->foreign('id_vivienda')->references('id')->on('vivienda');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('adicional_vivienda', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('adicional_vivienda');
    }
}
