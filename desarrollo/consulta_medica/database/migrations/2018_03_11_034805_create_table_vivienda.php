<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVivienda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vivienda', function (Blueprint $table) {
            $table->increments('id');
            $table->string('departamento', 20);
            $table->string('provincia', 30);
            $table->string('distrito', 30);
            $table->string('ubigeo', 6);
            $table->string('centro_poblado', 40);
            $table->string('direccion', 100);
            $table->integer('lt_ambito')->unsigned();
            $table->string('telefono', 20)->nullable();
            $table->integer('lt_categoria_socioecon')->unsigned();
            $table->integer('lt_riesgo_familiar')->unsigned();
            $table->integer('id_ficha_familiar')->unsigned();
            $table->timestamps();
            $table->foreign('lt_ambito')->references('id')->on('lt_ambito');
            $table->foreign('lt_categoria_socioecon')->references('id')->on('lt_categoria_socioeconomica');
            $table->foreign('lt_riesgo_familiar')->references('id')->on('lt_riesgo_familiar');
            $table->foreign('id_ficha_familiar')->references('id')->on('ficha_familiar');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vivienda', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('vivienda');
    }
}
