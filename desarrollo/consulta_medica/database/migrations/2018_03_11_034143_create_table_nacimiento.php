<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNacimiento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nacimiento', function (Blueprint $table) {
            $table->increments('id');
            $table->timeTz('hora');
            $table->decimal('peso', 5, 3);
            $table->decimal('talla', 3, 2);
            $table->integer('cuna');
            $table->date('fec_alta');
            $table->integer('id_recien_nacido')->unsigned();
            $table->timestamps();
            $table->foreign('id_recien_nacido')->references('id')->on('recien_nacido');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nacimiento', function(Blueprint $table) {
            $table->dropSoftDeletes();
        });
        Schema::dropIfExists('nacimiento');
    }
}
