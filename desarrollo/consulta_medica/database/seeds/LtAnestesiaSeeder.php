<?php

use Illuminate\Database\Seeder;

class LtAnestesiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_anestesia')->insert([
		    array(
			    'codigo' => 'local',
			    'nombre' => 'Local',
			    'descripcion' => 'Entumece una pequeña parte del cuerpo. El paciente permanece despierto y alerta.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'regional',
			    'nombre' => 'Regional',
			    'descripcion' => 'Bloquea el dolor en un área del cuerpo, como un brazo o una pierna.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'general',
			    'nombre' => 'General',
			    'descripcion' => 'El paciente se torna inconsciente, no siente dolor y no recuerdan la intervención quirúrgica.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
