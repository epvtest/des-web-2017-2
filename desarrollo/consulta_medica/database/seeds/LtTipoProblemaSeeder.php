<?php

use Illuminate\Database\Seeder;

class LtTipoProblemaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_tipo_problema')->insert([
		    array(
			    'codigo' => 'agudo',
			    'nombre' => 'Enfermedad aguda',
			    'descripcion' => 'Aquella enfermedad que se presenta de manera súbita, y puede desaparecer de la misma forma.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'cronico',
			    'nombre' => 'Enfermedad crónica',
			    'descripcion' => 'Afecciones de desarrollo lento y que se mantienen a lo largo del tiempo.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
