<?php

use Illuminate\Database\Seeder;

class LtAmbitoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_ambito')->insert([
		    array(
			    'codigo' => 'urbano',
			    'nombre' => 'Urbano',
			    'descripcion' => 'Ámbito urbano',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'marginal',
			    'nombre' => 'Urbano Marginal',
			    'descripcion' => 'Ámbito urbano marginal',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'rural',
			    'nombre' => 'Rural',
			    'descripcion' => 'Ámbito rural',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
