<?php

use Illuminate\Database\Seeder;

class LtNivelInstruccionMadreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_nivel_instruccion_madre')->insert([
		    array(
			    'codigo' => 'ninguna',
			    'nombre' => 'Ninguna',
			    'descripcion' => 'Ninguna',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'primaria',
			    'nombre' => 'Primaria',
			    'descripcion' => 'Primaria',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'secundaria',
			    'nombre' => 'Secundaria',
			    'descripcion' => 'Secundaria',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'tecnica',
			    'nombre' => 'Técnica',
			    'descripcion' => 'Técnica',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'profesional',
			    'nombre' => 'Profesional',
			    'descripcion' => 'Profesional',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
