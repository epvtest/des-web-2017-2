<?php

use Illuminate\Database\Seeder;

class LtSexoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_sexo')->insert([
		    array(
			    'codigo' => 'masculino',
			    'nombre' => 'Masculino',
			    'descripcion' => 'Masculino.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'femenino',
			    'nombre' => 'Femenino',
			    'descripcion' => 'Femenino.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
