<?php

use Illuminate\Database\Seeder;

class LtServicioSanitarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_servicio_sanitario')->insert([
		    array(
			    'codigo' => 'redpublicaint',
			    'nombre' => 'Red pública dentro de la vivienda',
			    'descripcion' => 'Red pública dentro de la vivienda',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'redpublicaext',
			    'nombre' => 'Red pública fuera de la vivienda',
			    'descripcion' => 'Red pública fuera de la vivienda',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'pozoseptico',
			    'nombre' => 'Pozo séptico',
			    'descripcion' => 'Pozo séptico',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'pozociegonegro',
			    'nombre' => 'Pozo ciego o negro',
			    'descripcion' => 'Pozo ciego o negro',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'notieneservicio',
			    'nombre' => 'No tiene servicio higiénico',
			    'descripcion' => 'No tiene servicio higiénico',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
