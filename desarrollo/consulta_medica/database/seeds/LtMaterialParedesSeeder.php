<?php

use Illuminate\Database\Seeder;

class LtMaterialParedesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_material_paredes')->insert([
		    array(
			    'codigo' => 'ladrillocemento',
			    'nombre' => 'Ladrillo o bloque de cemento',
			    'descripcion' => 'Ladrillo o bloque de cemento',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'adobetapia',
			    'nombre' => 'Adobe o tapia',
			    'descripcion' => 'Adobe o tapia',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'quincha',
			    'nombre' => 'Quincha (caña con barro)',
			    'descripcion' => 'Quincha (caña con barro)',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'piedrabarro',
			    'nombre' => 'Piedra con barro',
			    'descripcion' => 'Piedra con barro',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'madera',
			    'nombre' => 'Madera',
			    'descripcion' => 'Madera',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'estera',
			    'nombre' => 'Estera',
			    'descripcion' => 'Estera',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'otro',
			    'nombre' => 'Otro',
			    'descripcion' => 'Otro',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
