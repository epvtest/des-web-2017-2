<?php

use Illuminate\Database\Seeder;

class LtPermanenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_permanencia')->insert([
		    array(
			    'codigo' => 'permanencia',
			    'nombre' => 'Tiempo de permanencia',
			    'descripcion' => 'Tiempo de permanencia',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'transeunte',
			    'nombre' => 'Transeunte',
			    'descripcion' => 'Transeunte',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'flotante',
			    'nombre' => 'Flotante',
			    'descripcion' => 'Flotante',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'migrante',
			    'nombre' => 'Migrante',
			    'descripcion' => 'Migrante',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'residente',
			    'nombre' => 'Residente',
			    'descripcion' => 'Residente',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
