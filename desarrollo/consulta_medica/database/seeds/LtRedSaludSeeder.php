<?php

use Illuminate\Database\Seeder;

class LtRedSaludSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_red_salud')->insert([
		    array(
			    'codigo' => 'dirislimanorte',
			    'nombre' => 'Dirección de Red Integrada de Salud - Lima Norte',
			    'descripcion' => 'Dirección de Red Integrada de Salud - Lima Norte',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'dirislimacentro',
			    'nombre' => 'Dirección de Red Integrada de Salud - Lima Centro',
			    'descripcion' => 'Dirección de Red Integrada de Salud - Lima Centro',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'dirislimasur',
			    'nombre' => 'Dirección de Red Integrada de Salud - Lima Sur',
			    'descripcion' => 'Dirección de Red Integrada de Salud - Lima Sur',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'dirislimaeste',
			    'nombre' => 'Dirección de Red Integrada de Salud - Lima Este',
			    'descripcion' => 'Dirección de Red Integrada de Salud - Lima Este',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresaamazonas',
			    'nombre' => 'Dirección Regional de Salud de Amazonas',
			    'descripcion' => 'Dirección Regional de Salud - Amazonas',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'disuresabagua',
			    'nombre' => 'Dirección Sub Regional de Salud Bagua',
			    'descripcion' => 'Dirección Sub Regional de Salud Bagua',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresaancash',
			    'nombre' => 'Dirección Regional de Salud de Ancash',
			    'descripcion' => 'Dirección Regional de Salud de Ancash',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresaapurimacabancay',
			    'nombre' => 'Dirección Regional de Salud de Apurimac',
			    'descripcion' => 'Dirección Regional de Salud de Apurimac',
			    'prioridad' => 80,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresaapurimacandahuaylas',
			    'nombre' => 'Dirección Regional de Salud Apurimac 2 Andahuaylas',
			    'descripcion' => 'Dirección Regional de Salud Apurimac 2 Andahuaylas',
			    'prioridad' => 90,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresaarequipa',
			    'nombre' => 'Dirección Regional de Salud de Arequipa',
			    'descripcion' => 'Dirección Regional de Salud de Arequipa',
			    'prioridad' => 100,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresaayacucho',
			    'nombre' => 'Dirección Regional de Salud de Ayacucho',
			    'descripcion' => 'Dirección Regional de Salud de Ayacucho',
			    'prioridad' => 110,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresacallao',
			    'nombre' => 'Dirección Regional de Salud de Callao',
			    'descripcion' => 'Dirección Regional de Salud de Callao',
			    'prioridad' => 120,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresacajamarca',
			    'nombre' => 'Dirección Regional de Salud de Cajamarca',
			    'descripcion' => 'Dirección Regional de Salud de Cajamarca',
			    'prioridad' => 130,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'disachota',
			    'nombre' => 'Dirección de Salud Chota',
			    'descripcion' => 'Dirección de Salud Chota',
			    'prioridad' => 140,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'disacutervo',
			    'nombre' => 'Dirección de Salud Cutervo',
			    'descripcion' => 'Dirección de Salud Cutervo',
			    'prioridad' => 150,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'disajaen',
			    'nombre' => 'Dirección de Salud Jaén',
			    'descripcion' => 'Dirección de Salud Jaén',
			    'prioridad' => 160,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresacusco',
			    'nombre' => 'Dirección Regional de Salud de Cusco',
			    'descripcion' => 'Dirección Regional de Salud de Cusco',
			    'prioridad' => 170,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresahuancavelica',
			    'nombre' => 'Dirección Regional de Salud de Huancavelica',
			    'descripcion' => 'Dirección Regional de Salud de Huancavelica',
			    'prioridad' => 180,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresahuanuco',
			    'nombre' => 'Dirección Regional de Salud de Huanuco',
			    'descripcion' => 'Dirección Regional de Salud de Huanuco',
			    'prioridad' => 190,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresaica',
			    'nombre' => 'Dirección Regional de Salud de Ica',
			    'descripcion' => 'Dirección Regional de Salud de Ica',
			    'prioridad' => 200,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresajunin',
			    'nombre' => 'Dirección Regional de Salud de Junin',
			    'descripcion' => 'Dirección Regional de Salud de Junin',
			    'prioridad' => 210,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresalalibertad',
			    'nombre' => 'Dirección Regional de Salud de La Libertad',
			    'descripcion' => 'Dirección Regional de Salud de La Libertad',
			    'prioridad' => 220,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresalambayeque',
			    'nombre' => 'Dirección Regional de Salud de Lambayeque',
			    'descripcion' => 'Dirección Regional de Salud de Lambayeque',
			    'prioridad' => 230,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresalima',
			    'nombre' => 'Dirección Regional de Salud de Lima',
			    'descripcion' => 'Dirección Regional de Salud de Lima',
			    'prioridad' => 240,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresaloreto',
			    'nombre' => 'Dirección Regional de Salud de Loreto',
			    'descripcion' => 'Dirección Regional de Salud de Loreto',
			    'prioridad' => 250,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresamadrededios',
			    'nombre' => 'Dirección Regional de Salud de Madre De Dios',
			    'descripcion' => 'Dirección Regional de Salud de Madre De Dios',
			    'prioridad' => 260,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresamoquegua',
			    'nombre' => 'Dirección Regional de Salud de Moquegua',
			    'descripcion' => 'Dirección Regional de Salud de Moquegua',
			    'prioridad' => 270,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresapasco',
			    'nombre' => 'Dirección Regional de Salud de Pasco',
			    'descripcion' => 'Dirección Regional de Salud de Pasco',
			    'prioridad' => 280,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresapiura',
			    'nombre' => 'Dirección Regional de Salud de Piura',
			    'descripcion' => 'Dirección Regional de Salud de Piura',
			    'prioridad' => 290,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'disuresacolonna',
			    'nombre' => 'Dirección Sub Regional de Salud Luciano Castillo',
			    'descripcion' => 'Dirección Sub Regional de Salud Luciano Castillo Colonna',
			    'prioridad' => 300,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresapuno',
			    'nombre' => 'Dirección Regional de Salud de Puno',
			    'descripcion' => 'Dirección Regional de Salud de Puno',
			    'prioridad' => 310,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresasanmartin',
			    'nombre' => 'Dirección Regional de Salud de San Martin',
			    'descripcion' => 'Dirección Regional de Salud de San Martin',
			    'prioridad' => 320,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresatacna',
			    'nombre' => 'Dirección Regional de Salud de Tacna',
			    'descripcion' => 'Dirección Regional de Salud de Tacna',
			    'prioridad' => 330,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresatumbes',
			    'nombre' => 'Dirección Regional de Salud de Tumbes',
			    'descripcion' => 'Dirección Regional de Salud de Tumbes',
			    'prioridad' => 340,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'diresaucayali',
			    'nombre' => 'Dirección Regional de Salud de Ucayali',
			    'descripcion' => 'Dirección Regional de Salud de Ucayali',
			    'prioridad' => 350,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
