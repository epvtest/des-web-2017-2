<?php

use Illuminate\Database\Seeder;

class LtEliminacionExcretasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_eliminacion_excretas')->insert([
		    array(
			    'codigo' => 'airelibre',
			    'nombre' => 'Aire libre',
			    'descripcion' => 'Aire libre',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'acequiacorral',
			    'nombre' => 'Acequia, corral',
			    'descripcion' => 'Acequia, corral',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'letrina',
			    'nombre' => 'Letrina',
			    'descripcion' => 'Letrina',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'banhopublico',
			    'nombre' => 'Baño público',
			    'descripcion' => 'Baño público',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'banhopropio',
			    'nombre' => 'Baño propio',
			    'descripcion' => 'Baño propio',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
