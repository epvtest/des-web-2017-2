<?php

use Illuminate\Database\Seeder;

class LtRhSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_rh')->insert([
		    array(
			    'codigo' => 'positivo',
			    'nombre' => 'Positivo',
			    'descripcion' => 'Factor Rh positivo.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'negativo',
			    'nombre' => 'Negativo',
			    'descripcion' => 'Factor Rh negativo.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
