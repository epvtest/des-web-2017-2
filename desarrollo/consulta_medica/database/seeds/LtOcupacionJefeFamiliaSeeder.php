<?php

use Illuminate\Database\Seeder;

class LtOcupacionJefeFamiliaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_ocupacion_jefe_familia')->insert([
		    array(
			    'codigo' => 'desocupado',
			    'nombre' => 'Desocupado',
			    'descripcion' => 'Desocupado',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'trabajoeventual',
			    'nombre' => 'Trabajo eventual',
			    'descripcion' => 'Trabajo eventual',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'empleadosinseguro',
			    'nombre' => 'Empleado sin seguro',
			    'descripcion' => 'Empleado sin seguro',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'contratadosinseguro',
			    'nombre' => 'Contratado sin seguro',
			    'descripcion' => 'Contratado sin seguro',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'profesionalproductor',
			    'nombre' => 'Profesional o productor',
			    'descripcion' => 'Profesional o productor',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
