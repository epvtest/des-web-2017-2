<?php

use Illuminate\Database\Seeder;

class LtCondicionPacienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_condicion_paciente')->insert([
		    array(
			    'codigo' => 'vivo',
			    'nombre' => 'Vivo',
			    'descripcion' => 'Paciente vivo.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'fallecido',
			    'nombre' => 'Fallecido',
			    'descripcion' => 'Paciente fallecido.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'fuga',
			    'nombre' => 'Fuga',
			    'descripcion' => 'Paciente que se ha fugado del establecimiento de salud.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
