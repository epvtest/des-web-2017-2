<?php

use Illuminate\Database\Seeder;

class LtDestinoPacienteSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_destino_paciente')->insert([
		    array(
			    'codigo' => 'domicilio',
			    'nombre' => 'Domicilio',
			    'descripcion' => 'Significa que el paciente ha sido derivado a su domicilio, ya sea para continuar su tratamiento, o porque éste ha finalizado.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'hospitalizacion',
			    'nombre' => 'Hospitalización',
			    'descripcion' => 'El paciente es transferido al área de hospitalización, una vez superada la emergencia.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'transferencia',
			    'nombre' => 'Transferencia',
			    'descripcion' => 'El paciente es transferido a otro establecimiento de salud, bajo una hoja de referencia.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'ignorado',
			    'nombre' => 'Ignorado',
			    'descripcion' => 'El paciente ha fugado del centro de salud.',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
