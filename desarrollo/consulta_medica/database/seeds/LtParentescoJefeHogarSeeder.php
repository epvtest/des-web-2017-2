<?php

use Illuminate\Database\Seeder;

class LtParentescoJefeHogarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_parentesco_jefe_hogar')->insert([
		    array(
			    'codigo' => 'jefe',
			    'nombre' => 'Jefe',
			    'descripcion' => 'La persona es jefe del hogar.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'conyuge',
			    'nombre' => 'Cónyuge',
			    'descripcion' => 'La persona es cónyuge del jefe de hogar.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'hijo',
			    'nombre' => 'Hijo(a)',
			    'descripcion' => 'La persona es hijo o hija del jefe de hogar.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'nieto',
			    'nombre' => 'Nieto(a)',
			    'descripcion' => 'La persona es nieto o nieta del jefe de hogar.',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'padre',
			    'nombre' => 'Padre/Madre',
			    'descripcion' => 'La persona es padre o madre del jefe de hogar.',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'hermano',
			    'nombre' => 'Hermano(a)',
			    'descripcion' => 'La persona es hermano o hermana del jefe de hogar.',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'yernonuera',
			    'nombre' => 'Yerno / nuera',
			    'descripcion' => 'La persona es yerno o nuera del jefe de hogar.',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'abuelo',
			    'nombre' => 'Abuelo(a)',
			    'descripcion' => 'La persona es abuelo o abuela del jefe de hogar.',
			    'prioridad' => 80,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'suegro',
			    'nombre' => 'Suegro(a)',
			    'descripcion' => 'La persona es suegro o suegra del jefe de hogar.',
			    'prioridad' => 90,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'tio',
			    'nombre' => 'Tío(a)',
			    'descripcion' => 'La persona es tío o tía del jefe de hogar.',
			    'prioridad' => 100,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'sobrino',
			    'nombre' => 'Sobrino(a)',
			    'descripcion' => 'La persona es sobrino o sobrina del jefe de hogar.',
			    'prioridad' => 110,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'primo',
			    'nombre' => 'Primo(a)',
			    'descripcion' => 'La persona es primo o prima del jefe de hogar.',
			    'prioridad' => 120,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'cunhado',
			    'nombre' => 'Cuñado(a)',
			    'descripcion' => 'La persona es cuñado o cuñada del jefe de hogar.',
			    'prioridad' => 130,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'otropariente',
			    'nombre' => 'Otro pariente',
			    'descripcion' => 'La persona es otro pariente en relación al jefe de hogar, cuya denominación no figura en esta lista.',
			    'prioridad' => 140,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'serviciodomestico',
			    'nombre' => 'Servicio doméstico',
			    'descripcion' => 'La persona presta servicio doméstico al hogar.',
			    'prioridad' => 150,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'hijoserviciodomestico',
			    'nombre' => 'Hijo del servicio doméstico',
			    'descripcion' => 'La persona es hijo o hija de quién presta servicio doméstico al hogar.',
			    'prioridad' => 160,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'nopariente',
			    'nombre' => 'No pariente',
			    'descripcion' => 'La persona no tiene parentesco con el jefe de hogar.',
			    'prioridad' => 170,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
