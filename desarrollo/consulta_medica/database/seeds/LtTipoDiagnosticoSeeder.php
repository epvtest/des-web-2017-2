<?php

use Illuminate\Database\Seeder;

class LtTipoDiagnosticoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_tipo_diagnostico')->insert([
		    array(
			    'codigo' => 'p',
			    'nombre' => 'P',
			    'descripcion' => 'P',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'd',
			    'nombre' => 'D',
			    'descripcion' => 'D',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'r',
			    'nombre' => 'R',
			    'descripcion' => 'R',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
