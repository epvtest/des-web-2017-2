<?php

use Illuminate\Database\Seeder;

class LtPersonasDormitorioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_personas_dormitorio')->insert([
		    array(
			    'codigo' => 'seismas',
			    'nombre' => '6 o más miembros',
			    'descripcion' => 'Seis o más miembros',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'cinco',
			    'nombre' => '5 miembros',
			    'descripcion' => 'Cinco miembros',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'cuatro',
			    'nombre' => '4 miembros',
			    'descripcion' => 'Cuatro miembros',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'tres',
			    'nombre' => '3 Miembros',
			    'descripcion' => 'Tres Miembros',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'unodos',
			    'nombre' => '1 o 2 miembros',
			    'descripcion' => 'Uno o dos miembros',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
