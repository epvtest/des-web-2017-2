<?php

use Illuminate\Database\Seeder;

class LtGrupoSanguineoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_grupo_sanguineo')->insert([
		    array(
			    'codigo' => 'a',
			    'nombre' => 'Tipo A',
			    'descripcion' => 'Grupo sanguíneo del tipo A.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'b',
			    'nombre' => 'Tipo B',
			    'descripcion' => 'Grupo sanguíneo del tipo B.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'ab',
			    'nombre' => 'Tipo AB',
			    'descripcion' => 'Grupo sanguíneo del tipo AB.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'o',
			    'nombre' => 'Tipo O',
			    'descripcion' => 'Grupo sanguíneo del tipo O.',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
