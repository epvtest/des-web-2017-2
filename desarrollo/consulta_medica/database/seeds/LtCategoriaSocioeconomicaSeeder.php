<?php

use Illuminate\Database\Seeder;

class LtCategoriaSocioeconomicaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_categoria_socioeconomica')->insert([
		    array(
			    'codigo' => 'nopobre',
			    'nombre' => 'No Pobre',
			    'descripcion' => 'No pobre',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'pobre',
			    'nombre' => 'Pobre No Extremo',
			    'descripcion' => 'Pobre no extremo',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'pobreextremo',
			    'nombre' => 'Pobre Extremo',
			    'descripcion' => 'Pobre extremo',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
