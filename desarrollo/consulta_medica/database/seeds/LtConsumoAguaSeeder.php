<?php

use Illuminate\Database\Seeder;

class LtConsumoAguaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_consumo_agua')->insert([
		    array(
			    'codigo' => 'acequia',
			    'nombre' => 'Acequia',
			    'descripcion' => 'Acequia',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'cisterna',
			    'nombre' => 'Cisterna',
			    'descripcion' => 'Cisterna',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'pozo',
			    'nombre' => 'Pozo',
			    'descripcion' => 'Pozo',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'redpublica',
			    'nombre' => 'Red pública',
			    'descripcion' => 'Red pública',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'conexiondomicilio',
			    'nombre' => 'Conexión domiciliaria',
			    'descripcion' => 'Conexión domiciliaria',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
