<?php

use Illuminate\Database\Seeder;

class LtEstadoCivilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_estado_civil')->insert([
		    array(
			    'codigo' => 'viudo',
			    'nombre' => 'Viudo',
			    'descripcion' => 'Viudo',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'solteroconfamilia',
			    'nombre' => 'Soltero con familia',
			    'descripcion' => 'Soltero con familia',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'divorciado',
			    'nombre' => 'Divorciado',
			    'descripcion' => 'Divorciado',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'unionestable',
			    'nombre' => 'Unión estable',
			    'descripcion' => 'Unión estable',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'solterosinfamilia',
			    'nombre' => 'Soltero sin familia',
			    'descripcion' => 'Soltero sin familia',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
