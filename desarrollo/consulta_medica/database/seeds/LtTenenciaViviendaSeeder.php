<?php

use Illuminate\Database\Seeder;

class LtTenenciaViviendaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_tenencia_vivienda')->insert([
		    array(
			    'codigo' => 'alquiler',
			    'nombre' => 'Alquiler',
			    'descripcion' => 'Alquiler',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'cuidadoralojado',
			    'nombre' => 'Cuidador o alojado',
			    'descripcion' => 'Cuidador o alojado',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'plansocial',
			    'nombre' => 'Plan social',
			    'descripcion' => 'Plan social',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'alquilerventa',
			    'nombre' => 'Alquiler venta',
			    'descripcion' => 'Alquiler venta',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'propia',
			    'nombre' => 'Propia',
			    'descripcion' => 'Propia',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
