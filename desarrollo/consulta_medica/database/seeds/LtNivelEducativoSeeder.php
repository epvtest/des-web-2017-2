<?php

use Illuminate\Database\Seeder;

class LtNivelEducativoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_nivel_educativo')->insert([
		    array(
			    'codigo' => 'ninguno',
			    'nombre' => 'Ninguno',
			    'descripcion' => 'La persona no ha concluido ningún nivel de estudios.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'inicial',
			    'nombre' => 'Inicial',
			    'descripcion' => 'La persona ha concluido el nivel inicial.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'primaria',
			    'nombre' => 'Primaria',
			    'descripcion' => 'La persona ha concluido el nivel primaria.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'secundaria',
			    'nombre' => 'Secundaria',
			    'descripcion' => 'La persona ha concluido el nivel secundaria.',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'superiornouniversitaria',
			    'nombre' => 'Superior no universitaria',
			    'descripcion' => 'La persona ha concluido alguna carrera técnica.',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'superioruniversitaria',
			    'nombre' => 'Superior universitaria',
			    'descripcion' => 'La persona ha concluido alguna carrera universitaria.',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'postgrado',
			    'nombre' => 'Postgrado o similar',
			    'descripcion' => 'La persona posee estudios de maestría, doctorado o similares.',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'analfabeto',
			    'nombre' => 'Analfabeto',
			    'descripcion' => 'La persona no posee ningún nivel de estudio.',
			    'prioridad' => 80,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
