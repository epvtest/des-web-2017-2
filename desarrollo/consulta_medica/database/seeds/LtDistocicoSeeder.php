<?php

use Illuminate\Database\Seeder;

class LtDistocicoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_distocico')->insert([
		    array(
			    'codigo' => 'mecanica',
			    'nombre' => 'Distocia mecánica',
			    'descripcion' => 'Las distocias óseas afectan a la disposición de los huesos de la pelvis, que dificultan la salida de la cabeza del bebé por falta de espacio.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'hiperdinamica',
			    'nombre' => 'Distocia hiperdinámica',
			    'descripcion' => 'Actividad contráctil del útero caracterizada por contracciones fuertes o muy frecuentes.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'hipodinamica',
			    'nombre' => 'Distocia hipodinámica',
			    'descripcion' => 'Actividad contráctil del útero caracterizada por contracciones débiles o poco frecuentes.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'incoordinacion',
			    'nombre' => 'Incoordinación uterina',
			    'descripcion' => 'Actividad contráctil no rítmica del útero.',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'transversaloblicua',
			    'nombre' => 'Presentación fetal transversal u oblicua',
			    'descripcion' => 'El feto se presenta en forma transversal u oblicua.',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'podalica',
			    'nombre' => 'Presentación fetal podálica',
			    'descripcion' => 'El feto se presenta en posición de nalgas.',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
