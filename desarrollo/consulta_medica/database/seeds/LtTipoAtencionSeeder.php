<?php

use Illuminate\Database\Seeder;

class LtTipoAtencionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_tipo_atencion')->insert([
	    	array(
	    		'codigo' => 'controlreciennacido',
			    'nombre' => 'Control del recién nacido',
			    'descripcion' => 'Control del recién nacido.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'crecimientodesarrollo',
			    'nombre' => 'Crecimiento y desarrollo',
			    'descripcion' => 'Crecimiento y desarrollo.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'administrarvacunas',
			    'nombre' => 'Administración de vacunas',
			    'descripcion' => 'Administración de vacunas.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'administrarmicronutrientes',
			    'nombre' => 'Administración de micronutrientes',
			    'descripcion' => 'Administración de micronutrientes.',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'evaluacionodontologica',
			    'nombre' => 'Evaluación odontológica',
			    'descripcion' => 'Evaluación odontológica.',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'visitafamiliar',
			    'nombre' => 'Visita Familiar Integral',
			    'descripcion' => 'Visita Familiar Integral.',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'sesionesdemostrativas',
			    'nombre' => 'Sesiones demostrativas',
			    'descripcion' => 'Sesiones demostrativas.',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'evaluaciongeneral',
			    'nombre' => 'Evaluación general, crecimiento y desarrollo',
			    'descripcion' => 'Evaluación general, crecimiento y desarrollo.',
			    'prioridad' => 80,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'inmunizacion',
			    'nombre' => 'Inmunizaciones',
			    'descripcion' => 'Inmunizaciones.',
			    'prioridad' => 90,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'evaluacionbucal',
			    'nombre' => 'Evaluación bucal',
			    'descripcion' => 'Evaluación bucal.',
			    'prioridad' => 100,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'Iintervencionespreventivas',
			    'nombre' => 'Intervenciones preventivas',
			    'descripcion' => 'Intervenciones preventivas.',
			    'prioridad' => 110,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'consejeriaintegral',
			    'nombre' => 'Consejería integral',
			    'descripcion' => 'Consejería integral.',
			    'prioridad' => 120,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'visitadomiciliaria',
			    'nombre' => 'Visita Domiciliaria',
			    'descripcion' => 'Visita domiciliaria.',
			    'prioridad' => 130,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'temaseducacion',
			    'nombre' => 'Temas Educativos',
			    'descripcion' => 'Temas educativos.',
			    'prioridad' => 140,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'atencionprioridadessanitarias',
			    'nombre' => 'Atención de prioridades sanitarias',
			    'descripcion' => 'Atención de proridades sanitarias.',
			    'prioridad' => 150,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
