<?php

use Illuminate\Database\Seeder;

class LtRiesgoFamiliarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_riesgo_familiar')->insert([
		    array(
			    'codigo' => 'bajo',
			    'nombre' => 'Bajo',
			    'descripcion' => 'Riesgo familiar bajo',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'mediano',
			    'nombre' => 'Mediano',
			    'descripcion' => 'Riesgo familiar mediano',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'alto',
			    'nombre' => 'Alto',
			    'descripcion' => 'Riesgo familiar alto',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
