<?php

use Illuminate\Database\Seeder;

class LtMaterialPisosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_material_pisos')->insert([
		    array(
			    'codigo' => 'parquetmadera',
			    'nombre' => 'Parqueto o madera pulida',
			    'descripcion' => 'Parqueto o madera pulida',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'asfaltovinilo',
			    'nombre' => 'Láminas asfálticas, vinílicos',
			    'descripcion' => 'Láminas asfálticas, vinílicos',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'losetaterrazo',
			    'nombre' => 'Losetas, terrazos o similares',
			    'descripcion' => 'Losetas, terrazos o similares',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'madera',
			    'nombre' => 'Madera (entablados)',
			    'descripcion' => 'Madera (entablados)',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'cemento',
			    'nombre' => 'Cemento',
			    'descripcion' => 'Cemento',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'tierra',
			    'nombre' => 'Tierra',
			    'descripcion' => 'Tierra',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'otro',
			    'nombre' => 'Otro',
			    'descripcion' => 'Otro',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
