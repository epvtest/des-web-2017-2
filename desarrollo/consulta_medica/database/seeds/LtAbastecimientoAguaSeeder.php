<?php

use Illuminate\Database\Seeder;

class LtAbastecimientoAguaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_abastecimiento_agua')->insert([
		    array(
			    'codigo' => 'redpublicaint',
			    'nombre' => 'Red pública dentro de la vivienda',
			    'descripcion' => 'Red pública dentro de la vivienda',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'redpublicaext',
			    'nombre' => 'Red pública fuera de la vivienda',
			    'descripcion' => 'Red pública fuera de la vivienda',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'pozopilon',
			    'nombre' => 'Pozo artesanal o pilón',
			    'descripcion' => 'Pozo artesanal o pilón',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'otro',
			    'nombre' => 'Otro',
			    'descripcion' => 'Otro',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
