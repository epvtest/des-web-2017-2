<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(LtSeguroSeeder::class);
	    $this->call(LtTipoAtencionSeeder::class);
	    $this->call(LtTipoProblemaSeeder::class);
	    $this->call(LtDestinoPacienteSeeder::class);
	    $this->call(LtCondicionPacienteSeeder::class);
	    $this->call(LtCondicionAltaSeeder::class);
	    $this->call(LtCausaExtEmergenciaSeeder::class);
	    $this->call(LtAnestesiaSeeder::class);
	    $this->call(LtDistocicoSeeder::class);
	    $this->call(LtNivelEducativoSeeder::class);
	    $this->call(LtParentescoJefeHogarSeeder::class);
	    $this->call(LtRhSeeder::class);
	    $this->call(LtGrupoSanguineoSeeder::class);
	    $this->call(LtOcupacionSeeder::class);
	    $this->call(LtSexoSeeder::class);
	    $this->call(LtTipoIdentificacionSeeder::class);
	    $this->call(LtTipoDiagnosticoSeeder::class);
	    $this->call(LtCombustibleCocinaSeeder::class);
	    $this->call(LtRiesgoFamiliarSeeder::class);
	    $this->call(LtCategoriaSocioeconomicaSeeder::class);
	    $this->call(LtAmbitoSeeder::class);
	    $this->call(LtDisposicionBasuraSeeder::class);
	    $this->call(LtServicioSanitarioSeeder::class);
	    $this->call(LtAbastecimientoAguaSeeder::class);
	    $this->call(LtMaterialTechoSeeder::class);
	    $this->call(LtMaterialPisosSeeder::class);
	    $this->call(LtMaterialParedesSeeder::class);
	    $this->call(LtPersonasDormitorioSeeder::class);
	    $this->call(LtIngresoFamiliarSeeder::class);
	    $this->call(LtOcupacionJefeFamiliaSeeder::class);
	    $this->call(LtNivelInstruccionMadreSeeder::class);
	    $this->call(LtEnergiaElectricaSeeder::class);
	    $this->call(LtEliminacionExcretasSeeder::class);
	    $this->call(LtConsumoAguaSeeder::class);
	    $this->call(LtTenenciaViviendaSeeder::class);
	    $this->call(LtGrupoFamiliarSeeder::class);
	    $this->call(LtEstadoCivilSeeder::class);
	    $this->call(LtPermanenciaSeeder::class);
	    $this->call(LtProgramaSocialSeeder::class);
	    $this->call(LtCategoriaSeeder::class);
	    $this->call(LtRedSaludSeeder::class);
    }
}
