<?php

use Illuminate\Database\Seeder;

class LtIngresoFamiliarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_ingreso_familiar')->insert([
		    array(
			    'codigo' => 'menos400',
			    'nombre' => 'Menor de 400',
			    'descripcion' => 'Menor de 400',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'de401a800',
			    'nombre' => 'De 401 a 800',
			    'descripcion' => 'De 401 a 800',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'de801a1200',
			    'nombre' => 'De 801 a 1200',
			    'descripcion' => 'De 801 a 1200',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'de1201a1600',
			    'nombre' => 'De 1201 a 1600',
			    'descripcion' => 'De 1201 a 1600',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'de1601amas',
			    'nombre' => 'Más de 1600',
			    'descripcion' => 'Más de 1600',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
