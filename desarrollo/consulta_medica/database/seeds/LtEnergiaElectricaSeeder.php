<?php

use Illuminate\Database\Seeder;

class LtEnergiaElectricaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_energia_electrica')->insert([
		    array(
			    'codigo' => 'noenergia',
			    'nombre' => 'Sin energía',
			    'descripcion' => 'Sin energía',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'lamparanoelectrica',
			    'nombre' => 'Lámpara no eléctrica',
			    'descripcion' => 'Lámpara no eléctrica',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'temporal',
			    'nombre' => 'Temporal',
			    'descripcion' => 'Temporal',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'permanente',
			    'nombre' => 'Permanente',
			    'descripcion' => 'Permanente',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
