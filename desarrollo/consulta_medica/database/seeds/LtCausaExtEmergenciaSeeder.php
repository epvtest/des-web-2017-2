<?php

use Illuminate\Database\Seeder;

class LtCausaExtEmergenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_causa_ext_emergencia')->insert([
		    array(
			    'codigo' => 'accidentetrabajo',
			    'nombre' => 'Accidente de trabajo',
			    'descripcion' => 'La emergencia se ha producido dentro del lugar de trabajo.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'accidentehogar',
			    'nombre' => 'Accidente de hogar',
			    'descripcion' => 'La emergencia se ha producido en el hogar.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'accidentetransitochofer',
			    'nombre' => 'Accidente de tránsito - Chofer',
			    'descripcion' => 'Un accidente de tránsito, en donde el paciente es el que manejaba.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'accidentetransitoocupante',
			    'nombre' => 'Accidente de tránsito - Ocupante',
			    'descripcion' => 'Un accidente de tránsito, en donde el paciente era pasajero del vehículo.',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'accidentetransitopeaton',
			    'nombre' => 'Accidente de tránsito - Peatón',
			    'descripcion' => 'Un accidente de tránsito, en donde el paciente es un externo al vehículo.',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'agresion',
			    'nombre' => 'Agresión',
			    'descripcion' => 'El paciente ha sido internado producto de una agresión.',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'autoinfligido',
			    'nombre' => 'Autoinfligido',
			    'descripcion' => 'El paciente se ha hecho daño a sí mismo.',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'desastrenatural',
			    'nombre' => 'Desastre Natural',
			    'descripcion' => 'Ha sucedido un desastre natural.',
			    'prioridad' => 80,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'otros',
			    'nombre' => 'Otros',
			    'descripcion' => 'Otras causas que han detonado la emergencia y no se encuentran listadas.',
			    'prioridad' => 90,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
