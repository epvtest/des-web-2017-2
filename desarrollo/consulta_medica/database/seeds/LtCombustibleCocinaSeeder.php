<?php

use Illuminate\Database\Seeder;

class LtCombustibleCocinaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_combustible_cocina')->insert([
		    array(
			    'codigo' => 'electricidad',
			    'nombre' => 'Electricidad',
			    'descripcion' => 'Electricidad',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'gas',
			    'nombre' => 'Gas',
			    'descripcion' => 'Gas',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'kerosene',
			    'nombre' => 'Kerosene',
			    'descripcion' => 'Kerosene',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'carbon',
			    'nombre' => 'Carbón',
			    'descripcion' => 'Carbón',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'lenha',
			    'nombre' => 'Leña',
			    'descripcion' => 'Leña',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'otro',
			    'nombre' => 'Otro',
			    'descripcion' => 'Otro',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'nococina',
			    'nombre' => 'No cocina',
			    'descripcion' => 'No cocina',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
