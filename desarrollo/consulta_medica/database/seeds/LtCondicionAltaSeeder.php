<?php

use Illuminate\Database\Seeder;

class LtCondicionAltaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_condicion_alta')->insert([
		    array(
			    'codigo' => 'curado',
			    'nombre' => 'Curado',
			    'descripcion' => 'El paciente se encuentra en condiciones óptimas de salud.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'mejorado',
			    'nombre' => 'Mejorado',
			    'descripcion' => 'El paciente no se encuentra totalmente recuperado, pero muestra mejoras respecto a cómo ingresó al centro de salud.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'estacionario',
			    'nombre' => 'Estacionario',
			    'descripcion' => 'El paciente se encuentra en las mismas condiciones de salud que cuando ingresó al centro de salud.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'empeorado',
			    'nombre' => 'Empeorado',
			    'descripcion' => 'La salud del paciente se ha visto deteriorada desde que ingresó al centro de salud.',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
