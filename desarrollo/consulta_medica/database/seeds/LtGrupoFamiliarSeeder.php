<?php

use Illuminate\Database\Seeder;

class LtGrupoFamiliarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_grupo_familiar')->insert([
		    array(
			    'codigo' => 'masnuevemiembros',
			    'nombre' => 'Más de 9 miembros',
			    'descripcion' => 'Más de 9 miembros',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'sieteaochomiembros',
			    'nombre' => '7 a 8 miembros',
			    'descripcion' => '7 a 8 miembros',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'cincoaseismiembros',
			    'nombre' => '5 a 6 miembros',
			    'descripcion' => '5 a 6 miembros',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'tresacuatromiembros',
			    'nombre' => '3 a 4 miembros',
			    'descripcion' => '3 a 4 miembros',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'unoadosmiembros',
			    'nombre' => '1 a 2 miembros',
			    'descripcion' => '1 a 2 miembros',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
