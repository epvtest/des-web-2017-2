<?php

use Illuminate\Database\Seeder;

class LtDisposicionBasuraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_disposicion_basura')->insert([
		    array(
			    'codigo' => 'campoabierto',
			    'nombre' => 'A campo abierto',
			    'descripcion' => 'A campo abierto',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'rioacequia',
			    'nombre' => 'Al río o acequia',
			    'descripcion' => 'Al río o acequia',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'pozo',
			    'nombre' => 'En un pozo',
			    'descripcion' => 'En un pozo',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'enterrarquemar',
			    'nombre' => 'Se entierra o quema',
			    'descripcion' => 'Se entierra o quema',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'carrorecolector',
			    'nombre' => 'Carro recolector',
			    'descripcion' => 'Carro recolector',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
