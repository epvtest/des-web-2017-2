<?php

use Illuminate\Database\Seeder;

class LtSeguroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table("lt_seguro")->insert([
		    array(
			    'codigo' => 'sis',
			    'nombre' => 'Seguro Integral de Salud',
			    'descripcion' => 'Seguro social de salud, dirigido a personas en situación de pobreza y pobreza extrema.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'essalud',
			    'nombre' => 'EsSalud',
			    'descripcion' => 'Seguro social de salud, organismo público descentralizado, con personería jurídica de derecho público interno, adscrito al Sector Trabajo y Promoción Social.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'segffaa',
			    'nombre' => 'Seguros de las Fuerzas Armadas',
			    'descripcion' => 'Seguro social de salud dirigidos a las Fuerzas Armadas.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'saludpol',
			    'nombre' => 'Fondo de Aseguramiento en Salud',
			    'descripcion' => 'Seguro social de salud dirigido a la Policía Nacional del Perú.',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'rimac',
			    'nombre' => 'Rímac Seguros',
			    'descripcion' => 'RIMAC Seguros es la empresa líder del mercado asegurador peruano. Formamos parte de Breca, conglomerado empresarial peruano con presencia internacional y con más de cien años de existenc ia, fundado por la familia Brescia Cafferata.',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'mapfre',
			    'nombre' => 'MAPFRE Perú',
			    'descripcion' => 'Somos un equipo multinacional que trabaja para avanzar constantemente en el servicio y desarrollar la mejor relación con nuestros clientes , distribuidores, proveedores, accionistas y sociedad.',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'sanitas',
			    'nombre' => 'Sanitas Perú',
			    'descripcion' => 'Aseguradora peruana que forma parte de Keralty, antes Sanita Internacional.',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'pacifico',
			    'nombre' => 'Pacífico Seguros',
			    'descripcion' => 'Somos una empresa que pertenece al mercado asegurador peruano y cuyo objetivo principal es ayudar a nuestros clientes a gestionar sus riesgos y proteger aquello que más valoran y así garantizar el logro de sus metas.',
			    'prioridad' => 80,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'otro',
			    'nombre' => 'Otro',
			    'descripcion' => 'El paciente cuenta con un seguro de salud que no figura en la lista.',
			    'prioridad' => 90,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'ninguno',
			    'nombre' => 'No tiene',
			    'descripcion' => 'El paciente no cuenta con ningún seguro de salud.',
			    'prioridad' => 100,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
		]);
    }
}
