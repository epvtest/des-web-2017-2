<?php

use Illuminate\Database\Seeder;

class LtOcupacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_ocupacion')->insert([
		    array(
			    'codigo' => 'trabajadordependiente',
			    'nombre' => 'Trabajador dependiente (asalariado)',
			    'descripcion' => 'Trabajador dependiente (asalariado).',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'trabajadorindependiente',
			    'nombre' => 'Trabajador independiente',
			    'descripcion' => 'Trabajador independiente.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'empleador',
			    'nombre' => 'Empleador',
			    'descripcion' => 'Empleador.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'serviciodomestico',
			    'nombre' => 'Servicio doméstico',
			    'descripcion' => 'Servicio doméstico.',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'buscandotrabajo',
			    'nombre' => 'Buscando trabajo',
			    'descripcion' => 'Buscando trabajo.',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'quehacereshogar',
			    'nombre' => 'Quehaceres del hogar',
			    'descripcion' => 'Quehaceres del hogar.',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'estudiante',
			    'nombre' => 'Estudiante',
			    'descripcion' => 'Estudiante.',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'jubilado',
			    'nombre' => 'Jubilado',
			    'descripcion' => 'Jubilado.',
			    'prioridad' => 80,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'sinactividad',
			    'nombre' => 'Sin actividad',
			    'descripcion' => 'Sin actividad.',
			    'prioridad' => 90,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
