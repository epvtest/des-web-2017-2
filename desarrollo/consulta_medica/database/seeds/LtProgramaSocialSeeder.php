<?php

use Illuminate\Database\Seeder;

class LtProgramaSocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_programa_social')->insert([
		    array(
			    'codigo' => 'vasoleche',
			    'nombre' => 'Vaso de leche',
			    'descripcion' => 'Programa de vaso de leche',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'comedorpopular',
			    'nombre' => 'Comedor popular',
			    'descripcion' => 'Comedor popular',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'pronaa',
			    'nombre' => 'PRONAA',
			    'descripcion' => 'Programa Nacional de Asistencia Alimentaria',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'panfar',
			    'nombre' => 'PANFAR',
			    'descripcion' => 'Programa en Alimentación y Nutrición a familias en alto riesgo',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'pantbc',
			    'nombre' => 'PANTBC',
			    'descripcion' => 'Programa de Alimentación y Nutrición para el Paciente Ambulatorio con Tuberculosis y Familia',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'otro',
			    'nombre' => 'Otro',
			    'descripcion' => 'Otro programa social',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
