<?php

use Illuminate\Database\Seeder;

class LtTipoIdentificacionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_tipo_identificacion')->insert([
		    array(
			    'codigo' => 'pasaporte',
			    'nombre' => 'Pasaporte',
			    'descripcion' => 'Pasaporte.',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'dni',
			    'nombre' => 'DNI',
			    'descripcion' => 'Documento Nacional de Identidad.',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'ruc',
			    'nombre' => 'RUC',
			    'descripcion' => 'Registro Único de Contribuyente.',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'carneextranjeria',
			    'nombre' => 'Carné de extranjería',
			    'descripcion' => 'Carné de Extranjería.',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'otro',
			    'nombre' => 'Otro',
			    'descripcion' => 'Otro',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
