<?php

use Illuminate\Database\Seeder;

class LtCategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_categoria')->insert([
		    array(
			    'codigo' => 'i1',
			    'nombre' => 'Categoría I1',
			    'descripcion' => 'Categoría I1',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'i2',
			    'nombre' => 'Categoría I2',
			    'descripcion' => 'Categoría I2',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'i3',
			    'nombre' => 'Categoría I3',
			    'descripcion' => 'Categoría I3',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'i4',
			    'nombre' => 'Categoría I4',
			    'descripcion' => 'Categoría I4',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'ii1',
			    'nombre' => 'Categoría II1',
			    'descripcion' => 'Categoría II1',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'ii2',
			    'nombre' => 'Categoría II2',
			    'descripcion' => 'Categoría II2',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'iie',
			    'nombre' => 'Categoría IIE',
			    'descripcion' => 'Categoría IIE',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'iii1',
			    'nombre' => 'Categoría III1',
			    'descripcion' => 'Categoría III1',
			    'prioridad' => 80,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'iiie',
			    'nombre' => 'Categoría IIIE',
			    'descripcion' => 'Categoría IIIE',
			    'prioridad' => 90,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'iii2',
			    'nombre' => 'Categoría III2',
			    'descripcion' => 'Categoría III2',
			    'prioridad' => 100,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
