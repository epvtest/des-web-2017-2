<?php

use Illuminate\Database\Seeder;

class LtMaterialTechoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('lt_material_techo')->insert([
		    array(
			    'codigo' => 'concretoarmado',
			    'nombre' => 'Concreto armado',
			    'descripcion' => 'Concreto armado',
			    'prioridad' => 10,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'madera',
			    'nombre' => 'Madera',
			    'descripcion' => 'Madera',
			    'prioridad' => 20,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'tejas',
			    'nombre' => 'Tejas',
			    'descripcion' => 'Tejas',
			    'prioridad' => 30,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'calaminafibra',
			    'nombre' => 'Planchas de calamina, fibra',
			    'descripcion' => 'Planchas de calamina, fibra',
			    'prioridad' => 40,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'canhaestera',
			    'nombre' => 'Caña o estera con torta de barro',
			    'descripcion' => 'Caña o estera con torta de barro',
			    'prioridad' => 50,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'pajahojas',
			    'nombre' => 'Paja, hojas de palamera',
			    'descripcion' => 'Paja, hojas de palamera',
			    'prioridad' => 60,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    ),
		    array(
			    'codigo' => 'otro',
			    'nombre' => 'Otro',
			    'descripcion' => 'Otro',
			    'prioridad' => 70,
			    'created_at' => date('Y-m-d H:m:s'),
			    'updated_at' => date('Y-m-d H:m:s')
		    )
	    ]);
    }
}
