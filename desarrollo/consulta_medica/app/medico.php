<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class medico extends Model
{
    use SoftDeletes;
	
    protected $table = 'medico';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('numero_cmp', 'fec_emision_licencia', 'jubilado', 'id_persona');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
