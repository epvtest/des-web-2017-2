<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class detalleSocioeconomico extends Model
{
    use SoftDeletes;
	
    protected $table = 'detalle_socioeconomico';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('anho', 'lt_estado_civil', 'lt_grupo_familiar', 'lt_tenencia_vivienda', 'lt_consumo_agua', 'lt_eliminacion_excretas', 'lt_energia_electrica', 'lt_nivel_instruccion_madre', 'lt_ocupacion_jefe_familia', 'lt_ingreso_familiar', 'lt_personas_dormitorio', 'id_ficha_familiar');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
