<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class examenAuxiliar extends Model
{
    use SoftDeletes;
	
    protected $table = 'examen_auxiliar';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('descripcion', 'fec_emision', 'id_medico', 'id_consulta');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
