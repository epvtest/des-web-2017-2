<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class examenAuxiliarDetalle extends Model
{
    use SoftDeletes;
	
    protected $table = 'examen_auxiliar_detalle';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('lt_clave', 'valor', 'id_examen_auxiliar');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
