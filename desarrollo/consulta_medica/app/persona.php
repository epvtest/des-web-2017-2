<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class persona extends Model
{
    use SoftDeletes;
	
    protected $table = 'persona';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('lt_tipo_identificacion', 'numero_identificacion', 'nombre', 'ape_paterno', 'ape_materno', 'ape_casada', 'lt_sexo', 'lt_estado_civil', 'fec_nacimiento', 'telefono', 'lt_ocupacion', 'lt_grupo_sanguineo', 'lt_rh', 'lt_parentesco_jefe_hogar', 'ind_victima_violacion_ddhh', 'ind_provision_anticonceptivos', 'lt_nivel_educativo', 'centro_educativo', 'ingresos_mensuales');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
