<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class recienNacido extends Model
{
    use SoftDeletes;
	
    protected $table = 'recien_nacido';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('aspecto_general', 'desarrollo', 'signos_vitales', 'piel', 'cabeza', 'boca', 'nariz', 'ojos', 'oidos', 'cabellos', 'torax', 'dorso');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
