<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class atencionIntegral extends Model
{
	use SoftDeletes;
	
    protected $table = 'atencion_integral';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('lt_tipo_atencion', 'descripcion', 'fecha', 'lugar', 'id_paciente');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
