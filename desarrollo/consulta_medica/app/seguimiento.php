<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class seguimiento extends Model
{
    use SoftDeletes;
	
    protected $table = 'seguimiento';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('fec_visito', 'nombre_visitador', 'problemas_identificados', 'recomendaciones', 'ind_cumple_recomendaciones', 'id_ficha_familiar');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
