<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class detalleFamilia extends Model
{
	use SoftDeletes;
	
    protected $table = 'detalle_familia';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('lt_programa_social', 'otro_programa_social', 'distrito_procedencia', 'lt_permanencia', 'observaciones', 'id_ficha_familiar');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
