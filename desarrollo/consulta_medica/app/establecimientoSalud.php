<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class establecimientoSalud extends Model
{
    use SoftDeletes;
	
    protected $table = 'establecimiento_salud';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('nombre', 'lt_categoria', 'lt_red_salud');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
