<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class parto extends Model
{
    use SoftDeletes;
	
    protected $table = 'parto';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('ind_eutocico', 'duracion_periodo_expulsivo', 'lt_distocico', 'pre_parto', 'lt_anestesia', 'duracion_parto', 'signos_sufrimiento_fetal', 'intervencion_medicacion', 'observacion', 'id_obstetriz', 'id_medico', 'id_recien_nacido');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
