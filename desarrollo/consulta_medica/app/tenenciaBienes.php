<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class tenenciaBienes extends Model
{
    use SoftDeletes;
	
    protected $table = 'tenencia_bienes';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('ind_radio', 'ind_televisor', 'ind_refrigeradora', 'ind_cocina', 'ind_telefono_fijo', 'ind_telefono_celular', 'ind_lavadora', 'ind_computadora', 'ind_horno_microondas', 'ind_suministro_electrico', 'suministro_electrico', 'consumo_electrico', 'mes_periodo_consumo', 'anho_periodo_consumo', 'id_vivienda');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
