<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class tratamiento extends Model
{
    use SoftDeletes;
	
    protected $table = 'tratamiento';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('descripcion', 'id_consulta');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
