<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ltCategoria extends Model
{
    use SoftDeletes;
	
    protected $table = 'lt_categoria';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('codigo', 'nombre', 'descripcion', 'prioridad');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
