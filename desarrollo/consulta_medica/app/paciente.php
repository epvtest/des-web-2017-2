<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class paciente extends Model
{
    use SoftDeletes;
	
    protected $table = 'paciente';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('numero_seguro', 'id_persona', 'lt_seguro');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
