<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class detalleVivienda extends Model
{
    use SoftDeletes;
	
    protected $table = 'detalle_vivienda';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('lt_material_paredes', 'otro_material_paredes', 'lt_material_pisos', 'otro_material_pisos', 'lt_material_techo', 'otro_material_techo', 'lt_abastecimiento_agua', 'lt_servicio_sanitario', 'habitaciones', 'id_vivienda');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
