<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class antecedentesPatologias extends Model
{
	use SoftDeletes;
	
    protected $table = 'antecedentes_patologias';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('antecedentes_madre', 'antecedentes_padre', 'antecedentes_abuelos', 'otros', 'id_recien_nacido');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
