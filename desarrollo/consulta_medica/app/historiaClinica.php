<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class historiaClinica extends Model
{
    use SoftDeletes;
	
    protected $table = 'historia_clinica';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('numero_hc', 'ind_emergencia', 'motivo_emergencia', 'lt_causa_ext_emergencia', 'ind_referencia', 'fec_referencia', 'motivo_referencia', 'acompanhante', 'id_ficha_familiar', 'id_paciente');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
