<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class problema extends Model
{
    use SoftDeletes;
	
    protected $table = 'problema';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('lt_tipo_problema', 'fecha', 'ind_activo', 'observacion', 'id_paciente');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
