<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class alta extends Model
{
    use SoftDeletes;

    protected $table = 'alta';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('lt_condicion_alta', 'lt_condicion_paciente', 'lt_destino_paciente', 'id_establecimiento_destino', 'servicio_transferir', 'numero_cama', 'fecha', 'hora', 'id_historia_clinica');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
