<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class examenFisico extends Model
{
    use SoftDeletes;
	
    protected $table = 'examen_fisico';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('temperatura', 'presion_arterial', 'frecuencia_cardiaca', 'frecuencia_respiratoria', 'talla', 'peso', 'indice_masa_corporal', 'id_consulta');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
