<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class vivienda extends Model
{
    use SoftDeletes;
	
    protected $table = 'vivienda';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('departamento', 'provincia', 'distrito', 'ubigeo', 'centro_poblado', 'direccion', 'lt_ambito', 'telefono', 'lt_categoria_socioecon', 'lt_riesgo_familiar', 'id_ficha_familiar');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
