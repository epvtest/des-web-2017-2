<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class consulta extends Model
{
	use SoftDeletes;
	
    protected $table = 'consulta';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('fec_consulta', 'motivo', 'tiempo_enfermedad', 'apetito', 'sed', 'suenho', 'estado_animo', 'orina', 'deposiciones', 'fec_proxima_cita', 'observaciones', 'id_medico', 'id_historia_clinica');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
