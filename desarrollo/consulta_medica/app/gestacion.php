<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class gestacion extends Model
{
    use SoftDeletes;
	
    protected $table = 'gestacion';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('ind_complicado', 'fec_concepcion', 'duracion_embarazo', 'patologias', 'id_recien_nacido');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
