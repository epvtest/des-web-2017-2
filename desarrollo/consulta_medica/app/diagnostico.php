<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class diagnostico extends Model
{
    use SoftDeletes;
	
    protected $table = 'gestacion';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('lt_tipo_diagnostico', 'descripcion', 'codigo_cie_10', 'id_consulta');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
