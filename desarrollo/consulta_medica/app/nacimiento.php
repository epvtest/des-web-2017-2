<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class nacimiento extends Model
{
    use SoftDeletes;
	
    protected $table = 'nacimiento';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('hora', 'peso', 'talla', 'cuna', 'fec_alta', 'id_recien_nacido');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
