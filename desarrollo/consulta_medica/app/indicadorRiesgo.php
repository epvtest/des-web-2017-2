<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class indicadorRiesgo extends Model
{
    use SoftDeletes;
	
    protected $table = 'indicador_riesgo';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('ind_hijos_sin_partida_nacimiento', 'ind_ninhos_desercion_escolar', 'ind_lactancia_exclusiva', 'ind_planificacion_familiar', 'ind_alimentacion_comp_deficiente', 'ind_papanicolau', 'ind_diabetes_hipertension', 'ind_dependencia_funcional', 'ind_gestante', 'ind_parto_domiciliario', 'ind_vacunas_completas', 'ind_leishmaniasis', 'ind_chagas', 'ind_estres', 'ind_depresion', 'ind_abandono', 'ind_violencia_familiar', 'ind_discapacidad_evidente', 'ind_madre_adolescente', 'ind_desnutricion', 'ind_tuberculosis', 'ind_esquizofrenia', 'ind_cancer', 'ind_alcoholismo_drogadiccion', 'ind_conducta_sexual_riesgo', 'ind_delincuencia_pandillaje', 'id_ficha_familiar');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
