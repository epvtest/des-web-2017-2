<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class educacionFamiliar extends Model
{
    use SoftDeletes;
	
    protected $table = 'educacion_familiar';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('fec_nutricion_saludable', 'fec_ambiente_saludable', 'fec_cultura_paz', 'fec_pautas_crianza', 'fec_habilidades_vida', 'fec_salud_sexual', 'fec_seguridad_vial', 'fec_prev_enf_preval', 'id_ficha_familiar');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
