<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class fichaFamiliar extends Model
{
    use SoftDeletes;
	
    protected $table = 'ficha_familiar';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('numero', 'ind_visita_domiciliaria', 'fec_verificacion', 'id_trabajador', 'observaciones', 'id_establecimiento_salud');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
