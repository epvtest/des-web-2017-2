<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class adicionalVivienda extends Model
{
	use SoftDeletes;

    protected $table = 'adicional_vivienda';
	
	protected $primaryKey = 'id';
	
	protected $fillable = array('ind_perros', 'ind_gatos', 'ind_vacas', 'ind_cabras', 'ind_carnero', 'ind_cerdo', 'ind_aves', 'ind_cuyes', 'ind_chirimachas', 'ind_caballos', 'ind_burros', 'ind_otros_animales', 'lt_disposicion_basura', 'id_vivienda');
	
	protected $hidden = ['created_at', 'updated_at'];

	protected $dates = ['deleted_at'];
}
