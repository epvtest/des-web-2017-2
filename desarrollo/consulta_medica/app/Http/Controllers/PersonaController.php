<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PersonaController extends Controller
{
    /**
     * Guarda una persona, sea o no nueva.
     */
    public function save(Request $request) {
        $personaPost = $request->all();
        $registro = $personaPost['persona'];
        // Por si no se haya marcado esta opción, se deja como defecto el valor false.
        $registro['ind_provision_anticonceptivos'] = $registro['ind_provision_anticonceptivos']? $registro['ind_provision_anticonceptivos'] : false;
        // Indica una actualización o un registro nuevo
        $persona = $registro['id'] ? persona::find($registro['id'])->setRawAttributes($registro) : new persona($registro);
        $registro->save();
        return response()->json([
            'success' => true,
            'persona' => $persona
        ]);
    }
}
