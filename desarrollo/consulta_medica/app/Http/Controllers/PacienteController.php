<?php

namespace App\Http\Controllers;

use App\ltEstadoCivil;
use App\ltGrupoSanguineo;
use App\ltNivelEducativo;
use App\ltOcupacion;
use App\ltParentescoJefeHogar;
use App\ltRh;
use App\ltSeguro;
use App\ltSexo;
use App\ltTipoIdentificacion;
use App\paciente;
use App\persona;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PacienteController extends Controller
{
    /**
     * Carga los combobox de la pantalla de la búsqueda de pacientes.
     *
     */
    public function index() {
        $lt_seguro = ltSeguro::all();
        $lt_parentesco_jefe_hogar = ltParentescoJefeHogar::all();
        $lt_tipo_identificacion = ltTipoIdentificacion::all();
        $lt_grupo_sanguineo = ltGrupoSanguineo::all();
        $lt_nivel_educativo  = ltNivelEducativo::all();
        $lt_estado_civil = ltEstadoCivil::all();
        $lt_ocupacion = ltOcupacion::all();
        $lt_sexo = ltSexo::all();
        $lt_rh = ltRh::all();
        return response()->json([
            'success' => true,
            'lt_seguro' => $lt_seguro,
            'lt_parentesco_jefe_hogar' => $lt_parentesco_jefe_hogar,
            'lt_tipo_identificacion' => $lt_tipo_identificacion,
            'lt_grupo_sanguineo' => $lt_grupo_sanguineo,
            'lt_nivel_educativo' => $lt_nivel_educativo,
            'lt_estado_civil' => $lt_estado_civil,
            'lt_ocupacion' => $lt_ocupacion,
            'lt_sexo' => $lt_sexo,
            'lt_rh' => $lt_rh,
        ]); 
    }


    /**
     * Busca a los pacientes de acuerdo a su tipo y número de documento de identidad.
     */
    public function search(Request $request) {
        $arrayData = $request->all();
        $paciente = DB::table('paciente')
            ->join('persona', function ($persona) use ($arrayData) {
                $persona->on('persona.id', '=', 'paciente.id_persona')
                    ->where([
                        ['persona.numero_identificacion', '=', $arrayData['numero_identificacion']],
                        ['persona.lt_tipo_identificacion', '=', $arrayData['lt_tipo_identificacion']]
                    ]);
            })
            ->first();

        return response()->json([
            'success' => true,
            'paciente' => $paciente
        ]);
    }

    /**
     * Almacena un paciente.
     */
    public function save(Request $request) {
        $pacientePost = $request->all();
        $personaController = new PersonaController;
        $personaResponse = $personaController->save($pacientePost['persona']);
        $persona->$personaResponse->all()['persona'];
        if ($pacientePost['id']) {  // Has id
            $paciente = paciente::find($pacientePost['id']);
            $paciente->setAttribute('lt_seguro', $pacientePost['lt_seguro']);
            $paciente->setAttribute('numero_seguro', $pacientePost['numero_seguro']);
        } else {
            $paciente = new paciente($pacientePost);
        }
        $paciente->setAttribute('id_persona', $persona->getAttributeValue('id'));
        $paciente->save();

        return response()->json([
            'success' => true,
            'paciente' => $paciente
        ]);
    }
    
}
