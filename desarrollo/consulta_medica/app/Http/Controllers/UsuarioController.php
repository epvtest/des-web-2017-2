<?php

namespace App\Http\Controllers;

use App\ltEstadoCivil;
use App\ltGrupoSanguineo;
use App\ltNivelEducativo;
use App\ltOcupacion;
use App\ltParentescoJefeHogar;
use App\ltRh;
use App\ltSeguro;
use App\ltSexo;
use App\ltTipoIdentificacion;
use App\paciente;
use App\persona;
use App\User;
use Illuminate\Http\Request;

class UsuarioController extends Controller
{
    /**
     * Muestra los typelist que se mostrarán en el formulario.
     */
    public function getListTypes() {
        $lt_seguro = ltSeguro::all();
        $lt_parentesco_jefe_hogar = ltParentescoJefeHogar::all();
        $lt_tipo_identificacion = ltTipoIdentificacion::all();
        $lt_grupo_sanguineo = ltGrupoSanguineo::all();
        $lt_nivel_educativo  = ltNivelEducativo::all();
        $lt_estado_civil = ltEstadoCivil::all();
        $lt_ocupacion = ltOcupacion::all();
        $lt_sexo = ltSexo::all();
        $lt_rh = ltRh::all();
        return response()->json([
            'success' => true,
            'lt_seguro' => $lt_seguro,
            'lt_parentesco_jefe_hogar' => $lt_parentesco_jefe_hogar,
            'lt_tipo_identificacion' => $lt_tipo_identificacion,
            'lt_grupo_sanguineo' => $lt_grupo_sanguineo,
            'lt_nivel_educativo' => $lt_nivel_educativo,
            'lt_estado_civil' => $lt_estado_civil,
            'lt_ocupacion' => $lt_ocupacion,
            'lt_sexo' => $lt_sexo,
            'lt_rh' => $lt_rh,
        ]);
    }

    /**
     * Registra un nuevo usuario a partir de los datos ingresados.
     */
    public function save(Request $request) {
        $personaCtrl = new PersonaController;
        $data = $request->all();
        $usuario = $data['user'];
        $persona = $data['persona'];
        $personaRes = $personaCtrl->save($persona);
        $personaAll = $personaRes->all();
        $personaActual = $personaAll['persona'];
        if ($usuario['id']) {
            $usuarioData = User::find($usuario['id']);
            $usuarioData->setAttribute('password', bcrypt($usuarioData['password']));
        } else {
            $usuarioData = User::create([
                'name' => $usuarioData['name'],
                'email' => $usuarioData['email'],
                'password' => bcrypt($usuarioData['password'])
            ]);
        }
        return response()->json([
            'success' => true,
            'usuario' => $usuarioData
        ]);
    }

}
