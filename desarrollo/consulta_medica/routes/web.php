<?php

/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | contains the "web" middleware group. Now create something great!
 * |
 */
Route::group([
    'prefix' => 'admin',
    'middlewere' => 'auth'
], function () {
    Route::get('/', function () {
        return view('admin.dashboard');
    })->name('administrador');
    Route::get('pacientes', function () {
        return view('admin.pacientes');
    })->name('pacientes');
    Route::get('diagnostico', function () {
        return view('admin.diagnostico');
    })->name('diagnostico');
    Route::get('examenes', function () {
        return view('admin.examenes');
    })->name('examenes');
    Route::get('triaje', function () {
        return view('admin.triaje');
    })->name('triaje');
    Route::get('historias', function () {
        return view('admin.historias');
    })->name('historias');
    Route::get('fese', function () {
        return view('admin.fese');
    })->name('fese');
});

Route::group([
    'middleware' => 'web'
], function () {
    Route::get('/', function () {
        return view('welcome');
    });
    Route::get('about', function () {
        return view('about');
    });
    Route::get('services', function () {
        return view('services');
    });
    Route::get('contact', function () {
        return view('contact');
    });
    Route::get('galery', function () {
        return view('galery');
    });
    
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');
    
    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');
    
    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
    
});