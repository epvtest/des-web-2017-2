var Paciente = function (data) {
	this.id = null;
	this.numero_seguro = null;
	this.id_persona = null;
	this.lt_seguro = null;
	this.persona = new Persona();

	this.hydrate = function (data) {
		if(data){
			this.id = data['id']?data['id']:this.id;
			this.numero_seguro = data['numero_seguro']?data['numero_seguro']:this.numero_seguro;
			this.id_persona = data['id_persona']?data['id_persona']:this.id_persona;
			this.lt_seguro = data['lt_seguro']?data['lt_seguro']:this.lt_seguro;
			this.persona = new Persona(data);
		}
	};
	this.hydrate(data);

	this.onlyLtId = function () {
		this.lt_seguro = this.lt_seguro['id'];
		this.persona.onlyLtId();
	}

};