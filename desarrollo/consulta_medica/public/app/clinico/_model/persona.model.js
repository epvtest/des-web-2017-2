var Persona = function (data) {
	this.id = null;
	this.lt_tipo_identificacion = null;
	this.numero_identificacion = null;
	this.nombre = null;
	this.ape_paterno = null;
	this.ape_materno = null;
	this.ape_casada = null;
	this.lt_sexo = null;
	this.lt_estado_civil = null;
	this.fec_nacimiento = null;
	this.telefono = null;
	this.lt_ocupacion = null;
	this.lt_grupo_sanguineo = null;
	this.lt_rh = null;
	this.lt_parentesco_jefe_hogar = null;
	this.ind_victima_violacion_ddhh = null;
	this.ind_provision_anticonceptivos = null;
	this.lt_nivel_educativo = null;
	this.centro_educativo = null;
	this.ingresos_mensuales = null;

	this.hydrate = function (data) {
		if(data){
			this.id = data['id']?data['id']:this.id;
			this.lt_tipo_identificacion = data['lt_tipo_identificacion']?data['lt_tipo_identificacion']:this.lt_tipo_identificacion;
			this.numero_identificacion = data['numero_identificacion']?data['numero_identificacion']:this.numero_identificacion;
			this.nombre = data['nombre']?data['nombre']:this.nombre;
			this.ape_paterno = data['ape_paterno']?data['ape_paterno']:this.ape_paterno;
			this.ape_materno = data['ape_materno']?data['ape_materno']:this.ape_materno;
			this.ape_casada = data['ape_casada']?data['ape_casada']:this.ape_casada;
			this.lt_sexo = data['lt_sexo']?data['lt_sexo']:this.lt_sexo;
			this.lt_estado_civil = data['lt_estado_civil']?data['lt_estado_civil']:this.lt_estado_civil;
			this.fec_nacimiento = data['fec_nacimiento']?new Date(data['fec_nacimiento']):this.fec_nacimiento;
			this.telefono = data['telefono']?data['telefono']:this.telefono;
			this.lt_ocupacion = data['lt_ocupacion']?data['lt_ocupacion']:this.lt_ocupacion;
			this.lt_grupo_sanguineo = data['lt_grupo_sanguineo']?data['lt_grupo_sanguineo']:this.lt_grupo_sanguineo;
			this.lt_rh = data['lt_rh']?data['lt_rh']:this.lt_rh;
			this.lt_parentesco_jefe_hogar = data['lt_parentesco_jefe_hogar']?data['lt_parentesco_jefe_hogar']:this.lt_parentesco_jefe_hogar;
			this.ind_victima_violacion_ddhh = data['ind_victima_violacion_ddhh'] !== 0;
			this.ind_provision_anticonceptivos = data['ind_provision_anticonceptivos'] !== 0;
			this.lt_nivel_educativo = data['lt_nivel_educativo']?data['lt_nivel_educativo']:this.lt_nivel_educativo;
			this.centro_educativo = data['centro_educativo']?data['centro_educativo']:this.centro_educativo;
			this.ingresos_mensuales = data['ingresos_mensuales']?data['ingresos_mensuales']:this.ingresos_mensuales;
		}
	};

	this.hydrate(data);

	this.onlyLtId = function () {
		this.lt_parentesco_jefe_hogar = this.lt_parentesco_jefe_hogar['id'];
		this.lt_tipo_identificacion = this.lt_tipo_identificacion['id'];
		this.lt_grupo_sanguineo = this.lt_grupo_sanguineo['id'];
		this.lt_nivel_educativo = this.lt_nivel_educativo['id'];
		this.lt_estado_civil = this.lt_estado_civil['id'];
		this.lt_ocupacion = this.lt_ocupacion['id'];
		this.lt_sexo = this.lt_sexo['id'];
		this.lt_rh = this.lt_rh['id'];
	}
};