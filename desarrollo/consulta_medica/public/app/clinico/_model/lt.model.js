var LtModel = function (data) {
	this.id = null;
	this.codigo = null;
	this.nombre = null;
	this.descripcion = null;
	this.prioridad = null;

	this.hydrate = function (data) {
		if(data){
			this.id = data['id']?data['id']:this.id;
			this.codigo = data['codigo']?data['codigo']:this.codigo ;
			this.nombre = data['nombre']?data['nombre']:this.nombre;
			this.descripcion = data['descripcion']?data['descripcion']:this.descripcion;
			this.prioridad = data['prioridad']?data['prioridad']:this.prioridad;
		}
	};

	this.hydrate(data);
};