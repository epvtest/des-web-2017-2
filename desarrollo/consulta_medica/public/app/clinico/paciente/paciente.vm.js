var VMPaciente = function () {

	this.paciente = null;
	this.enablePacienteForm = false;

	this.lt_seguro = [];
	this.lt_parentesco_jefe_hogar = [];
	this.lt_tipo_identificacion = [];
	this.lt_grupo_sanguineo = [];
	this.lt_nivel_educativo = [];
	this.lt_estado_civil = [];
	this.lt_ocupacion = [];
	this.lt_sexo = [];
	this.lt_rh = [];

	this.errorMsg = '';
	this.procesando = false;

	this.searchForm = {
		lt_tipo_identificacion : null,
		numero_identificacion: '',
		clean: function () {
			this.lt_tipo_identificacion = null;
			this.numero_identificacion = '';
		}
	};


	this.showMessageError = function (error) {
		this.errorMsg = error;
		this.procesando = false;
	};

	this.nuevo = function () {
		this.searchForm.clean();
		this.paciente = new Paciente();
		this.setEnablePacienteForm(true);
	};

	this.cancelar = function () {
		this.nuevo();
		this.setEnablePacienteForm(false);
	};

	this.reset = function () {
		this.searchForm.clean();
		this.paciente = new Paciente();
		this.setEnablePacienteForm(false);
	};

	this.setEnablePacienteForm = function (flag) {
		this.enablePacienteForm = flag;
	};

	this.fillLts = function () {
		this.paciente.lt_seguro = this.findLtById(this.lt_seguro, this.paciente.lt_seguro);
		this.paciente.persona.lt_parentesco_jefe_hogar = this.findLtById(this.lt_parentesco_jefe_hogar, this.paciente.persona.lt_parentesco_jefe_hogar);
		this.paciente.persona.lt_tipo_identificacion = this.findLtById(this.lt_tipo_identificacion, this.paciente.persona.lt_tipo_identificacion);
		this.paciente.persona.lt_grupo_sanguineo = this.findLtById(this.lt_grupo_sanguineo, this.paciente.persona.lt_grupo_sanguineo);
		this.paciente.persona.lt_nivel_educativo = this.findLtById(this.lt_nivel_educativo, this.paciente.persona.lt_nivel_educativo);
		this.paciente.persona.lt_estado_civil = this.findLtById(this.lt_estado_civil, this.paciente.persona.lt_estado_civil);
		this.paciente.persona.lt_ocupacion = this.findLtById(this.lt_ocupacion, this.paciente.persona.lt_ocupacion);
		this.paciente.persona.lt_sexo = this.findLtById(this.lt_sexo, this.paciente.persona.lt_sexo);
		this.paciente.persona.lt_rh = this.findLtById(this.lt_rh, this.paciente.persona.lt_rh);
	};

	this.findLtById = function (lt_array, id) {
		return lt_array.find(function (elem) {
			return elem['id'] === id;
		});
	};

	this.hydrate = function (data) {
		if (data) {
			this.lt_seguro = data['lt_seguro']?data['lt_seguro']:this.lt_seguro;
			this.lt_parentesco_jefe_hogar = data['lt_parentesco_jefe_hogar']?data['lt_parentesco_jefe_hogar']:this.lt_parentesco_jefe_hogar;
			this.lt_tipo_identificacion = data['lt_tipo_identificacion']?data['lt_tipo_identificacion']:this.lt_tipo_identificacion;
			this.lt_grupo_sanguineo = data['lt_grupo_sanguineo']?data['lt_grupo_sanguineo']:this.lt_grupo_sanguineo;
			this.lt_nivel_educativo = data['lt_nivel_educativo']?data['lt_nivel_educativo']:this.lt_nivel_educativo;
			this.lt_estado_civil = data['lt_estado_civil']?data['lt_estado_civil']:this.lt_estado_civil;
			this.lt_ocupacion = data['lt_ocupacion']?data['lt_ocupacion']:this.lt_ocupacion;
			this.lt_sexo = data['lt_sexo']?data['lt_sexo']:this.lt_sexo;
			this.lt_rh = data['lt_rh']?data['lt_rh']:this.lt_rh;
		}
	}
};
