angular.module('app.clinico')
	.controller('PacienteController',
		['$scope', 'MESSAGES', 'PacienteService', '$location', '$window',
			function ($scope, MESSAGES, PacienteService, $location, $window) {

				$scope.vm = new VMPaciente();

				$scope.hasErrors = function (attr) {
					return attr.$invalid && (!attr.$pristine || attr.$touched);
				};

				/* Index */
				$scope.index = function () {
					$scope.vm.procesando = true;
					$scope.vm.error = "";

					PacienteService.index()
						.then(
							function (response) {
								var data = response.data;
								$scope.vm.hydrate(data);    // Fill lts
							},
							function (reason) {
								$scope.vm.showMessageError(MESSAGES.SERVER.ERROR);
							}
						);
				};

				/* Create */
				$scope.save = function () {
					$scope.vm.procesando = true;
					$scope.vm.error = "";

					$scope.vm.paciente.onlyLtId();

					PacienteService.save($scope.vm.paciente)
						.then(
							function (response) {
								$scope.vm.reset();
								$scope.paciente_form.$setPristine();
							},
							function (error) {

							}
						)
				};

				$scope.search = function () {
					$scope.vm.procesando = true;
					$scope.vm.error = "";
					$scope.vm.paciente = new Paciente();    // RESET FORM PACIENTE

					var criteria = {
						lt_tipo_identificacion: $scope.vm.searchForm.lt_tipo_identificacion['id'],
						numero_identificacion : $scope.vm.searchForm.numero_identificacion,
					};

					PacienteService.search(criteria)
						.then(
							function (response) {
								var data = response.data;
								var paciente = data['paciente'];
								if (!paciente) {
									$scope.vm.showMessageError(MESSAGES.PACIENTE.NOT_FOUND);
								} else {
									$scope.vm.paciente = new Paciente(paciente);
									$scope.vm.fillLts();
								}
							},
							function (error) {
							}
						)
				};

				$scope.index(); // Fill lts
			}
		]);