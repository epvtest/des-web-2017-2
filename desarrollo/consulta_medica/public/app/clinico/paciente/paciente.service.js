angular.module('app.clinico')
	.service('PacienteService',
		['$http','$q', 'API',
			function($http,$q,API) {

				this.index = function () {
					var deferred = $q.defer();

					deferred.resolve(
						$http({
							method: 'GET',
							url: API.PACIENTE.INDEX
						})
					);
					return deferred.promise;
				};

				this.save = function (userData) {
					var deferred = $q.defer();
					deferred.resolve(
						$http({
							method: 'POST',
							url: API.PACIENTE.SAVE,
							data: JSON.stringify(userData)
						})
					);
					return deferred.promise;
				};

				this.search = function (criteria) {
					var deferred = $q.defer();
					deferred.resolve(
						$http({
							method: 'POST',
							url: API.PACIENTE.SEARCH,
							data: JSON.stringify(criteria)
						})
					);
					return deferred.promise;
				}
			}
		]);