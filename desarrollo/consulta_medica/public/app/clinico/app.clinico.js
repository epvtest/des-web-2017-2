var app = angular.module('app', [
	'app.core',
	'app.clinico'
]);

app.config(['$stateProvider','$urlRouterProvider','$locationProvider',
	function($stateProvider,$urlRouterProvider,$locationProvider){

		$stateProvider
			.state("home",{
				url:"/",
				templateUrl : 'app/clinico/home/home.html'
			})
			.state("paciente", {
				url: "/paciente",
				templateUrl : 'app/clinico/paciente/paciente.html',
				controller : 'PacienteController'
			})
		;

		$urlRouterProvider.otherwise('/');
		$locationProvider.html5Mode(true);
	}
]);

app.run(['$rootScope','$window',
	function ($rootScope,$window) {
		$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

		});

		$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
			$window.scrollTo(0, 0);
		});
	}
]);
