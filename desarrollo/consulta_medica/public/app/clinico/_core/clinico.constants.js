angular.module('app.core')
    .constant('API', {
        SESSION : {
            LOGOUT : 'api/session/logout'
        },
	    PACIENTE: {
        	INDEX : 'api/paciente/index',
		    SEARCH: 'api/paciente/search',
		    SAVE: 'api/paciente/save'
	    }
    })
    .constant('MESSAGES', {
    	SERVER: {
    		ERROR: 'Error en el Servidor. Vuelva a intentar'
	    },
    	LOGOUT: {
		    FAIL_AUTH: 'Error durante logout'
	    },
	    PACIENTE: {
    		NOT_FOUND: 'Paciente no encontrado'
	    }
    });

