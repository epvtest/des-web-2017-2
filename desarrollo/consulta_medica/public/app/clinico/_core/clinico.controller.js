angular.module('app.core')
	.controller('ClinicoController',
		['$scope','sessionService','$location','$window',
			function($scope,sessionService,$location,$window) {
				$scope.saliendo = false;

				$scope.logout = function(){
					$scope.saliendo = true;

					sessionService.logout()
						.then(
							function(response) {
								$window.location.reload();
							},
							function(reason) {
							}
						);
				}

			}
		]);
