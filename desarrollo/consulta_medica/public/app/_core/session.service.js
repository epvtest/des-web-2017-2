angular.module('app.core')
	.service('sessionService',
		['$http', '$q', 'API',
			function ($http, $q, API) {

				this.register = function (userData) {
					var deferred = $q.defer();
					deferred.resolve(
						$http({
							method: 'POST',
							url: API.SESSION.REGISTER,
							data: JSON.stringify(userData)
						})
					);
					return deferred.promise;
				};

				this.login = function (userData) {
					var deferred = $q.defer();
					deferred.resolve(
						$http({
							method: 'POST',
							url: API.SESSION.LOGIN,
							data: JSON.stringify(userData)
						})
					);
					return deferred.promise;
				};

				this.logout = function (userData) {
					var deferred = $q.defer();
					deferred.resolve(
						$http({
							method: 'GET',
							url: API.SESSION.LOGOUT
						})
					);
					return deferred.promise;
				}
			}
		]);
