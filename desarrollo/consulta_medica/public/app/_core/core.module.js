/**
 * Common modules
 */
angular.module('app.core', [
	'ngRoute',
	'ngAnimate',
	'ui.router',
	'ui.bootstrap',
]);