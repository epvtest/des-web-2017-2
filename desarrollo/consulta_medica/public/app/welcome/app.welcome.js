var app = angular.module('app', [
	'app.core',
	'app.welcome'
]);

app.config(['$stateProvider','$urlRouterProvider','$locationProvider',
	function($stateProvider,$urlRouterProvider,$locationProvider){

		$stateProvider
			.state("register",{
				url:"/registro",
				templateUrl : 'app/welcome/register/register.html',
				controller: 'RegisterController'
			})
			.state("login",{
				url:"/logeo",
				templateUrl : 'app/welcome/login/login.html',
				controller: 'LoginController'
			})
			.state("home",{
				url:"/",
				templateUrl : 'app/welcome/home/home.html',
			})
			.state("acerca",{
				url:"/acerca",
				templateUrl : 'app/welcome/acerca/acerca.html'
			});

		$urlRouterProvider.otherwise('/');
		$locationProvider.html5Mode(true);
	}
]);

app.run(['$rootScope','$window',
	function ($rootScope,$window) {
		$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {

		});

		$rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
			$window.scrollTo(0, 0);
		});
	}
]);
