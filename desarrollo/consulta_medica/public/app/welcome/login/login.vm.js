var VMLogin = function () {

	this.email = '';
	this.password = '';
	this.errorMsg = '';
	this.procesando = false;

	this.showMessageError = function (error) {
		this.errorMsg = error;
		this.procesando = false;
	}
};
