angular.module('app.welcome')
	.controller('LoginController',
		['$scope', 'MESSAGES', 'sessionService', '$location', '$window',
			function ($scope, MESSAGES, sessionService, $location, $window) {

				$scope.vm = new VMLogin();

				$scope.hasErrors = function (attr) {
					return attr.$invalid && (!attr.$pristine || attr.$touched);
				};

				/* Login */
				$scope.login = function () {
					$scope.vm.procesando = true;
					$scope.vm.error = "";

					var user = {
						email: $scope.vm.email,
						password: $scope.vm.password
					};

					sessionService.login(user)
						.then(
							function (response) {
								var data = response.data;
								if (data.success)
									$window.location.reload();
								else
									$scope.vm.showMessageError((MESSAGES.LOGIN.FAIL_AUTH));
							},
							function (reason) {
								$scope.vm.showMessageError(MESSAGES.SERVER.ERROR);
							}
						);
				};
			}
		]);