angular.module('app.welcome')
	.service('sessionService',
		['$http','$q', 'API',
			function($http,$q,API) {

				this.getListTypes = function () {
					var deferred = $q.defer();
					deferred.resolve(
						$http({
							method: 'GET',
							url: API.USUARIO.LISTTYPES
						})
					);
					return deferred.promise;
				}

				this.save = function () {
					var deferred = $q.defer();
					deferred.resolve(
						$http({
							method: 'POST',
							url: API.USUARIO.SAVE
						})
					);
					return deferred.promise;
				}

			}
		]);