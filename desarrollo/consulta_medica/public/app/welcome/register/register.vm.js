var VMRegister = function () {

	this.name = '';
	this.email = '';
	this.password = '';

	this.errorMsg = '';
	this.procesando = false;

	this.showMessageError = function (error) {
		this.errorMsg = error;
		this.procesando = false;
	};

	this.hydrate = function (data) {
		if (data) {
			this.lt_seguro = data['lt_seguro']?data['lt_seguro']:this.lt_seguro;
			this.lt_parentesco_jefe_hogar = data['lt_parentesco_jefe_hogar']?data['lt_parentesco_jefe_hogar']:this.lt_parentesco_jefe_hogar;
			this.lt_tipo_identificacion = data['lt_tipo_identificacion']?data['lt_tipo_identificacion']:this.lt_tipo_identificacion;
			this.lt_grupo_sanguineo = data['lt_grupo_sanguineo']?data['lt_grupo_sanguineo']:this.lt_grupo_sanguineo;
			this.lt_nivel_educativo = data['lt_nivel_educativo']?data['lt_nivel_educativo']:this.lt_nivel_educativo;
			this.lt_estado_civil = data['lt_estado_civil']?data['lt_estado_civil']:this.lt_estado_civil;
			this.lt_ocupacion = data['lt_ocupacion']?data['lt_ocupacion']:this.lt_ocupacion;
			this.lt_sexo = data['lt_sexo']?data['lt_sexo']:this.lt_sexo;
			this.lt_rh = data['lt_rh']?data['lt_rh']:this.lt_rh;
		}
	}
};
