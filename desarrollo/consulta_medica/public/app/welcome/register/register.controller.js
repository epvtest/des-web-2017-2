angular.module('app.welcome')
	.controller('RegisterController',
		['$scope', 'MESSAGES', 'sessionService', '$state',
			function ($scope, MESSAGES, sessionService, $state) {

				$scope.vm = new VMRegister();

				$scope.hasErrors = function (attr) {
					return attr.$invalid && (!attr.$pristine || attr.$touched);
				};

				/* Index */
				$scope.getListTypes = function () {
					$scope.vm.error = "";
					
					sessionService.getListTypes()
						.then(
							function (response) {
								var data = response.data;
								$scope.vm.hydrate(data);    // Fill List types
							},
							function (reason) {
								$scope.vm.showMessageError(MESSAGES.SERVER.ERROR);
							}
						);
				};

				/* Login */
				$scope.register = function () {
					$scope.vm.procesando = true;
					$scope.vm.error = "";

					var user = {
						name: $scope.vm.name,
						email: $scope.vm.email,
						password: $scope.vm.password
					};
					var persona = $scope.vm.persona
					var request = {user:user, persona:persona}
					console.log(request)
					try{
					sessionService.save(request)
						.then(
							function (response) {
								var data = response.data;
								if (data)
									$state.go('login');
								else
									$scope.vm.showMessageError((MESSAGES.REGISTER.FAIL));
							},
							function (reason) {
								$scope.vm.showMessageError(MESSAGES.SERVER.ERROR);
								$scope.vm.procesando = false;
							}
						);
					} catch(e) {
						console.log(e)
					}
				};

				$scope.getListTypes();

			}
		]);