angular.module('app.core')
    .constant('API', {
        SESSION : {
            LOGIN: 'api/session/login',
            LOGOUT : 'api/session/logout',
	        REGISTER: 'api/session/register'
        },
        USUARIO : {
            LISTTYPES : 'api/usuario/listtypes',
            SAVE : 'api/usuario/save'
        }
    })
    .constant('MESSAGES', {
    	SERVER: {
    		ERROR: 'Error en el Servidor. Vuelva a intentar'
	    },
    	LOGIN: {
		    FAIL_AUTH: 'Usuario o Password Incorrecto'
	    },
	    REGISTER: {
    		FAIL: 'Error en proceso de registro'
	    }
    });

