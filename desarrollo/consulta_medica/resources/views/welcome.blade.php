@extends('layout') @section('content')



<div class="w3-about about-gap" id="about">
	<div class="container">
		<div class="w3-heading-all">
			<h3>Nosotros</h3>
		</div>
		<div class="w3-about-grids">
			<div class="col-md-6 w3-about-left-grid">
				<div class=" w3-about-left-grid-inner-head">
					<i class="fa fa-microphone" aria-hidden="true"></i>
					<h3>Vamos a hablar del SIS.</h3>
				</div>
				<div class=" w3-about-left-grid-inner2-head">
					<h3 align="justify">Durante estos últimos años, se han
						hecho enormes esfuerzos en mejorar
						nuestro servicio, y en seguir llegando
						a quienes más lo necesitan.</h3>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="col-md-6 w3-about-right-grid">
				<div class="col-md-8 w3-about-right-text1">
					<h5>Usain Bolt</h5>
					<h4>Especialista</h4>
					<h3>Ut sed augue porttitor, vehicula eros in, vehicula elit.
						Aliquam ut ex aliquam risus vestibulum hendrerit.</h3>

				</div>
				<div class="col-md-4 w3-about-right-img1">
					<img src="images/a11.jpg" alt="img" />
				</div>
				<div class="clearfix"></div>
				<div class="col-md-8 w3-about-right-text1">
					<h5>Dunke alpha 212</h5>
					<h4>Especialista</h4>
					<h3>Ut sed augue porttitor, vehicula eros in, vehicula elit.
						Aliquam ut ex aliquam risus vestibulum hendrerit.</h3>
				</div>
				<div class="col-md-4 w3-about-right-img1">
					<img src="images/a121.jpg" alt="img" />
				</div>
				<div class="clearfix"></div>
				<div class="col-md-8 w3-about-right-text1">
					<h5>Monst ibram 143</h5>
					<h4>Specialist3</h4>
					<h3>Ut sed augue porttitor, vehicula eros in, vehicula elit.
						Aliquam ut ex aliquam risus vestibulum hendrerit.</h3>
				</div>
				<div class="col-md-4 w3-about-right-img1">
					<img src="images/a13.jpg" alt="img" />
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //About -->
<!-- /services -->
<div class="services" id="services">
	<div class="container">
		<div class="w3-heading-all services-head">
			<h3>Servicios</h3>
		</div>

		<div class="w3-services-grids">
			<div class="col-md-4 w3-services-grids1">
				<div class="w3-services-grid1">
					<i class="fa fa-user-md" aria-hidden="true"></i>
					<h3>Equipo especializado</h3>
					<div class="w3-services-grid1-left">
						<h4>10</h4>
						<p>ipsum</p>
					</div>
					<div class="w3-services-grid1-right">
						<h4>5</h4>
						<p>basusx</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-md-4 w3-services-grids1 ">
				<div class=" w3-services-grid2">
					<i class="fa fa-laptop" aria-hidden="true"></i>
					<h3>Doctores calificados</h3>
					<div class="w3-services-grid1-left">
						<h4>20</h4>
						<p>basus</p>
					</div>
					<div class="w3-services-grid1-right">
						<h4>10</h4>
						<p>basus</p>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="col-md-4 w3-services-grids1">
				<div class=" w3-services-grid3">
					<i class="fa fa-hospital-o" aria-hidden="true"></i>
					<h3>Infraestructura adecuada</h3>
					<div class="w3-services-grid1-left">
						<h4>15</h4>
						<p>mpsum</p>
					</div>
					<div class="w3-services-grid1-right">
						<h4>20</h4>
						<p>rasus</p>
					</div>
					<div class="clearfix"></div>

				</div>
			</div>

			<div class="clearfix"></div>
			<div class="w3-services-grid-bottom">
				<div class="col-md-4 w3-services-grids1">
					<div class="w3-services-grid4">
						<i class="fa fa-heartbeat" aria-hidden="true"></i>
						<h3>Especialización cardíaca</h3>
						<div class="w3-services-grid1-left">
							<h4>25</h4>
							<p>ipsum</p>
						</div>
						<div class="w3-services-grid1-right">
							<h4>30</h4>
							<p>basusx</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-4 w3-services-grids1">
					<div class=" w3-services-grid5">
						<i class="fa fa-flask" aria-hidden="true"></i>
						<h3>Clínica Pediátrica</h3>
						<div class="w3-services-grid1-left">
							<h4>35</h4>
							<p>ipsum</p>
						</div>
						<div class="w3-services-grid1-right">
							<h4>25</h4>
							<p>basusx</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-md-4 w3-services-grids1">
					<div class=" w3-services-grid6">
						<i class="fa fa-ambulance" aria-hidden="true"></i>
						<h3>Atención inmediata en emergencias</h3>
						<div class="w3-services-grid1-left">
							<h4>40</h4>
							<p>ipsum</p>
						</div>
						<div class="w3-services-grid1-right">
							<h4>45</h4>
							<p>basusx</p>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>

				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
<!-- //services -->
<!-- testimonials -->
<div class="testimonials" id="testimonials">
	<div class="container">
		<div class="w3-heading-all">
			<h3>Testimonios</h3>
		</div>
		<div class="w3ls_testimonials_grids">
			<section class="center slider">
				<div class="agileits_testimonial_grid">
					<div class="w3l_testimonial_grid">
						<p>Realmente, una atención de primera y la atención excelente.
						Me ayudaron a sobrellevar mi terapia de rehabilitación.</p>
						<h4>Fernando Rivas</h4>
						<h5>Empresario</h5>
						<div class="w3l_testimonial_grid_pos">
							<img src="images/tm1.jpg" alt=" " class="img-responsive" />
						</div>
					</div>
				</div>
				<div class="agileits_testimonial_grid">
					<div class="w3l_testimonial_grid">
						<p>Los doctores son muy amables, y te atienden rápido cuando
						más los necesitas.</p>
						<h4>Laura Martínez</h4>
						<h5>Jubilada</h5>
						<div class="w3l_testimonial_grid_pos">
							<img src="images/tm2.jpg" alt=" " class="img-responsive" />
						</div>
					</div>
				</div>
				<div class="agileits_testimonial_grid">
					<div class="w3l_testimonial_grid">
						<p>El diagnóstico preciso, junto a las pastillas correctas.
						Además de la buena disposición del personal.</p>
						<h4>Francisco Arias</h4>
						<h5>Profesor</h5>
						<div class="w3l_testimonial_grid_pos">
							<img src="images/tm3.jpg" alt=" " class="img-responsive" />
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</div>
<!-- //testimonials -->
@stop
