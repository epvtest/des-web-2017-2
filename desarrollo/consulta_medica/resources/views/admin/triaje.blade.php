@extends('admin.layout')

@section('content')

<h1>Triaje</h1>
<p>Usuario autenticado :  {{auth()->user()->name}} </p>

<form class="form-horizontal">
<fieldset>

<div class="form-group">
  <label class="col-md-4 control-label" for="paciente">Paciente</label>  
  <div class="col-md-4">
  	<input id="paciente" name="paciente" type="text" placeholder="" class="form-control input-md" disabled>
  </div>
</div>

<div class="form-group">
  <label class="col-md-5 control-label" for="temperatura">Temperatura</label>  
  <div class="col-md-2">
  	<input id="temperatura" name="temperatura" type="text" placeholder="En °C" class="form-control input-md" required>
  </div>
</div>

<div class="form-group">
  <label class="col-md-5 control-label" for="presion_arterial">Presión arterial</label>  
  <div class="col-md-2">
  	<input id="presion_arterial" name="presion_arterial" type="text" placeholder="" class="form-control input-md" required>
  </div>
</div>

<div class="form-group">
  <label class="col-md-5 control-label" for="frecuencia_cardiaca">Frecuencia cardíaca</label>  
  <div class="col-md-2">
	<input id="frecuencia_cardiaca" name="frecuencia_cardiaca" type="text" placeholder="Por minuto" class="form-control input-md" required>
  </div>
</div>

<div class="form-group">
  <label class="col-md-5 control-label" for="frecuencia_respiratoria">Frecuencia respiratoria</label>  
  <div class="col-md-2">
  	<input id="frecuencia_respiratoria" name="frecuencia_respiratoria" type="text" placeholder="Por minuto" class="form-control input-md" required>
  </div>
</div>

<div class="form-group">
  <label class="col-md-5 control-label" for="talla">Talla</label>  
  <div class="col-md-2">
  	<input id="talla" name="talla" type="text" placeholder="En centímetros" class="form-control input-md" required>
  </div>
</div>

<div class="form-group">
  <label class="col-md-5 control-label" for="peso">Peso</label>  
  <div class="col-md-2">
  	<input id="peso" name="peso" type="text" placeholder="En kilogramos" class="form-control input-md" required>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="doctor">Realizado por</label>  
  <div class="col-md-4">
  	<input id="doctor" name="doctor" type="text" placeholder="" class="form-control input-md" disabled>
  </div>
</div>

<!-- Botones de acción -->
<div class="form-group">
  <label class="col-md-2 control-label" for="btnEditar"></label>
  <div class="col-md-2" align="center">
    <button id="btnEditar" name="btnEditar" class="btn btn-primary">Editar</button>
  </div>
  <div class="col-md-2" align="center">
    <input id="btnLimpiar" name="btnLimpiar" class="btn btn-primary" type="reset" value="Limpiar">
  </div>
  <div class="col-md-2" align="center">
    <input id="btnGuardar" name="btnGuardar" class="btn btn-primary" type="submit" value="Guardar">
  </div>
  <div class="col-md-2" align="center">
    <button id="btnCancelar" name="btnCancelar" class="btn btn-primary">Cancelar</button>
  </div>
</div>


</fieldset>
</form>

@stop
