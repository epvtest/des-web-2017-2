@extends('admin.layout')

@section('content')

<h1>Historia clínica</h1>
<p>Usuario autenticado :  {{auth()->user()->name}} </p>
<form class="form-horizontal">
<fieldset>


<!-- datos de cabecera -->
<div class="form-group">
  <label class="col-md-1 control-label" for="paciente">Paciente</label>  
  <div class="col-md-4">
  	<input id="paciente" name="paciente" type="text" class="form-control input-md" disabled>
  </div>

  <label class="col-md-1 control-label" for="id">HC</label>  
  <div class="col-md-2">
  	<input id="id" name="id" type="text" class="form-control input-md" disabled>
  </div>
  
  <label class="col-md-1 control-label" for="fichafamiliar">Ficha familiar</label>  
  <div class="col-md-2">
  	<input id="fichafamiliar" name="fichafamiliar" type="text" class="form-control input-md" disabled>
  </div>
</div>

<!-- Identificación -->
<div class="form-group">
  <label class="col-md-2 control-label" for="lt_tipo_documento">Tipo de documento</label>
  <div class="col-md-2">
    <select id="lt_tipo_documento" name="lt_tipo_documento" class="form-control" disabled>
    </select>
  </div>
  <label class="col-md-2 control-label" for="numero_documento">Número de documento</label>  
  <div class="col-md-2">
  <input id="numero_documento" name="numero_documento" type="text" class="form-control input-md" disabled>
  </div>
</div>

<!-- Identificación en el sistema de seguros -->
<div class="form-group">
  
  <label class="col-md-1 control-label" for="seguro">¿Tiene seguro?</label>
  <div class="col-md-1"> 
    <label class="radio-inline" for="seguro-0">
      <input type="radio" name="seguro" id="seguro-0" value="si" checked="checked">
      Sí
    </label> 
    <label class="radio-inline" for="seguro-1">
      <input type="radio" name="seguro" id="seguro-1" value="no">
      No
    </label>
  </div>
  
  <label class="col-md-1 control-label" for="sis">¿Afiliado SIS?</label>
  <div class="col-md-1"> 
    <label class="radio-inline" for="sis-0">
      <input type="radio" name="sis" id="sis-0" value="si" checked="checked">
      Sí
    </label> 
    <label class="radio-inline" for="sis-1">
      <input type="radio" name="sis" id="sis-1" value="no">
      No
    </label>
  </div>

  <label class="col-md-2 control-label" for="numero_seguro">Número de seguro</label>  
  <div class="col-md-2">
    <input id="numero_seguro" name="numero_seguro" type="text" placeholder="" class="form-control input-md" disabled>
  </div>
</div>

<!-- Datos de emergencia, de serlo -->
<div class="form-group">
  <label class="col-md-1 control-label" for="emergencia-0"></label>  
  <div class="col-md-1">
  <div class="checkbox">
    <label for="emergencia-0">
      <input type="checkbox" name="emergencia" id="emergencia-0" value="ind_emergencia">
      Emergencia
    </label>
	</div>
  </div>
  
  <label class="col-md-2 control-label" for="motivo_emergencia">Motivo</label>  
  <div class="col-md-3">
	<input id="motivo_emergencia" name="motivo_emergencia" type="text" placeholder="Ingrese el motivo de la emergencia" class="form-control input-md">
  </div>

  <label class="col-md-2 control-label" for="causaext">Causa externa</label>
  <div class="col-md-2">
    <select id="causaext" name="causaext" class="form-control">
    </select>
  </div>
</div>

<!-- Datos de referencia -->
<div class="form-group">
  <label class="col-md-1 control-label" for="referencia-0"></label>  
  <div class="col-md-1">
  <div class="checkbox">
    <label for="referencia-0">
      <input type="checkbox" name="referencia" id="referencia-0" value="ind_referencia">
      Referencia
    </label>
	</div>
  </div>
  
  <label class="col-md-2 control-label" for="motivo_referencia">Motivo</label>  
  <div class="col-md-3">
	<input id="motivo_referencia" name="motivo_referencia" type="text" placeholder="Ingrese el motivo de la referencia" class="form-control input-md">
  </div>

  <label class="col-md-2 control-label" for="fecha_referencia">Fecha</label>  
  <div class="col-md-2">
	<input id="fecha_referencia" name="fecha_referencia" type="date" placeholder="Ingrese la fecha de la referencia." class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="btnTriaje"></label>
  <div class="col-md-8">
    <button id="btnTriaje" name="btnTriaje" class="btn btn-primary"><span class="fa fa-heartbeat"></span> Triaje</button>
    <button id="btnExamenes" name="btnExamenes" class="btn btn-primary"><span class="fa fa-stethoscope"></span> Exámenes Auxiliares</button>
  </div>
</div>

<legend>Consulta</legend>

<!-- Enfermedad -->
<div class="form-group">
  <label class="col-md-2 control-label" for="motivo_enf">Motivo</label>  
  <div class="col-md-3">
  	<input id="motivo_enf" name="motivo_enf" type="text" placeholder="Ingrese el motivo de su consulta" class="form-control input-md">    
  </div>

  <label class="col-md-2 control-label" for="tiempo_enfermedad">Tiempo de enfermedad</label>  
  <div class="col-md-3">
  	<input id="tiempo_enfermedad" name="tiempo_enfermedad" type="text" placeholder="¿Cuánto tiempo lleva con la enfermedad?" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="apetito">Apetito</label>  
  <div class="col-md-3">
  	<input id="apetito" name="apetito" type="text" placeholder="Apetito" class="form-control input-md">    
  </div>

  <label class="col-md-2 control-label" for="sed">Sed</label>  
  <div class="col-md-3">
  	<input id="sed" name="sed" type="text" placeholder="Sed" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="suenho">Sueño</label>  
  <div class="col-md-3">
  	<input id="suenho" name="suenho" type="text" placeholder="Sueño" class="form-control input-md">    
  </div>

  <label class="col-md-2 control-label" for="estado_animo">Estado de ánimo</label>  
  <div class="col-md-3">
  	<input id="estado_animo" name="estado_animo" type="text" placeholder="Estado de ánimo" class="form-control input-md">
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="orina">Orina</label>  
  <div class="col-md-3">
  	<input id="orina" name="orina" type="text" placeholder="Orina" class="form-control input-md">    
  </div>

  <label class="col-md-2 control-label" for="deposiciones">Deposiciones</label>  
  <div class="col-md-3">
  	<input id="deposiciones" name="deposiciones" type="text" placeholder="Deposiciones" class="form-control input-md">
  </div>
</div>

<!-- Médico que atendió la consulta -->
<div class="form-group">
  <label class="col-md-2 control-label" for="medico">Médico</label>  
  <div class="col-md-8">
  	<input id="medico" name="medico" type="text" class="form-control input-md" disabled>    
  </div>
</div>

<!-- Observaciones -->
<div class="form-group">
  <label class="col-md-2 control-label" for="observaciones">Observaciones</label>
  <div class="col-md-8">                     
    <textarea class="form-control" id="observaciones" name="observaciones"></textarea>
  </div>
</div>

<div class="form-group">
  <label class="col-md-2 control-label" for="diagnostico"></label>
  <div class="col-md-2" align="center">
    <button id="diagnostico" name="diagnostico" class="btn btn-primary"><span class="fa fa-stethoscope"></span> Diagnóstico</button>
  </div>
  <div class="col-md-2" align="center">
    <button id="diagnostico" name="diagnostico" class="btn btn-primary"><span class="ionicons ion-ios-paper-outline"></span> Tratamiento</button>
  </div>
  <div class="col-md-2" align="center">
    <button id="diagnostico" name="diagnostico" class="btn btn-primary"><span class="fi-check"></span> Alta</button>
  </div>
  <div class="col-md-2" align="center">
    <button id="diagnostico" name="diagnostico" class="btn btn-primary"><span class="fi-clipboard-pencil"></span> Editar</button>
  </div>
</div>

</fieldset>
</form>

@stop
