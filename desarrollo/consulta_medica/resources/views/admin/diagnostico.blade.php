@extends('admin.layout')

@section('content')

<h1>Diagnóstico y tratamiento</h1>
<p>Usuario autenticado :  {{auth()->user()->name}} </p>

<form class="form-horizontal">
<fieldset>

<div class="form-group">
  <label class="col-md-4 control-label" for="paciente">Paciente</label>  
  <div class="col-md-4">
  	<input id="paciente" name="paciente" type="text" placeholder="" class="form-control input-md" disabled>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="doctor">Realizado por</label>
  <div class="col-md-4">
  	<input id="doctor" name="doctor" type="text" placeholder="" class="form-control input-md" disabled>
  </div>
</div>

</fieldset>
</form>

@stop
