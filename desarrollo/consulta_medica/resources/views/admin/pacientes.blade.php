@extends('admin.layout') @section('header')
<h1>
	Pacientes <small>Control de Pacientes</small>
</h1>
<ol class="breadcrumb">
	<li><a href="#"><i class="fa fa-dashboard"></i> Admin</a></li>
	<li class="active">Pacientes</li>
</ol>
@stop @section('content')

<h1>Pacientes</h1>
<p>Usuario autenticado : {{auth()->user()->name}}</p>
<form class="form-horizontal">
<fieldset>

<legend>Búsqueda de pacientes</legend>

<!-- Datos de paciente -->
<div class="form-group">
  <label class="col-md-2 control-label" for="lt_tipo_documento">Tipo de documento</label>
  <div class="col-md-2">
    <select id="lt_tipo_documento" name="lt_tipo_documento" class="form-control" required>
    </select>
  </div>
  <label class="col-md-2 control-label" for="numero_documento">Número de documento</label>  
  <div class="col-md-2">
  <input id="numero_documento" name="numero_documento" type="text" placeholder="Ingresa aquí tu número de documento" class="form-control input-md" required>
  </div>
</div>

<div class="form-group">
  <label class="col-md-4 control-label" for="button1id"></label>
  <div class="col-md-8">
    <button id="button1id" name="button1id" class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Buscar</button>
    <button id="button2id" name="button2id" class="btn btn-danger"><span class="glyphicon glyphicon-erase"></span> Limpiar</button>
    <button id="button3id" name="button3id" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Nuevo</button>
  </div>
</div>

</fieldset>
</form>

@stop
