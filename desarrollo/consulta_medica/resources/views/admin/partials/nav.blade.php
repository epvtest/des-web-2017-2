<!-- Sidebar Menu -->
<ul class="sidebar-menu" data-widget="tree">
	<li class="header">Navegacion</li>
	<!-- Optionally, you can add icons to the links -->
	<li {{ request()->is('admin') ? 'class=active' : ''}}><a href="{{route('administrador')}}"><i class="fa fa-home"></i> <span>Inicio</span></a></li>
	
	<li class="treeview {{ request()->is('admin/pacientes') ? 'active' : ''}}"><a href="#"><i class="fa fa-hospital-o"></i> <span>Busqueda</span>
			<span class="pull-right-container"> <i
				class="fa fa-angle-left pull-right"></i>
		</span> </a>
		<ul class="treeview-menu ">
			<li {{ request()->is('admin/pacientes') ? 'class=active' : ''}}><a href="{{route('pacientes')}}">Pacientes</a></li>
			
		</ul></li>
		<li class="treeview {{ !request()->is('admin/pacientes') ? 'active' : ''}}"><a href="#"><i class="fa fa-user-md"></i> <span>Gestion</span>
			<span class="pull-right-container"> <i
				class="fa fa-angle-left pull-right"></i>
		</span> </a>
		<ul class="treeview-menu">
			<li  {{ request()->is('admin/fese') ? 'class=active' : ''}}><a href="{{route('fese')}}">Ficha familiar</a></li>
			<li  {{ request()->is('admin/historias') ? 'class=active' : ''}}><a href="{{route('historias')}}">Historia Clinica</a></li>
			<li  {{ request()->is('admin/triaje') ? 'class=active' : ''}}><a href="{{route('triaje')}}">Triaje</a></li>
			<li  {{ request()->is('admin/examenes') ? 'class=active' : ''}}><a href="{{route('examenes')}}">Examen Auxiliar</a></li>
			<li  {{ request()->is('admin/diagnostico') ? 'class=active' : ''}}><a href="{{route('diagnostico')}}">Diagnostico</a></li>
		</ul></li>
</ul>
<!-- /.sidebar-menu -->