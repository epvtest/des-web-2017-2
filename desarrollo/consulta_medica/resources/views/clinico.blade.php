<html ng-app="app"
      lang="{{ app()->getLocale() }}">
<head>
	<base href="/public">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>Sistema Clínico</title>
	{{-- Fonts --}}
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
	{{-- Styles --}}
	<link rel="stylesheet" href="{{asset('lib/bootstrap/dist/css/bootstrap.css')}}" type="text/css">
	<link rel="stylesheet" href="{{asset('lib/bootstrap/dist/css/bootstrap-theme.css')}}" type="text/css">
	<link rel="stylesheet" href="{{asset('css/styles.css')}}" type="text/css">

	{{-- Vendor Scripts --}}
	<script src="{{asset('lib/jquery/dist/jquery.js')}}"></script>
	<script src="{{asset('lib/lodash/lodash.min.js')}}"></script>
	<script src="{{asset('lib/angular/angular.min.js')}}"></script>
	<script src="{{asset('lib/angular-animate/angular-animate.min.js')}}"></script>
	<script src="{{asset('lib/angular-route/angular-route.min.js')}}"></script>
	<script src="{{asset('lib/angular-cookies/angular-cookies.min.js')}}"></script>
	<script src="{{asset('lib/angular-ui-router/release/angular-ui-router.min.js')}}"></script>
	<script src="{{asset('lib/angular-bootstrap/ui-bootstrap.min.js')}}"></script>
	<script src="{{asset('lib/angular-bootstrap/ui-bootstrap-tpls.min.js')}}"></script>
	<script src="{{asset('lib/bootstrap/dist/js/bootstrap.js')}}"></script>

	{{-- App Scripts --}}
	<script src="{{asset('app/_core/core.module.js')}}"></script>
	<script src="{{asset('app/_core/session.service.js')}}"></script>

	<script src="{{asset('app/clinico/app.clinico.js')}}"></script>
	<script src="{{asset('app/clinico/_core/clinico.core.module.js')}}"></script>
	<script src="{{asset('app/clinico/_core/clinico.constants.js')}}"></script>
	<script src="{{asset('app/clinico/_core/clinico.controller.js')}}"></script>

	{{-- Models --}}
	<script src="{{asset('app/clinico/_model/lt.model.js')}}"></script>
	<script src="{{asset('app/clinico/_model/paciente.model.js')}}"></script>
	<script src="{{asset('app/clinico/_model/persona.model.js')}}"></script>

	{{-- Paciente --}}
	<script src="{{asset('app/clinico/paciente/paciente.vm.js')}}"></script>
	<script src="{{asset('app/clinico/paciente/paciente.service.js')}}"></script>
	<script src="{{asset('app/clinico/paciente/paciente.controller.js')}}"></script>


</head>

<body>
<div class="navbar-wrapper" ng-controller="ClinicoController">
	<div class="container-fluid">
		<nav class="navbar navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
					        data-toggle="collapse" data-target="#navbar"
					        aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#">Logo</a>
				</div>

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a ui-sref="home">Home</a></li>
						<li><a ui-sref="paciente">Paciente</a></li>
						{{--<li><a ui-sref="acerca">Acerca</a></li>--}}
					</ul>

					<ul class="nav navbar-nav pull-right">
						<li><a href="">Martin More</a></li>
						<li><a href="" ng-click="logout()">Logout</a></li>
					</ul>
				</div>
			</div>
		</nav>
	</div>
</div>

<div class="padding-60" ui-view ng-clock>
</div>

</body>
</html>