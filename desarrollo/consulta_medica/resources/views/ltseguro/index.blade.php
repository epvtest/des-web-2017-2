<h1 class="text-primary">Lista de Seguros</h1>
 
<table class="table table-bordered" id="tableMonedas">
  <thead>
    <tr>
        <th class="text-center">Id Seguro</th>
        <th class="text-center">Código</th>
        <th class="text-center">Nombre</th>
        <th class="text-center">Descripción</th>
        <th class="text-center">Prioridad</th>
    </tr>
  </thead>
  <tbody>
    @foreach($ltseguro as $seguro)
        <tr>
            <td class="text-center">{{ $seguro->id}}</td>
            <td class="text-center">{{ $seguro->codigo}}</td>
            <td class="text-center">{{ $seguro->nombre }}</td>
            <td class="text-center">{{ $seguro->descripcion}}</td>
            <td class="text-center">{{ $seguro->prioridad}}</td>
        </tr>
    @endforeach
  </tbody>
  <tfoot>
    <tr>
      <th class="text-center">Id Seguro</th>
      <th class="text-center">Código</th>
      <th class="text-xcenter">Nombre</th>
      <th class="text-center">Descripción</th>
      <th class="text-center">Prioridad</th>
    </tr>
  </tfoot>
</table>