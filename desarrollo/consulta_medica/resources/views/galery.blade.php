
@extends('layout') @section('content')
<div class="section-w3ls agileits-gallery" id="portfolio">
	<div class="w3-heading-all">
				<h3>Our gallery</h3>
			</div>
		<div class="gallery-w3layouts">
			<div class="col-sm-3 w3_tab_img_left">
				<div class="demo">
					<a class="cm-overlay" href="images/g1.jpg">
						<figure class="imghvr-shutter-in-out-diag-2">
							<img src="images/g1.jpg" alt=" " class="img-responsive" />
						</figure>
					</a>
				</div>
				<div class="agile-gallery-info">
					<h5>MediBulk</h5>
					<p>we are innovation driven</p>
				</div>
			</div>
			<div class="col-sm-3 w3_tab_img_left">
				<div class="demo">
					<a class="cm-overlay" href="images/g2.jpg">
						<figure class="imghvr-shutter-in-out-diag-2">
							<img src="images/g2.jpg" alt=" " class="img-responsive" />
						</figure>
					</a>
				</div>
				<div class="agile-gallery-info">
					<h5>MediBulk</h5>
					<p>we are innovation driven </p>
				</div>
			</div>
			<div class="col-sm-3 w3_tab_img_left">
				<div class="demo">
					<a class="cm-overlay" href="images/g3.jpg">
						<figure class="imghvr-shutter-in-out-diag-2">
							<img src="images/g3.jpg" alt=" " class="img-responsive" />
						</figure>
					</a>
				</div>
				<div class="agile-gallery-info">
					<h5>MediBulk</h5>
					<p>we are innovation driven</p>
				</div>
			</div>
			<div class="col-sm-3 w3_tab_img_left">
				<div class="demo">
					<a class="cm-overlay" href="images/g4.jpg">
						<figure class="imghvr-shutter-in-out-diag-2">
							<img src="images/g4.jpg" alt=" " class="img-responsive" />
						</figure>
					</a>
				</div>
				<div class="agile-gallery-info">
					<h5>MediBulk</h5>
					<p>we are innovation driven</p>
				</div>
			</div>
			<div class="clearfix"></div>
			<div class="col-sm-3 w3_tab_img_left">
				<div class="demo">
					<a class="cm-overlay" href="images/g5.jpg">
						<figure class="imghvr-shutter-in-out-diag-2">
							<img src="images/g5.jpg" alt=" " class="img-responsive" />
						</figure>
					</a>
				</div>
				<div class="agile-gallery-info">
					<h5>MediBulk</h5>
					<p>we are innovation driven</p>
				</div>
			</div>
			<div class="col-sm-3 w3_tab_img_left">
				<div class="demo">
					<a class="cm-overlay" href="images/g8.jpg">
						<figure class="imghvr-shutter-in-out-diag-2">
							<img src="images/g8.jpg" alt=" " class="img-responsive" />
						</figure>
					</a>
				</div>
				<div class="agile-gallery-info">
					<h5>MediBulk</h5>
					<p>we are innovation driven</p>
				</div>
			</div>
			<div class="col-sm-3 w3_tab_img_left">
				<div class="demo">
					<a class="cm-overlay" href="images/g7.jpg">
						<figure class="imghvr-shutter-in-out-diag-2">
							<img src="images/g7.jpg" alt=" " class="img-responsive" />
						</figure>
					</a>
				</div>
				<div class="agile-gallery-info">
					<h5>MediBulk</h5>
					<p>we are innovation driven</p>
				</div>
			</div>
			<div class="col-sm-3 w3_tab_img_left">
				<div class="demo">
					<a class="cm-overlay" href="images/g6.jpg">
						<figure class="imghvr-shutter-in-out-diag-2">
							<img src="images/g6.jpg" alt=" " class="img-responsive" />
						</figure>
					</a>
				</div>
				<div class="agile-gallery-info">
					<h5>MediBulk</h5>
					<p>we are innovation driven</p>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
	<!-- //menu -->
@stop