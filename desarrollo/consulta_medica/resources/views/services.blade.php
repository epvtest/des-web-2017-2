@extends('layout') 

@section('content')

<div class="banner-bottom">
	<div class="container">
		<div class="w3-heading-all">
			<h3>Special Services</h3>
		</div>
		<div class="col-md-4 w3l_services_footer_top_left">
			<img src="images/sp5.jpg" alt=" " class="img-responsive" />
		</div>
		<div class="col-md-8 w3l_services_footer_top_right">
			<div class="w3l_services_footer_top_right_main">
				<div class="w3l_services_footer_top_right_main_l">
					<h3>20 January 2018</h3>
				</div>
				<div class="w3l_services_footer_top_right_main_l1">
					<div class="w3ls_service_icon">
						<i class="fa fa-globe" aria-hidden="true"></i>
					</div>
				</div>
				<div class="w3l_services_footer_top_right_main_r">
					<a href="#">Maximus pretium </a>
					<p>Morbi sollicitudin odio massa, et rutrum sem rutrum in. Duis
						elementum turpis ultricies, finibus leo.</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="w3l_services_footer_top_right_main">
				<div class="w3l_services_footer_top_right_main_l">
					<h3>25 January 2018</h3>
				</div>
				<div class="w3l_services_footer_top_right_main_l1">
					<div class="w3ls_service_icon">
						<i class="fa fa-map" aria-hidden="true"></i>
					</div>
				</div>
				<div class="w3l_services_footer_top_right_main_r">
					<a href="#">Aliquam faucibus </a>
					<p>Morbi sollicitudin odio massa, et rutrum sem rutrum in. Duis
						elementum turpis ultricies, finibus leo.</p>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="w3l_services_footer_top_right_main">
				<div class="w3l_services_footer_top_right_main_l">
					<h3>30 January 2018</h3>
				</div>
				<div class="w3l_services_footer_top_right_main_l1">
					<div class="w3ls_service_icon">
						<i class="fa fa-pie-chart" aria-hidden="true"></i>
					</div>
				</div>
				<div class="w3l_services_footer_top_right_main_r">
					<a href="#">Vitae faucibus</a>
					<p>Morbi sollicitudin odio massa, et rutrum sem rutrum in. Duis
						elementum turpis ultricies, finibus leo.</p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!-- //offer-bottom -->
<!-- offer -->
<div class="offer">
	<div class="container">
		<div class="w3ls_banner_bottom_grids">
			<div class="col-md-4 agile_offer_grid">
				<div class="agileits_w3layouts_offer_grid">
					<div class="agile_offer_grid_pos">
						<p>01.</p>
					</div>
				</div>
				<div class="wthree_offer_grid1">
					<h4>Operation Theatre</h4>
					<p class="w3_agileits_service_para">Praesent nec blandit lorem, et
						facilisis mi. Ut fringilla massa massa, id consequat eros iaculis
						quis.</p>
				</div>
			</div>
			<div class="col-md-4 agile_offer_grid">
				<div class="agileits_w3layouts_offer_grid">
					<div class="agile_offer_grid_pos">
						<p>02.</p>
					</div>
				</div>
				<div class="wthree_offer_grid1">
					<h4>Outdoor Checkup</h4>
					<p class="w3_agileits_service_para">Praesent nec blandit lorem, et
						facilisis mi. Ut fringilla massa massa, id consequat eros iaculis
						quis.</p>
				</div>
			</div>
			<div class="col-md-4 agile_offer_grid">
				<div class="agileits_w3layouts_offer_grid">
					<div class="agile_offer_grid_pos">
						<p>03.</p>
					</div>
				</div>
				<div class="wthree_offer_grid1">
					<h4>Cancer Service</h4>
					<p class="w3_agileits_service_para">Praesent nec blandit lorem, et
						facilisis mi. Ut fringilla massa massa, id consequat eros iaculis
						quis.</p>
				</div>
			</div>

			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //offer -->
<!-- testimonials -->
<div class="testimonials" id="testimonials">
	<div class="container">
		<div class="w3-heading-all">
			<h3>Testimonials</h3>
			<div class="w3ls_testimonials_grids">
				<section class="center slider">
					<div class="agileits_testimonial_grid">
						<div class="w3l_testimonial_grid">
							<p>In eu auctor felis, id eleifend dolor. Integer bibendum dictum
								erat, non laoreet dolor.</p>
							<h4>Rosy Crisp</h4>
							<h5>Student</h5>
							<div class="w3l_testimonial_grid_pos">
								<img src="images/tm1.jpg" alt=" " class="img-responsive" />
							</div>
						</div>
					</div>
					<div class="agileits_testimonial_grid">
						<div class="w3l_testimonial_grid">
							<p>In eu auctor felis, id eleifend dolor. Integer bibendum dictum
								erat, non laoreet dolor.</p>
							<h4>Laura Paul</h4>
							<h5>Student</h5>
							<div class="w3l_testimonial_grid_pos">
								<img src="images/tm2.jpg" alt=" " class="img-responsive" />
							</div>
						</div>
					</div>
					<div class="agileits_testimonial_grid">
						<div class="w3l_testimonial_grid">
							<p>In eu auctor felis, id eleifend dolor. Integer bibendum dictum
								erat, non laoreet dolor.</p>
							<h4>Michael Doe</h4>
							<h5>Student</h5>
							<div class="w3l_testimonial_grid_pos">
								<img src="images/tm3.jpg" alt=" " class="img-responsive" />
							</div>
						</div>
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
<!-- //testimonials -->
@stop
