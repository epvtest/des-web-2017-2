@extends('layout') @section('content')
<div class="about">
	<div class="container">
		<div class="w3-heading-all">
			<h3>Nosotros</h3>
		</div>
		<div class="ab-agile">
			<div class="col-md-6 aboutleft">
				<h3>Te damos la bienvenida al SIS</h3>
				<p class="para1">Somos una institución comprometida con el salud y
				el bienestar, tanto de ti como los tuyos. Garantizamos:</p>
				<p>
					<i class="fa fa-check" aria-hidden="true"></i>Excelente atención
				</p>
				<p>
					<i class="fa fa-check" aria-hidden="true"></i>Equipos de vanguardia,
					ayudan a diagnosticar la mayoría de las enfermedades.
				</p>
				<p>
					<i class="fa fa-check" aria-hidden="true"></i>Sólida infraestructura
				</p>
			</div>
			<div class="col-md-6 aboutright">
				<img src="images/p1.jpg" class="img-responsive" alt="" />
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
<!-- //about -->
<!-- wthree-mid -->
<div class="wthree-mid">
	<div class="container">
		<h3>Tu salud nos importa</h3>
		<p>😊</p>
	</div>
</div>
<!-- //wthree-mid -->
<!--/team-->
<div class="team" id="team">
	<div class="w3-heading-all">
		<h3>Nuestros doctores</h3>
	</div>
	<div class="w3-agile-team-grids">
		<div class="w3-agile-team-grid1">
			<div class="col-md-4 w3-agile-team-img1">
				<div class="w3-agile-team-img-head">

					<h3>Joe Root</h3>
					<div class="team-social">
						<a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i
							class="fa fa-linkedin"></i></a> <a href="#"><i
							class="fa fa-pinterest-p"></i></a>
					</div>

				</div>
			</div>
			<div class="col-md-4 w3-agile-team-img1 w3-agile-team-img-1">
				<div class="w3-agile-team-img-head">
					<h3>perry jest</h3>
					<div class="team-social">
						<a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i
							class="fa fa-linkedin"></i></a> <a href="#"><i
							class="fa fa-pinterest-p"></i></a>
					</div>
				</div>

			</div>
			<div class="col-md-4 w3-agile-team-img1 w3-agile-team-img-2">
				<div class="w3-agile-team-img-head">
					<h3>Shakeera ls</h3>
					<div class="team-social">
						<a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i
							class="fa fa-linkedin"></i></a> <a href="#"><i
							class="fa fa-pinterest-p"></i></a>
					</div>
				</div>

			</div>
			<div class="clearfix"></div>
		</div>
		<div class="w3-agile-team-grid2">
			<div class="col-md-4 w3-agile-team-img1 w3-agile-team-img-3">
				<div class="w3-agile-team-img-head">
					<h3>sharapova</h3>
					<div class="team-social">
						<a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i
							class="fa fa-linkedin"></i></a> <a href="#"><i
							class="fa fa-pinterest-p"></i></a>
					</div>
				</div>

			</div>
			<div class="col-md-4 w3-agile-team-img1 w3-agile-team-img-4">
				<div class="w3-agile-team-img-head">
					<h3>serina willams</h3>
					<div class="team-social">
						<a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i
							class="fa fa-linkedin"></i></a> <a href="#"><i
							class="fa fa-pinterest-p"></i></a>
					</div>
				</div>
			</div>
			<div class="col-md-4 w3-agile-team-img1 w3-agile-team-img-5">
				<div class="w3-agile-team-img-head">
					<h3>Usain bolt</h3>
					<div class="team-social">
						<a href="#"><i class="fa fa-twitter"></i></a> <a href="#"><i
							class="fa fa-linkedin"></i></a> <a href="#"><i
							class="fa fa-pinterest-p"></i></a>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<!--//team-->
@stop
