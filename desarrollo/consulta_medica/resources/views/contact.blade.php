@extends('layout') @section('content')
<div class="contact-page-w3ls inner-padding">
	<div class="container">
	<div class="w3-heading-all">
				<h3>Escríbemos un correo</h3>
			</div>
	<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d62423.5867979333!2d-77.05392458903486!3d-12.079656658997903!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sSIS!5e0!3m2!1ses!2spe!4v1523113634071" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
<div class="contact-info-w3ls">
	<div class="contact-left-w3layouts">
			<div class="contact-w3-agileits">
				<img src="images/c1.jpg" alt="img">
				<div class="right-contact-w3ls">
					<h6>Daniel</h6>
					<p class="work-w3">Director</p>
					<div class="span-sub-w3ls">
						<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span><p class="number-w3">+51 969 696 969</p>
					</div>
					<div class="span-sub-w3ls">
						<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:info@example.com">daniel.mora@sis.gob.pe</a>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="contact-w3-agileits">
				<img src="images/c2.jpg" alt="img">
				<div class="right-contact-w3ls">
					<h6>Smith Carl</h6>
					<p class="work-w3">Especialista</p>
					<div class="span-sub-w3ls">
						<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span><p class="number-w3">+51 969 969 969</p>
					</div>
					<div class="span-sub-w3ls">
						<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:info@example.com">smith.carl@sis.gob.pe</a>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="contact-w3-agileits">
				<img src="images/c3.jpg" alt="img">
				<div class="right-contact-w3ls">
					<h6>James Mac</h6>
					<p class="work-w3">Doctor</p>
					<div class="span-sub-w3ls">
						<span class="glyphicon glyphicon-earphone" aria-hidden="true"></span><p class="number-w3">+51 969 969 969</p>
					</div>
					<div class="span-sub-w3ls">
						<span class="glyphicon glyphicon-envelope" aria-hidden="true"></span><a href="mailto:info@example.com">james.mac@sis.gob.e</a>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
	</div>
	<div class="contact-right-w3layouts">
	<h5 class="title-w3">¡Nos encantaría escucharte!</h5>
	<p class="head-w3-agileits">Si deseas comunicarte con nosotros, no dudes en llamarnos o llenar el formulario mostrado debajo y nosotros te contactaremos.</p>
		<form action="#" method="post">
			<input type="text" name="your name" placeholder="Tu nombre" required="">
			<input type="email" name="your email" placeholder="Tu correo" required="">
			<textarea name="your message" placeholder="Tu mensaje" required=""></textarea>
			<input type="submit" value="Enviar mensaje">
		</form>
	</div>
	<div class="clearfix"> </div>
	</div>
	<!---728x90--->
	</div>
</div>
@stop