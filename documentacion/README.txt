Se debe analizar y mejorar el diagrama ERD BASE a partir de los formatos de registro
adjuntados para generar script de BD de la aplicaci�n a desarrollar.
Luego de definir la BD se proceder� a establecer los casos de uso para la aplicaci�n
con su respectiva secuencia de interacciones adjuntada por un prototipo de interfaz.
(diagramas de an�lisis y dise�o)

NOTA: FRAMEWORKS A UTILIZAR ANGULARJS 1.5.8 Y LARAVEL 5.4
TENER EN CUENTA AL GENERAR EL DIAGRAMA ER SE DEBEN SEGUIR CONVENCIONES DEL FRAMEWORK
LARAVEL:
- LLAVES PRIMARIAS DE TIPO INTEGER AUTOINCREMENTARLES CON NOMBRE id.
- CLAVES FORANEAS DEBEN SEGUIR LA CONVENCION nombre_tabla_id. Ejemplo: persona_id hace
referencia al campo id de la tabla persona.
- RELACIONES DE MUCHOS A MUCHOS EL NOMBRE DE LA TABLA INTERMEDIA DEBE FORMARSE ALFABETI
CAMENTE A PARTIR DEL RESTO DE TABLAS POR EJEMPLO: asumiendo que un usuario puede tener
varios roles y un rol pertenecer a varios usuarios el nombre de la tabla intermedia seria
rol_usuario. Esto claro se puede obviar en caso de que la tabla intermedia tenga un nombre
conceptual apropiado para la relacion intermedia.
- AVERIGUAR SOFT DELETES EN LARAVEL PUES SE USARA 3 CAMPOS DE TIPO TIMESTAMP PARA CADA TABLA
DE LA BD: created_at, updated_at y deleted_at.
- CAMPO NOMBRE PARA CADA TABLA DE MANTENIMIENTO (no usar nombre de campo descripcion a menos
que la tabla asi lo requiera y seria opcional o nullable).
