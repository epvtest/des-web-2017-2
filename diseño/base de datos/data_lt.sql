
-- TABLA lt_seguro
INSERT INTO lt_seguro VALUES('sis','Seguro Integral de Salud','Seguro social de salud, dirigido a personas en situaci�n de pobreza y pobreza extrema.',10);
INSERT INTO lt_seguro VALUES('essalud','EsSalud','Seguro social de salud, organismo p�blico descentralizado, con personer�a jur�dica de derecho p�blico interno, adscrito al Sector Trabajo y Promoci�n Social.',20);
INSERT INTO lt_seguro VALUES('segffaa','Seguros de las Fuerzas Armadas','Seguro social de salud dirigidos a las Fuerzas Armadas.',30);
INSERT INTO lt_seguro VALUES('saludpol','Fondo de Aseguramiento en Salud','Seguro social de salud dirigido a la Polic�a Nacional del Per�.',40);
INSERT INTO lt_seguro VALUES('rimac','R�mac Seguros','RIMAC Seguros es la empresa l�der del mercado asegurador peruano. Formamos parte de Breca, conglomerado empresarial peruano con presencia internacional y con m�s de cien a�os de existenc ia, fundado por la familia Brescia Cafferata.',50);
INSERT INTO lt_seguro VALUES('mapfre','MAPFRE Per�','Somos un equipo multinacional que trabaja para avanzar constantemente en el servicio y desarrollar la mejor relaci�n con nuestros clientes , distribuidores, proveedores, accionistas y sociedad.',60);
INSERT INTO lt_seguro VALUES('sanitas','Sanitas Per�','Aseguradora peruana que forma parte de Keralty, antes Sanita Internacional.',70);
INSERT INTO lt_seguro VALUES('pacifico','Pac�fico Seguros','Somos una empresa que pertenece al mercado asegurador peruano y cuyo objetivo principal es ayudar a nuestros clientes a gestionar sus riesgos y proteger aquello que m�s valoran y as� garantizar el logro de sus metas.',80);
INSERT INTO lt_seguro VALUES('otro','Otro','El paciente cuenta con un seguro de salud que no figura en la lista.',90);
INSERT INTO lt_seguro VALUES('ninguno','No tiene','El paciente no cuenta con ning�n seguro de salud.',100);

-- TABLA lt_tipo_atencion
INSERT INTO lt_tipo_atencion VALUES('controlreciennacido','Control del reci�n nacido','Control del reci�n nacido.',10);
INSERT INTO lt_tipo_atencion VALUES('crecimientodesarrollo','Crecimiento y desarrollo','Crecimiento y desarrollo.',20);
INSERT INTO lt_tipo_atencion VALUES('administrarvacunas','Administraci�n de vacunas','Administraci�n de vacunas.',30);
INSERT INTO lt_tipo_atencion VALUES('administrarmicronutrientes','Administraci�n de micronutrientes','Administraci�n de micronutrientes.',40);
INSERT INTO lt_tipo_atencion VALUES('evaluacionodontologica','Evaluaci�n odontol�gica','Evaluaci�n odontol�gica.',50);
INSERT INTO lt_tipo_atencion VALUES('visitafamiliar','Visita Familiar Integral','Visita Familiar Integral.',60);
INSERT INTO lt_tipo_atencion VALUES('sesionesdemostrativas','Sesiones demostrativas','Sesiones demostrativas.',70);
INSERT INTO lt_tipo_atencion VALUES('evaluaciongeneral','Evaluaci�n general, crecimiento y desarrollo','Evaluaci�n general, crecimiento y desarrollo.',80);
INSERT INTO lt_tipo_atencion VALUES('inmunizacion','Inmunizaciones','Inmunizaciones.',90);
INSERT INTO lt_tipo_atencion VALUES('evaluacionbucal','Evaluaci�n bucal','Evaluaci�n bucal.',100);
INSERT INTO lt_tipo_atencion VALUES('Iintervencionespreventivas','Intervenciones preventivas','Intervenciones preventivas.',110);
INSERT INTO lt_tipo_atencion VALUES('consejeriaintegral','Consejer�a integral','Consejer�a integral.',120);
INSERT INTO lt_tipo_atencion VALUES('visitadomiciliaria','Visita Domiciliaria','Visita domiciliaria',130);
INSERT INTO lt_tipo_atencion VALUES('temaseducacion','Temas Educativos','Temas educativos.',140);
INSERT INTO lt_tipo_atencion VALUES('atencionprioridadessanitarias','Atenci�n de prioridades sanitarias','Atenci�n de proridades sanitarias.',150);

-- TABLA lt_tipo_problema
INSERT INTO lt_tipo_problema VALUES('agudo','Enfermedad aguda','Aquella enfermedad que se presenta de manera s�bita, y puede desaparecer de la misma forma.',10);
INSERT INTO lt_tipo_problema VALUES('cronico','Enfermedad cr�nica','Afecciones de desarrollo lento y que se mantienen a lo largo del tiempo.',20);

-- TABLA lt_destino_paciente
INSERT INTO lt_destino_paciente VALUES('domicilio','Domicilio','Significa que el paciente ha sido derivado a su domicilio, ya sea para continuar su tratamiento, o porque �ste ha finalizado.',10);
INSERT INTO lt_destino_paciente VALUES('hospitalizacion','Hospitalizaci�n','El paciente es transferido al �rea de hospitalizaci�n, una vez superada la emergencia',20);
INSERT INTO lt_destino_paciente VALUES('transferencia','Transferencia','El paciente es transferido a otro establecimiento de salud, bajo una hoja de referencia',30);
INSERT INTO lt_destino_paciente VALUES('ignorado','Ignorado','El paciente ha fugado del centro de salud',40);

-- TABLA lt_condicion_paciente
INSERT INTO lt_condicion_paciente VALUES('vivo','Vivo','Paciente vivo',10);
INSERT INTO lt_condicion_paciente VALUES('fallecido','Fallecido','Paciente fallecido',20);
INSERT INTO lt_condicion_paciente VALUES('fuga','Fuga','Paciente que se ha fugado del establecimiento de salud.',30);

-- TABLA lt_condicion_alta
INSERT INTO lt_condicion_alta VALUES('curado','Curado','El paciente se encuentra en condiciones �ptimas de salud.',10);
INSERT INTO lt_condicion_alta VALUES('mejorado','Mejorado','El paciente no se encuentra totalmente recuperado, pero muestra mejoras respecto a c�mo ingres� al centro de salud.',20);
INSERT INTO lt_condicion_alta VALUES('estacionario','Estacionario','El paciente se encuentra en las mismas condiciones de salud que cuando ingres� al centro de salud.',30);
INSERT INTO lt_condicion_alta VALUES('empeorado','Empeorado','La salud del paciente se ha visto deteriorada desde que ingres� al centro de salud.',40);

-- TABLA lt_causa_ext_emergencia
INSERT INTO lt_causa_ext_emergencia VALUES('accidentetrabajo','Accidente de trabajo','La emergencia se ha producido dentro del lugar de trabajo.',10);
INSERT INTO lt_causa_ext_emergencia VALUES('accidentehogar','Accidente de hogar','La emergencia se ha producido en el hogar.',20);
INSERT INTO lt_causa_ext_emergencia VALUES('accidentetransitochofer','Accidente de tr�nsito - Chofer','Un accidente de tr�nsito, en donde el paciente es el que manejaba.',30);
INSERT INTO lt_causa_ext_emergencia VALUES('accidentetransitoocupante','Accidente de tr�nsito - Ocupante','Un accidente de tr�nsito, en donde el paciente era pasajero del veh�culo.',40);
INSERT INTO lt_causa_ext_emergencia VALUES('accidentetransitopeaton','Accidente de tr�nsito - Peat�n','Un accidente de tr�nsito, en donde el paciente es un externo al veh�culo.',50);
INSERT INTO lt_causa_ext_emergencia VALUES('agresion','Agresi�n','El paciente ha sido internado producto de una agresi�n.',60);
INSERT INTO lt_causa_ext_emergencia VALUES('autoinfligido','Autoinfligido','El paciente se ha hecho da�o a s� mismo.',70);
INSERT INTO lt_causa_ext_emergencia VALUES('desastrenatural','Desastre Natural','Ha sucedido un desastre natural.',80);
INSERT INTO lt_causa_ext_emergencia VALUES('otros','Otros','Otras causas que han detonado la emergencia y no se encuentran listadas.',90);

-- TABLA lt_anestesia
INSERT INTO lt_anestesia VALUES('local','Local','Entumece una peque�a parte del cuerpo. El paciente permanece despierto y alerta.',10);
INSERT INTO lt_anestesia VALUES('regional','Regional','Bloquea el dolor en un �rea del cuerpo, como un brazo o una pierna.',20);
INSERT INTO lt_anestesia VALUES('general','General','El paciente se torna inconsciente, no siente dolor y no recuerdan la intervenci�n quir�rgica.',30);

-- TABLA lt_distocico
INSERT INTO lt_distocico VALUES('mecanica','Distocia mec�nica','Las distocias �seas afectan a la disposici�n de los huesos de la pelvis, que�dificultan la salida de la cabeza del beb� por falta de espacio.',10);
INSERT INTO lt_distocico VALUES('hiperdinamica','Distocia hiperdin�mica','Actividad contr�ctil del �tero caracterizada por contracciones fuertes o muy frecuentes.',20);
INSERT INTO lt_distocico VALUES('hipodinamica','Distocia hipodin�mica','Actividad contr�ctil del �tero caracterizada por contracciones d�biles o poco frecuentes.',30);
INSERT INTO lt_distocico VALUES('incoordinacion','Incoordinaci�n uterina','Actividad contr�ctil no r�tmica del �tero.',40);
INSERT INTO lt_distocico VALUES('transversaloblicua','Presentaci�n fetal transversal u oblicua','El feto se presenta en forma transversal u oblicua.',50);
INSERT INTO lt_distocico VALUES('podalica','Presentaci�n fetal pod�lica','El feto se presenta en posici�n de nalgas.',60);

-- TABLA lt_nivel_educativo
INSERT INTO lt_nivel_educativo VALUES('ninguno','Ninguno','La persona no ha concluido ning�n nivel de estudios.',10);
INSERT INTO lt_nivel_educativo VALUES('inicial','Inicial','La persona ha concluido el nivel inicial.',20);
INSERT INTO lt_nivel_educativo VALUES('primaria','Primaria','La persona ha concluido el nivel primaria.',30);
INSERT INTO lt_nivel_educativo VALUES('secundaria','Secundaria','La persona ha concluido el nivel secundaria.',40);
INSERT INTO lt_nivel_educativo VALUES('superiornouniversitaria','Superior no universitaria','La persona ha concluido alguna carrera t�cnica.',50);
INSERT INTO lt_nivel_educativo VALUES('superioruniversitaria','Superior universitaria','La persona ha concluido alguna carrera universitaria.',60);
INSERT INTO lt_nivel_educativo VALUES('postgrado','Postgrado o similar','La persona posee estudios de maestr�a, doctorado o similares.',70);
INSERT INTO lt_nivel_educativo VALUES('analfabeto','Analfabeto','La persona no posee ning�n nivel de estudio.',80);

-- TABLA lt_parentesco_jefe_hogar
INSERT INTO lt_parentesco_jefe_hogar VALUES('jefe','Jefe','La persona es jefe del hogar.',10);
INSERT INTO lt_parentesco_jefe_hogar VALUES('conyuge','C�nyuge','La persona es c�nyuge del jefe de hogar.',20);
INSERT INTO lt_parentesco_jefe_hogar VALUES('hijo','Hijo(a)','La persona es hijo o hija del jefe de hogar.',30);
INSERT INTO lt_parentesco_jefe_hogar VALUES('nieto','Nieto(a)','La persona es nieto o nieta del jefe de hogar.',40);
INSERT INTO lt_parentesco_jefe_hogar VALUES('padre','Padre/Madre','La persona es padre o madre del jefe de hogar.',50);
INSERT INTO lt_parentesco_jefe_hogar VALUES('hermano','Hermano(a)','La persona es hermano o hermana del jefe de hogar.',60);
INSERT INTO lt_parentesco_jefe_hogar VALUES('yernonuera','Yerno / nuera','La persona es yerno o nuera del jefe de hogar.',70);
INSERT INTO lt_parentesco_jefe_hogar VALUES('abuelo','Abuelo(a)','La persona es abuelo o abuela del jefe de hogar.',80);
INSERT INTO lt_parentesco_jefe_hogar VALUES('suegro','Suegro(a)','La persona es suegro o suegra del jefe de hogar.',90);
INSERT INTO lt_parentesco_jefe_hogar VALUES('tio','T�o(a)','La persona es t�o o t�a del jefe de hogar.',100);
INSERT INTO lt_parentesco_jefe_hogar VALUES('sobrino','Sobrino(a)','La persona es sobrino o sobrina del jefe de hogar.',110);
INSERT INTO lt_parentesco_jefe_hogar VALUES('primo','Primo(a)','La persona es primo o prima del jefe de hogar.',120);
INSERT INTO lt_parentesco_jefe_hogar VALUES('cunhado','Cu�ado(a)','La persona es cu�ado o cu�ada del jefe de hogar.',130);
INSERT INTO lt_parentesco_jefe_hogar VALUES('otropariente','Otro pariente','La persona es otro pariente en relaci�n al jefe de hogar, cuya denominaci�n no figura en esta lista.',140);
INSERT INTO lt_parentesco_jefe_hogar VALUES('serviciodomestico','Servicio dom�stico','La persona presta servicio dom�stico al hogar.',150);
INSERT INTO lt_parentesco_jefe_hogar VALUES('hijoserviciodomestico','Hijo del servicio dom�stico','La persona es hijo o hija de qui�n presta servicio dom�stico al hogar.',160);
INSERT INTO lt_parentesco_jefe_hogar VALUES('nopariente','No pariente','La persona no tiene parentesco con el jefe de hogar.',170);

-- TABLA lt_rh
INSERT INTO lt_rh VALUES('positivo','Positivo','Factor Rh positivo.',10);
INSERT INTO lt_rh VALUES('negativo','Negativo','Factor Rh negativo.',20);

-- TABLA lt_grupo_sanguineo
INSERT INTO lt_grupo_sanguineo VALUES('a','Tipo A','Grupo sangu�neo del tipo A.',10);
INSERT INTO lt_grupo_sanguineo VALUES('b','Tipo B','Grupo sangu�neo del tipo B.',20);
INSERT INTO lt_grupo_sanguineo VALUES('ab','Tipo AB','Grupo sangu�neo del tipo AB.',30);
INSERT INTO lt_grupo_sanguineo VALUES('o','Tipo O','Grupo sangu�neo del tipo O.',40);

-- TABLA lt_ocupacion
INSERT INTO lt_ocupacion VALUES('trabajadordependiente','Trabajador dependiente (asalariado)','Trabajador dependiente (asalariado).',10);
INSERT INTO lt_ocupacion VALUES('trabajadorindependiente','Trabajador independiente','Trabajador independiente.',20);
INSERT INTO lt_ocupacion VALUES('empleador','Empleador','Empleador.',30);
INSERT INTO lt_ocupacion VALUES('serviciodomestico','Servicio dom�stico','Servicio dom�stico.',40);
INSERT INTO lt_ocupacion VALUES('buscandotrabajo','Buscando trabajo','Buscando trabajo.',50);
INSERT INTO lt_ocupacion VALUES('quehacereshogar','Quehaceres del hogar','Quehaceres del hogar.',60);
INSERT INTO lt_ocupacion VALUES('estudiante','Estudiante','Estudiante.',70);
INSERT INTO lt_ocupacion VALUES('jubilado','Jubilado','Jubilado.',80);
INSERT INTO lt_ocupacion VALUES('sinactividad','Sin actividad','Sin actividad.',90);

-- TABLA lt_sexo
INSERT INTO lt_sexo VALUES('masculino','Masculino','Masculino.',10);
INSERT INTO lt_sexo VALUES('femenino','Femenino','Femenino.',20);

-- TABLA lt_tipo_identificacion
INSERT INTO lt_tipo_identificacion VALUES('pasaporte','Pasaporte','Pasaporte',10);
INSERT INTO lt_tipo_identificacion VALUES('dni','DNI','Documento Nacional de Identidad.',20);
INSERT INTO lt_tipo_identificacion VALUES('ruc','RUC','Registro �nico de Contribuyente',30);
INSERT INTO lt_tipo_identificacion VALUES('carneextranjeria','Carn� de extranjer�a','Carn� de Extranjer�a',40);
INSERT INTO lt_tipo_identificacion VALUES('otro','Otro','Otro',50);

-- TABLA lt_tipo_diagnostico
INSERT INTO lt_tipo_diagnostico VALUES('p','P','P',10);
INSERT INTO lt_tipo_diagnostico VALUES('d','D','D',20);
INSERT INTO lt_tipo_diagnostico VALUES('r','R','R',30);

-- TABLA lt_combustible_cocina
INSERT INTO lt_combustible_cocina VALUES('electricidad','Electricidad','Electricidad',10);
INSERT INTO lt_combustible_cocina VALUES('gas','Gas','Gas',20);
INSERT INTO lt_combustible_cocina VALUES('kerosene','Kerosene','Kerosene',30);
INSERT INTO lt_combustible_cocina VALUES('carbon','Carb�n','Carb�n',40);
INSERT INTO lt_combustible_cocina VALUES('lenha','Le�a','Le�a',50);
INSERT INTO lt_combustible_cocina VALUES('otro','Otro','Otro',60);
INSERT INTO lt_combustible_cocina VALUES('nococina','No cocina','No cocina',70);

-- TABLA lt_riesgo_familiar
INSERT INTO lt_riesgo_familiar VALUES('bajo','Bajo','Riesgo familiar bajo',10);
INSERT INTO lt_riesgo_familiar VALUES('mediano','Mediano','Riesgo familiar mediano',20);
INSERT INTO lt_riesgo_familiar VALUES('alto','Alto','Riesgo familiar alto',30);

-- TABLA lt_categoria_socioeconomica
INSERT INTO lt_categoria_socioeconomica VALUES('nopobre','No Pobre','No pobre',10);
INSERT INTO lt_categoria_socioeconomica VALUES('pobre','Pobre No Extremo','Pobre no extremo',20);
INSERT INTO lt_categoria_socioeconomica VALUES('pobreextremo','Pobre Extremo','Pobre extremo',30);

-- TABLA lt_ambito
INSERT INTO lt_ambito VALUES('urbano','Urbano','�mbito urbano',10);
INSERT INTO lt_ambito VALUES('marginal','Urbano Marginal','�mbito urbano marginal',20);
INSERT INTO lt_ambito VALUES('rural','Rural','�mbito rural',30);

-- TABLA lt_disposicion_basura
INSERT INTO lt_disposicion_basura VALUES('campoabierto','A campo abierto','A campo abierto',10);
INSERT INTO lt_disposicion_basura VALUES('rioacequia','Al r�o o acequia','Al r�o o acequia',20);
INSERT INTO lt_disposicion_basura VALUES('pozo','En un pozo','En un pozo',30);
INSERT INTO lt_disposicion_basura VALUES('enterrarquemar','Se entierra o quema','Se entierra o quema',40);
INSERT INTO lt_disposicion_basura VALUES('carrorecolector','Carro recolector','Carro recolector',50);

-- TABLA lt_servicio_sanitario
INSERT INTO lt_servicio_sanitario VALUES('redpublicaint','Red p�blica dentro de la vivienda','Red p�blica dentro de la vivienda',10);
INSERT INTO lt_servicio_sanitario VALUES('redpublicaext','Red p�blica fuera de la vivienda','Red p�blica fuera de la vivienda',20);
INSERT INTO lt_servicio_sanitario VALUES('pozoseptico','Pozo s�ptico','Pozo s�ptico',30);
INSERT INTO lt_servicio_sanitario VALUES('pozociegonegro','Pozo ciego o negro','Pozo ciego o negro',40);
INSERT INTO lt_servicio_sanitario VALUES('notieneservicio','No tiene servicio higi�nico','No tiene servicio higi�nico',50);

-- TABLA lt_abastecimiento_agua
INSERT INTO lt_abastecimiento_agua VALUES('redpublicaint','Red p�blica dentro de la vivienda','Red p�blica dentro de la vivienda',10);
INSERT INTO lt_abastecimiento_agua VALUES('redpublicaext','Red p�blica fuera de la vivienda','Red p�blica fuera de la vivienda',20);
INSERT INTO lt_abastecimiento_agua VALUES('pozopilon','Pozo artesanal o pil�n','Pozo artesanal o pil�n',30);
INSERT INTO lt_abastecimiento_agua VALUES('otro','Otro','Otro',40);

-- TABLA lt_material_techo
INSERT INTO lt_material_techo VALUES('concretoarmado','Concreto armado','Concreto armado',10);
INSERT INTO lt_material_techo VALUES('madera','Madera','Madera',20);
INSERT INTO lt_material_techo VALUES('tejas','Tejas','Tejas',30);
INSERT INTO lt_material_techo VALUES('calaminafibra','Planchas de calamina, fibra','Planchas de calamina, fibra',40);
INSERT INTO lt_material_techo VALUES('canhaestera','Ca�a o estera con torta de barro','Ca�a o estera con torta de barro',50);
INSERT INTO lt_material_techo VALUES('pajahojas','Paja, hojas de palamera','Paja, hojas de palamera',60);
INSERT INTO lt_material_techo VALUES('otro','Otro','Otro',70);

-- TABLA lt_material_pisos
INSERT INTO lt_material_pisos VALUES('parquetmadera','Parqueto o madera pulida','Parqueto o madera pulida',10);
INSERT INTO lt_material_pisos VALUES('asfaltovinilo','L�minas asf�lticas, vin�licos','L�minas asf�lticas, vin�licos',20);
INSERT INTO lt_material_pisos VALUES('losetaterrazo','Losetas, terrazos o similares','Losetas, terrazos o similares',30);
INSERT INTO lt_material_pisos VALUES('madera','Madera (entablados)','Madera (entablados)',40);
INSERT INTO lt_material_pisos VALUES('cemento','Cemento','Cemento',50);
INSERT INTO lt_material_pisos VALUES('tierra','Tierra','Tierra',60);
INSERT INTO lt_material_pisos VALUES('otro','Otro','Otro',70);

-- TABLA lt_material_paredes
INSERT INTO lt_material_paredes VALUES('ladrillocemento','Ladrillo o bloque de cemento','Ladrillo o bloque de cemento',10);
INSERT INTO lt_material_paredes VALUES('adobetapia','Adobe o tapia','Adobe o tapia',20);
INSERT INTO lt_material_paredes VALUES('quincha','Quincha (ca�a con barro)','Quincha (ca�a con barro)',30);
INSERT INTO lt_material_paredes VALUES('piedrabarro','Piedra con barro','Piedra con barro',40);
INSERT INTO lt_material_paredes VALUES('madera','Madera','Madera',50);
INSERT INTO lt_material_paredes VALUES('estera','Estera','Estera',60);
INSERT INTO lt_material_paredes VALUES('otro','Otro','Otro',70);

-- TABLA lt_personas_dormitorio
INSERT INTO lt_personas_dormitorio VALUES('seismas','6 o m�s miembros','Seis o m�s miembros',10);
INSERT INTO lt_personas_dormitorio VALUES('cinco','5 miembros','Cinco miembros',20);
INSERT INTO lt_personas_dormitorio VALUES('cuatro','4 miembros','Cuatro miembros',30);
INSERT INTO lt_personas_dormitorio VALUES('tres','3 Miembros','Tres Miembros',40);
INSERT INTO lt_personas_dormitorio VALUES('unodos','1 o 2 miembros','Uno o dos miembros',50);

-- TABLA lt_ingreso_familiar
INSERT INTO lt_ingreso_familiar VALUES('menos400','Menor de 400','Menor de 400',10);
INSERT INTO lt_ingreso_familiar VALUES('de401a800','De 401 a 800','De 401 a 800',20);
INSERT INTO lt_ingreso_familiar VALUES('de801a1200','De 801 a 1200','De 801 a 1200',30);
INSERT INTO lt_ingreso_familiar VALUES('de1201a1600','De 1201 a 1600','De 1201 a 1600',40);
INSERT INTO lt_ingreso_familiar VALUES('de1601amas','M�s de 1600','M�s de 1600',50);

-- TABLA lt_ocupacion_jefe_familia
INSERT INTO lt_ocupacion_jefe_familia VALUES('desocupado','Desocupado','Desocupado',10);
INSERT INTO lt_ocupacion_jefe_familia VALUES('trabajoeventual','Trabajo eventual','Trabajo eventual',20);
INSERT INTO lt_ocupacion_jefe_familia VALUES('empleadosinseguro','Empleado sin seguro','Empleado sin seguro',30);
INSERT INTO lt_ocupacion_jefe_familia VALUES('contratadosinseguro','Contratado sin seguro','Contratado sin seguro',40);
INSERT INTO lt_ocupacion_jefe_familia VALUES('profesionalproductor','Profesional o productor','Profesional o productor',50);

-- TABLA lt_nivel_instruccion_madre
INSERT INTO lt_nivel_instruccion_madre VALUES('ninguna','Ninguna','Ninguna',10);
INSERT INTO lt_nivel_instruccion_madre VALUES('primaria','Primaria','Primaria',20);
INSERT INTO lt_nivel_instruccion_madre VALUES('secundaria','Secundaria','Secundaria',30);
INSERT INTO lt_nivel_instruccion_madre VALUES('tecnica','T�cnica','T�cnica',40);
INSERT INTO lt_nivel_instruccion_madre VALUES('profesional','Profesional','Profesional',50);

-- TABLA lt_energia_electrica
INSERT INTO lt_energia_electrica VALUES('noenergia','Sin energ�a','Sin energ�a',10);
INSERT INTO lt_energia_electrica VALUES('lamparanoelectrica','L�mpara no el�ctrica','L�mpara no el�ctrica',20);
INSERT INTO lt_energia_electrica VALUES('temporal','Temporal','Temporal',30);
INSERT INTO lt_energia_electrica VALUES('permanente','Permanente','Permanente',40);

-- TABLA lt_eliminacion_excretas
INSERT INTO lt_eliminacion_excretas VALUES('airelibre','Aire libre','Aire libre',10);
INSERT INTO lt_eliminacion_excretas VALUES('acequiacorral','Acequia, corral','Acequia, corral',20);
INSERT INTO lt_eliminacion_excretas VALUES('letrina','Letrina','Letrina',30);
INSERT INTO lt_eliminacion_excretas VALUES('banhopublico','Ba�o p�blico','Ba�o p�blico',40);
INSERT INTO lt_eliminacion_excretas VALUES('banhopropio','Ba�o propio','Ba�o propio',50);

-- TABLA lt_consumo_agua
INSERT INTO lt_consumo_agua VALUES('acequia','Acequia','Acequia',10);
INSERT INTO lt_consumo_agua VALUES('cisterna','Cisterna','Cisterna',20);
INSERT INTO lt_consumo_agua VALUES('pozo','Pozo','Pozo',30);
INSERT INTO lt_consumo_agua VALUES('redpublica','Red p�blica','Red p�blica',40);
INSERT INTO lt_consumo_agua VALUES('conexiondomicilio','Conexi�n domiciliaria','Conexi�n domiciliaria',50);

-- TABLA lt_tenencia_vivienda
INSERT INTO lt_tenencia_vivienda VALUES('alquiler','Alquiler','Alquiler',10);
INSERT INTO lt_tenencia_vivienda VALUES('cuidadoralojado','Cuidador o alojado','Cuidador o alojado',20);
INSERT INTO lt_tenencia_vivienda VALUES('plansocial','Plan social','Plan social',30);
INSERT INTO lt_tenencia_vivienda VALUES('alquilerventa','Alquiler venta','Alquiler venta',40);
INSERT INTO lt_tenencia_vivienda VALUES('propia','Propia','Propia',50);

-- TABLA lt_grupo_familiar
INSERT INTO lt_grupo_familiar VALUES('masnuevemiembros','M�s de 9 miembros','M�s de 9 miembros',10);
INSERT INTO lt_grupo_familiar VALUES('sieteaochomiembros','7 a 8 miembros','7 a 8 miembros',20);
INSERT INTO lt_grupo_familiar VALUES('cincoaseismiembros','5 a 6 miembros','5 a 6 miembros',30);
INSERT INTO lt_grupo_familiar VALUES('tresacuatromiembros','3 a 4 miembros','3 a 4 miembros',40);
INSERT INTO lt_grupo_familiar VALUES('unoadosmiembros','1 a 2 miembros','1 a 2 miembros',50);

-- TABLA lt_estado_civil
INSERT INTO lt_estado_civil VALUES('viudo','Viudo','Viudo',10);
INSERT INTO lt_estado_civil VALUES('solteroconfamilia','Soltero con familia','Soltero con familia',20);
INSERT INTO lt_estado_civil VALUES('divorciado','Divorciado','Divorciado',30);
INSERT INTO lt_estado_civil VALUES('unionestable','Uni�n estable','Uni�n estable',40);
INSERT INTO lt_estado_civil VALUES('solterosinfamilia','Soltero sin familia','Soltero sin familia',50);

-- TABLA lt_permanencia
INSERT INTO lt_permanencia VALUES('permanencia','Tiempo de permanencia','Tiempo de permanencia',10);
INSERT INTO lt_permanencia VALUES('transeunte','Transeunte','Transeunte',20);
INSERT INTO lt_permanencia VALUES('flotante','Flotante','Flotante',30);
INSERT INTO lt_permanencia VALUES('migrante','Migrante','Migrante',40);
INSERT INTO lt_permanencia VALUES('residente','Residente','Residente',50);

-- TABLA lt_programa_social
INSERT INTO lt_programa_social VALUES('vasoleche','Vaso de leche','Programa de vaso de leche',10);
INSERT INTO lt_programa_social VALUES('comedorpopular','Comedor popular','Comedor popular',20);
INSERT INTO lt_programa_social VALUES('pronaa','PRONAA','Programa Nacional de Asistencia Alimentaria',30);
INSERT INTO lt_programa_social VALUES('panfar','PANFAR','Programa en Alimentaci�n y Nutrici�n a familias en alto riesgo',40);
INSERT INTO lt_programa_social VALUES('pantbc','PANTBC','Programa de Alimentaci�n y Nutrici�n para el Paciente Ambulatorio con Tuberculosis y Familia',50);
INSERT INTO lt_programa_social VALUES('otro','Otro','Otro programa social',60);

-- TABLA lt_categoria
INSERT INTO lt_categoria VALUES('i1','Categor�a I1','Categor�a I1',10);
INSERT INTO lt_categoria VALUES('i2','Categor�a I2','Categor�a I2',20);
INSERT INTO lt_categoria VALUES('i3','Categor�a I3','Categor�a I3',30);
INSERT INTO lt_categoria VALUES('i4','Categor�a I4','Categor�a I4',40);
INSERT INTO lt_categoria VALUES('ii1','Categor�a II1','Categor�a II1',50);
INSERT INTO lt_categoria VALUES('ii2','Categor�a II2','Categor�a II2',60);
INSERT INTO lt_categoria VALUES('iie','Categor�a IIE','Categor�a IIE',70);
INSERT INTO lt_categoria VALUES('iii1','Categor�a III1','Categor�a III1',80);
INSERT INTO lt_categoria VALUES('iiie','Categor�a IIIE','Categor�a IIIE',90);
INSERT INTO lt_categoria VALUES('iii2','Categor�a III2','Categor�a III2',100);

-- TABLA lt_red_salud
INSERT INTO lt_red_salud VALUES('dirislimanorte','Direcci�n de Red Integrada de Salud - Lima Norte','Direcci�n de Red Integrada de Salud - Lima Norte',10);
INSERT INTO lt_red_salud VALUES('dirislimacentro','Direcci�n de Red Integrada de Salud - Lima Centro','Direcci�n de Red Integrada de Salud - Lima Centro',20);
INSERT INTO lt_red_salud VALUES('dirislimasur','Direcci�n de Red Integrada de Salud - Lima Sur','Direcci�n de Red Integrada de Salud - Lima Sur',30);
INSERT INTO lt_red_salud VALUES('dirislimaeste','Direcci�n de Red Integrada de Salud - Lima Este','Direcci�n de Red Integrada de Salud - Lima Este',40);
INSERT INTO lt_red_salud VALUES('diresaamazonas','Direcci�n Regional de Salud de Amazonas','Direcci�n Regional de Salud - Amazonas',50);
INSERT INTO lt_red_salud VALUES('disuresabagua','Direcci�n Sub Regional de Salud Bagua','Direcci�n Sub Regional de Salud Bagua',60);
INSERT INTO lt_red_salud VALUES('diresaancash','Direcci�n Regional de Salud de Ancash','Direcci�n Regional de Salud de Ancash',70);
INSERT INTO lt_red_salud VALUES('diresaapurimacabancay','Direcci�n Regional de Salud de Apurimac','Direcci�n Regional de Salud de Apurimac',80);
INSERT INTO lt_red_salud VALUES('diresaapurimacandahuaylas','Direcci�n Regional de Salud Apurimac 2 Andahuaylas','Direcci�n Regional de Salud Apurimac 2 Andahuaylas',90);
INSERT INTO lt_red_salud VALUES('diresaarequipa','Direcci�n Regional de Salud de Arequipa','Direcci�n Regional de Salud de Arequipa',100);
INSERT INTO lt_red_salud VALUES('diresaayacucho','Direcci�n Regional de Salud de Ayacucho','Direcci�n Regional de Salud de Ayacucho',110);
INSERT INTO lt_red_salud VALUES('diresacallao','Direcci�n Regional de Salud de Callao','Direcci�n Regional de Salud de Callao',120);
INSERT INTO lt_red_salud VALUES('diresacajamarca','Direcci�n Regional de Salud de Cajamarca','Direcci�n Regional de Salud de Cajamarca',130);
INSERT INTO lt_red_salud VALUES('disachota','Direcci�n de Salud Chota','Direcci�n de Salud Chota',140);
INSERT INTO lt_red_salud VALUES('disacutervo','Direcci�n de Salud Cutervo','Direcci�n de Salud Cutervo',150);
INSERT INTO lt_red_salud VALUES('disajaen','Direcci�n de Salud Ja�n','Direcci�n de Salud Ja�n',160);
INSERT INTO lt_red_salud VALUES('diresacusco','Direcci�n Regional de Salud de Cusco','Direcci�n Regional de Salud de Cusco',170);
INSERT INTO lt_red_salud VALUES('diresahuancavelica','Direcci�n Regional de Salud de Huancavelica','Direcci�n Regional de Salud de Huancavelica',180);
INSERT INTO lt_red_salud VALUES('diresahuanuco','Direcci�n Regional de Salud de Huanuco','Direcci�n Regional de Salud de Huanuco',190);
INSERT INTO lt_red_salud VALUES('diresaica','Direcci�n Regional de Salud de Ica','Direcci�n Regional de Salud de Ica',200);
INSERT INTO lt_red_salud VALUES('diresajunin','Direcci�n Regional de Salud de Junin','Direcci�n Regional de Salud de Junin',210);
INSERT INTO lt_red_salud VALUES('diresalalibertad','Direcci�n Regional de Salud de La Libertad','Direcci�n Regional de Salud de La Libertad',220);
INSERT INTO lt_red_salud VALUES('diresalambayeque','Direcci�n Regional de Salud de Lambayeque','Direcci�n Regional de Salud de Lambayeque',230);
INSERT INTO lt_red_salud VALUES('diresalima','Direcci�n Regional de Salud de Lima','Direcci�n Regional de Salud de Lima',240);
INSERT INTO lt_red_salud VALUES('diresaloreto','Direcci�n Regional de Salud de Loreto','Direcci�n Regional de Salud de Loreto',250);
INSERT INTO lt_red_salud VALUES('diresamadrededios','Direcci�n Regional de Salud de Madre De Dios','Direcci�n Regional de Salud de Madre De Dios',260);
INSERT INTO lt_red_salud VALUES('diresamoquegua','Direcci�n Regional de Salud de Moquegua','Direcci�n Regional de Salud de Moquegua',270);
INSERT INTO lt_red_salud VALUES('diresapasco','Direcci�n Regional de Salud de Pasco','Direcci�n Regional de Salud de Pasco',280);
INSERT INTO lt_red_salud VALUES('diresapiura','Direcci�n Regional de Salud de Piura','Direcci�n Regional de Salud de Piura',290);
INSERT INTO lt_red_salud VALUES('disuresacolonna','Direcci�n Sub Regional de Salud Luciano Castillo','Direcci�n Sub Regional de Salud Luciano Castillo Colonna',300);
INSERT INTO lt_red_salud VALUES('diresapuno','Direcci�n Regional de Salud de Puno','Direcci�n Regional de Salud de Puno',310);
INSERT INTO lt_red_salud VALUES('diresasanmartin','Direcci�n Regional de Salud de San Martin','Direcci�n Regional de Salud de San Martin',320);
INSERT INTO lt_red_salud VALUES('diresatacna','Direcci�n Regional de Salud de Tacna','Direcci�n Regional de Salud de Tacna',330);
INSERT INTO lt_red_salud VALUES('diresatumbes','Direcci�n Regional de Salud de Tumbes','Direcci�n Regional de Salud de Tumbes',340);
INSERT INTO lt_red_salud VALUES('diresaucayali','Direcci�n Regional de Salud de Ucayali','Direcci�n Regional de Salud de Ucayali',350);
