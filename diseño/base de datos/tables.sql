
CREATE TABLE lt_seguro (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_tipo_atencion (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_tipo_problema (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_destino_paciente (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_condicion_paciente (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_condicion_alta (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_causa_ext_emergencia (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_anestesia (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_distocico (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_nivel_educativo (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_parentesco_jefe_hogar (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_rh (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_grupo_sanguineo (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_ocupacion (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_sexo (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_tipo_identificacion (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_clave (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_tipo_diagnostico (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_combustible_cocina (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_riesgo_familiar (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_categoria_socioeconomica (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_ambito (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_disposicion_basura (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_servicio_sanitario (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_abastecimiento_agua (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_material_techo (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_material_pisos (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_material_paredes (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_personas_dormitorio (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_ingreso_familiar (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_ocupacion_jefe_familia (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_nivel_instruccion_madre (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_energia_electrica (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_eliminacion_excretas (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_consumo_agua (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_tenencia_vivienda (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_grupo_familiar (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_estado_civil (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_permanencia (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_programa_social (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE recien_nacido (
                id INT AUTO_INCREMENT NOT NULL,
                aspecto_general VARCHAR(100) NOT NULL,
                desarrollo VARCHAR(50),
                signos_vitales VARCHAR(50),
                piel VARCHAR(50),
                cabeza VARCHAR(50),
                boca VARCHAR(50),
                nariz VARCHAR(50),
                ojos VARCHAR(50),
                oidos VARCHAR(50),
                cabellos VARCHAR(50),
                torax VARCHAR(50),
                dorso VARCHAR(50),
                PRIMARY KEY (id)
);


CREATE TABLE nacimiento (
                id INT NOT NULL,
                hora TIME NOT NULL,
                peso NUMERIC(5,3) NOT NULL,
                talla NUMERIC(3,2) NOT NULL,
                cuna INT NOT NULL,
                fec_alta DATE NOT NULL,
                id_recien_nacido INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE antecedentes_patologias (
                id INT NOT NULL,
                antecedentes_madre VARCHAR(200),
                antecedentes_padre VARCHAR(200),
                antecedentes_abuelos VARCHAR(200),
                otros VARCHAR(200),
                id_recien_nacido INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE gestacion (
                id INT NOT NULL,
                ind_complicado BOOLEAN NOT NULL,
                fec_concepcion DATE NOT NULL,
                duracion_embarazo INT NOT NULL,
                patologias VARCHAR(200),
                id_recien_nacido INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE persona (
                id INT AUTO_INCREMENT NOT NULL,
                lt_tipo_identificacion VARCHAR(50) NOT NULL,
                numero_identificacion VARCHAR(20) NOT NULL,
                nombre VARCHAR(30) NOT NULL,
                ape_paterno VARCHAR(30) NOT NULL,
                ape_materno VARCHAR(30),
                ape_casada VARCHAR(30),
                lt_sexo VARCHAR(50) NOT NULL,
                lt_estado_civil VARCHAR(50) NOT NULL,
                fec_nacimiento DATE NOT NULL,
                telefono VARCHAR(15) NOT NULL,
                lt_ocupacion VARCHAR(50) NOT NULL,
                lt_grupo_sanguineo VARCHAR(50) NOT NULL,
                lt_rh VARCHAR(50) NOT NULL,
                lt_parentesco_jefe_hogar VARCHAR(50) NOT NULL,
                ind_victima_violacion_ddhh BOOLEAN NOT NULL,
                ind_provision_anticonceptivos BOOLEAN NOT NULL,
                lt_nivel_educativo VARCHAR(50) NOT NULL,
                centro_educativo VARCHAR(100),
                ingresos_mensuales INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE usuario (
                id INT NOT NULL,
                usuario VARCHAR(60) NOT NULL,
                clave VARCHAR(32) NOT NULL,
                id_persona INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE medico (
                id INT NOT NULL,
                numero_cmp CHAR(6) NOT NULL,
                fec_emision_licencia DATE NOT NULL,
                jubilado BOOLEAN NOT NULL,
                id_persona INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE parto (
                id INT NOT NULL,
                ind_eutocico BOOLEAN NOT NULL,
                duracion_periodo_expulsivo VARCHAR(50) NOT NULL,
                lt_distocico VARCHAR(50) NOT NULL,
                solucion_distocico VARCHAR(200),
                pre_parto VARCHAR(100) NOT NULL,
                lt_anestesia VARCHAR(50) NOT NULL,
                duracion_parto VARCHAR(50) NOT NULL,
                signos_sufrimiento_fetal VARCHAR(200),
                intervencion_medicacion VARCHAR(200),
                observacion VARCHAR(1000),
                id_obstetriz INT NOT NULL,
                id_medico INT NOT NULL,
                id_recien_nacido INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE paciente (
                id INT AUTO_INCREMENT NOT NULL,
                numero_seguro VARCHAR(30) NOT NULL,
                id_persona INT NOT NULL,
                lt_seguro VARCHAR(50) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE problema (
                id INT NOT NULL,
                lt_tipo_problema VARCHAR(50) NOT NULL,
                fecha DATE NOT NULL,
                ind_activo BOOLEAN NOT NULL,
                observacion VARCHAR(1000) NOT NULL,
                id_paciente INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE atencion_integral (
                id INT NOT NULL,
                lt_tipo_atencion VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                fecha DATE NOT NULL,
                lugar VARCHAR(100) NOT NULL,
                id_paciente INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE lt_red_salud (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE lt_categoria (
                codigo VARCHAR(50) NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                descripcion VARCHAR(1000) NOT NULL,
                prioridad INT NOT NULL,
                PRIMARY KEY (codigo)
);


CREATE TABLE establecimiento_salud (
                id INT AUTO_INCREMENT NOT NULL,
                nombre VARCHAR(50) NOT NULL,
                lt_categoria VARCHAR(50) NOT NULL,
                lt_red_salud VARCHAR(50) NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE ficha_familiar (
                id INT NOT NULL,
                numero INT NOT NULL,
                ind_visita_domiciliaria BOOLEAN NOT NULL,
                fec_verificacion DATE NOT NULL,
                id_trabajador INT NOT NULL,
                observaciones VARCHAR(1000) NOT NULL,
                id_establecimiento_salud INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE historia_clinica (
                id INT AUTO_INCREMENT NOT NULL,
                numero_hc CHAR(11) NOT NULL,
                ind_emergencia BOOLEAN NOT NULL,
                motivo_emergencia VARCHAR(100),
                lt_causa_ext_emergencia VARCHAR(50) NOT NULL,
                ind_referencia BOOLEAN NOT NULL,
                fec_referencia DATE,
                motivo_referencia VARCHAR(100),
                acompanhante VARCHAR(100) NOT NULL,
                id_ficha_familiar INT NOT NULL,
                id_paciente INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE consulta (
                id INT NOT NULL,
                fec_consulta DATE NOT NULL,
                motivo VARCHAR(100) NOT NULL,
                tiempo_enfermedad VARCHAR(100) NOT NULL,
                apetito VARCHAR(100) NOT NULL,
                sed VARCHAR(100) NOT NULL,
                suenho VARCHAR(100) NOT NULL,
                estado_animo VARCHAR(100) NOT NULL,
                orina VARCHAR(100) NOT NULL,
                deposiciones VARCHAR(100) NOT NULL,
                fec_proxima_cita DATE NOT NULL,
                observaciones VARCHAR(1000) NOT NULL,
                id_medico INT NOT NULL,
                id_historia_clinica INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE examen_auxiliar (
                id INT NOT NULL,
                descripcion VARCHAR(200) NOT NULL,
                fec_emision DATE NOT NULL,
                id_medico INT NOT NULL,
                id_consulta INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE examen_auxiliar_detalle (
                id INT AUTO_INCREMENT NOT NULL,
                lt_clave VARCHAR(50) NOT NULL,
                valor VARCHAR(50) NOT NULL,
                id_examen_auxiliar INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE tratamiento (
                id INT AUTO_INCREMENT NOT NULL,
                descripcion VARCHAR(200) NOT NULL,
                id_consulta INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE diagnostico (
                id INT AUTO_INCREMENT NOT NULL,
                lt_tipo_diagnostico VARCHAR(50) NOT NULL,
                descripcion VARCHAR(200) NOT NULL,
                codigo_cie_10 CHAR(4) NOT NULL,
                id_consulta INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE examen_fisico (
                id INT AUTO_INCREMENT NOT NULL,
                temperatura NUMERIC(4,2) NOT NULL,
                presion_arterial VARCHAR(10) NOT NULL,
                frecuencia_cardiaca INT NOT NULL,
                frecuencia_respiratoria INT NOT NULL,
                talla NUMERIC(3,2) NOT NULL,
                peso NUMERIC(5,2) NOT NULL,
                indice_masa_corporal NUMERIC(4,2) NOT NULL,
                id_consulta INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE alta (
                id INT AUTO_INCREMENT NOT NULL,
                lt_condicion_alta VARCHAR(50) NOT NULL,
                lt_condicion_paciente VARCHAR(50) NOT NULL,
                lt_destino_paciente VARCHAR(50) NOT NULL,
                id_establecimiento_destino INT NOT NULL,
                servicio_transferir VARCHAR(100),
                numero_cama INT NOT NULL,
                fecha DATE NOT NULL,
                hora TIME NOT NULL,
                id_historia_clinica INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE vivienda (
                id INT NOT NULL,
                departamento VARCHAR(20) NOT NULL,
                provincia VARCHAR(30) NOT NULL,
                distrito VARCHAR(30) NOT NULL,
                ubigeo CHAR(6) NOT NULL,
                centro_poblado VARCHAR(40) NOT NULL,
                direccion VARCHAR(100) NOT NULL,
                lt_ambito VARCHAR(50) NOT NULL,
                telefono VARCHAR(20) NOT NULL,
                lt_categoria_socioecon VARCHAR(50) NOT NULL,
                lt_riesgo_familiar VARCHAR(50) NOT NULL,
                id_ficha_familiar INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE tenencia_bienes (
                id INT NOT NULL,
                lt_combustible_cocina VARCHAR(50) NOT NULL,
                ind_radio BOOLEAN NOT NULL,
                ind_televisor BOOLEAN NOT NULL,
                ind_refrigeradora BOOLEAN NOT NULL,
                ind_cocina BOOLEAN NOT NULL,
                ind_telefono_fijo BOOLEAN NOT NULL,
                ind_telefono_celular BOOLEAN NOT NULL,
                ind_lavadora BOOLEAN NOT NULL,
                ind_computadora BOOLEAN NOT NULL,
                ind_horno_microondas BOOLEAN NOT NULL,
                ind_suministro_electrico BOOLEAN NOT NULL,
                suministro_electrico VARCHAR(15),
                consumo_electrico NUMERIC(6,2) NOT NULL,
                mes_periodo_consumo INT,
                anho_periodo_consumo INT,
                id_vivienda INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE adicional_vivienda (
                id INT NOT NULL,
                ind_perros BOOLEAN NOT NULL,
                ind_gatos BOOLEAN NOT NULL,
                ind_vacas BOOLEAN NOT NULL,
                ind_cabras BOOLEAN NOT NULL,
                ind_carnero BOOLEAN NOT NULL,
                ind_cerdo BOOLEAN NOT NULL,
                ind_aves BOOLEAN NOT NULL,
                ind_cuyes BOOLEAN NOT NULL,
                ind_chirimachas BOOLEAN NOT NULL,
                ind_caballos BOOLEAN NOT NULL,
                ind_burros BOOLEAN NOT NULL,
                ind_otros_animales BOOLEAN NOT NULL,
                lt_disposicion_basura VARCHAR(50) NOT NULL,
                id_vivienda INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE detalle_vivienda (
                id INT NOT NULL,
                lt_material_paredes VARCHAR(50) NOT NULL,
                otro_material_paredes VARCHAR(50),
                lt_material_pisos VARCHAR(50) NOT NULL,
                otro_material_pisos VARCHAR(50),
                lt_material_techo VARCHAR(50) NOT NULL,
                otro_material_techo VARCHAR(50),
                lt_abastecimiento_agua VARCHAR(50) NOT NULL,
                lt_servicio_sanitario VARCHAR(50) NOT NULL,
                habitaciones INT NOT NULL,
                id_vivienda INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE educacion_familiar (
                id INT NOT NULL,
                fec_nutricion_saludable DATE NOT NULL,
                fec_ambiente_saludable DATE NOT NULL,
                fec_cultura_paz DATE NOT NULL,
                fec_pautas_crianza DATE NOT NULL,
                fec_habilidades_vida DATE NOT NULL,
                fec_salud_sexual DATE NOT NULL,
                fec_seguridad_vial DATE NOT NULL,
                fec_prev_enf_preval DATE NOT NULL,
                id_ficha_familiar INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE detalle_socioeconomico (
                id INT NOT NULL,
                anho INT NOT NULL,
                lt_estado_civil VARCHAR(50) NOT NULL,
                lt_grupo_familiar VARCHAR(50) NOT NULL,
                lt_tenencia_vivienda VARCHAR(50) NOT NULL,
                lt_consumo_agua VARCHAR(50) NOT NULL,
                lt_eliminacion_excretas VARCHAR(50) NOT NULL,
                lt_energia_electrica VARCHAR(50) NOT NULL,
                lt_nivel_instruccion_madre VARCHAR(50) NOT NULL,
                lt_ocupacion_jefe_familia VARCHAR(50) NOT NULL,
                lt_ingreso_familiar VARCHAR(50) NOT NULL,
                lt_personas_dormitorio VARCHAR(50) NOT NULL,
                id_ficha_familiar INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE detalle_familia (
                id INT NOT NULL,
                lt_programa_social VARCHAR(50) NOT NULL,
                otro_programa_social VARCHAR(50),
                distrito_procedencia VARCHAR(30) NOT NULL,
                lt_permanencia VARCHAR(50) NOT NULL,
                observaciones VARCHAR(1000) NOT NULL,
                id_ficha_familiar INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE seguimiento (
                id INT NOT NULL,
                fec_visita DATE NOT NULL,
                nombre_visitador VARCHAR(100) NOT NULL,
                problemas_identificados VARCHAR(255) NOT NULL,
                recomendaciones VARCHAR(255) NOT NULL,
                ind_cumple_recomendaciones BOOLEAN NOT NULL,
                id_ficha_familiar INT NOT NULL,
                PRIMARY KEY (id)
);


CREATE TABLE indicador_riesgo (
                id INT NOT NULL,
                ind_hijos_sin_partida_nacimiento BOOLEAN NOT NULL,
                ind_ninhos_desercion_escolar BOOLEAN NOT NULL,
                ind_lactancia_exclusiva BOOLEAN NOT NULL,
                ind_planificacion_familiar BOOLEAN NOT NULL,
                ind_alimentacion_comp_deficiente BOOLEAN NOT NULL,
                ind_papanicolau BOOLEAN NOT NULL,
                ind_diabetes_hipertension BOOLEAN NOT NULL,
                ind_dependencia_funcional BOOLEAN NOT NULL,
                ind_gestante BOOLEAN NOT NULL,
                ind_parto_domiciliario BOOLEAN NOT NULL,
                ind_vacunas_completas BOOLEAN NOT NULL,
                ind_leishmaniasis BOOLEAN NOT NULL,
                ind_chagas BOOLEAN NOT NULL,
                ind_estres BOOLEAN NOT NULL,
                ind_depresion BOOLEAN NOT NULL,
                ind_abandono BOOLEAN NOT NULL,
                ind_violencia_familiar BOOLEAN NOT NULL,
                ind_discapacidad_evidente BOOLEAN NOT NULL,
                ind_madre_adolescente BOOLEAN NOT NULL,
                ind_desnutricion BOOLEAN NOT NULL,
                ind_tuberculosis BOOLEAN NOT NULL,
                ind_esquizofrenia BOOLEAN NOT NULL,
                ind_cancer BOOLEAN NOT NULL,
                ind_alcoholismo_drogadiccion BOOLEAN NOT NULL,
                ind_conducta_sexual_riesgo BOOLEAN NOT NULL,
                ind_delincuencia_pandillaje BOOLEAN NOT NULL,
                id_ficha_familiar INT NOT NULL,
                PRIMARY KEY (id)
);


ALTER TABLE paciente ADD CONSTRAINT lt_seguro_paciente_fk
FOREIGN KEY (lt_seguro)
REFERENCES lt_seguro (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE atencion_integral ADD CONSTRAINT lt_tipo_atencion_atencion_integral_fk
FOREIGN KEY (lt_tipo_atencion)
REFERENCES lt_tipo_atencion (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE problema ADD CONSTRAINT lt_tipo_problema_problema_fk
FOREIGN KEY (lt_tipo_problema)
REFERENCES lt_tipo_problema (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE alta ADD CONSTRAINT lt_destino_paciente_alta_fk
FOREIGN KEY (lt_destino_paciente)
REFERENCES lt_destino_paciente (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE alta ADD CONSTRAINT lt_condicion_paciente_alta_fk
FOREIGN KEY (lt_condicion_paciente)
REFERENCES lt_condicion_paciente (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE alta ADD CONSTRAINT lt_condicion_alta_alta_fk
FOREIGN KEY (lt_condicion_alta)
REFERENCES lt_condicion_alta (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE historia_clinica ADD CONSTRAINT lt_causa_ext_emergencia_historia_clinica_fk
FOREIGN KEY (lt_causa_ext_emergencia)
REFERENCES lt_causa_ext_emergencia (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE parto ADD CONSTRAINT lt_anestesia_parto_fk
FOREIGN KEY (lt_anestesia)
REFERENCES lt_anestesia (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE parto ADD CONSTRAINT lt_distocico_parto_fk
FOREIGN KEY (lt_distocico)
REFERENCES lt_distocico (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT lt_nivel_educativo_persona_fk
FOREIGN KEY (lt_nivel_educativo)
REFERENCES lt_nivel_educativo (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT lt_parentesco_jefe_hogar_persona_fk
FOREIGN KEY (lt_parentesco_jefe_hogar)
REFERENCES lt_parentesco_jefe_hogar (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT lt_rh_persona_fk
FOREIGN KEY (lt_rh)
REFERENCES lt_rh (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT lt_grupo_sanguineo_persona_fk
FOREIGN KEY (lt_grupo_sanguineo)
REFERENCES lt_grupo_sanguineo (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT lt_ocupacion_persona_fk
FOREIGN KEY (lt_ocupacion)
REFERENCES lt_ocupacion (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT lt_sexo_persona_fk
FOREIGN KEY (lt_sexo)
REFERENCES lt_sexo (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT lt_tipo_identificacion_persona_fk
FOREIGN KEY (lt_tipo_identificacion)
REFERENCES lt_tipo_identificacion (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE examen_auxiliar_detalle ADD CONSTRAINT lt_clave_examen_auxiliar_detalle_fk
FOREIGN KEY (lt_clave)
REFERENCES lt_clave (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE diagnostico ADD CONSTRAINT lt_tipo_diagnostico_diagnostico_fk
FOREIGN KEY (lt_tipo_diagnostico)
REFERENCES lt_tipo_diagnostico (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tenencia_bienes ADD CONSTRAINT lt_combustible_cocina_tenencia_bienes_fk
FOREIGN KEY (lt_combustible_cocina)
REFERENCES lt_combustible_cocina (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE vivienda ADD CONSTRAINT lt_riesgo_familiar_vivienda_fk
FOREIGN KEY (lt_riesgo_familiar)
REFERENCES lt_riesgo_familiar (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE vivienda ADD CONSTRAINT categoria_socioeconomica_vivienda_fk
FOREIGN KEY (lt_categoria_socioecon)
REFERENCES lt_categoria_socioeconomica (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE vivienda ADD CONSTRAINT lt_ambito_vivienda_fk
FOREIGN KEY (lt_ambito)
REFERENCES lt_ambito (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE adicional_vivienda ADD CONSTRAINT lt_disposicion_basura_adicional_vivienda_fk
FOREIGN KEY (lt_disposicion_basura)
REFERENCES lt_disposicion_basura (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_vivienda ADD CONSTRAINT lt_servicio_sanitario_detalle_vivienda_fk
FOREIGN KEY (lt_servicio_sanitario)
REFERENCES lt_servicio_sanitario (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_vivienda ADD CONSTRAINT lt_abastecimiento_agua_detalle_vivienda_fk
FOREIGN KEY (lt_abastecimiento_agua)
REFERENCES lt_abastecimiento_agua (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_vivienda ADD CONSTRAINT lt_material_techo_detalle_vivienda_fk
FOREIGN KEY (lt_material_techo)
REFERENCES lt_material_techo (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_vivienda ADD CONSTRAINT lt_material_pisos_detalle_vivienda_fk
FOREIGN KEY (lt_material_pisos)
REFERENCES lt_material_pisos (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_vivienda ADD CONSTRAINT lt_material_paredes_detalle_vivienda_fk
FOREIGN KEY (lt_material_paredes)
REFERENCES lt_material_paredes (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_socioeconomico ADD CONSTRAINT lt_personas_dormitorio_detalle_socioeconomico_fk
FOREIGN KEY (lt_personas_dormitorio)
REFERENCES lt_personas_dormitorio (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_socioeconomico ADD CONSTRAINT lt_ingreso_familiar_detalle_socioeconomico_fk
FOREIGN KEY (lt_ingreso_familiar)
REFERENCES lt_ingreso_familiar (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_socioeconomico ADD CONSTRAINT lt_ocupacion_jefe_familia_detalle_socioeconomico_fk
FOREIGN KEY (lt_ocupacion_jefe_familia)
REFERENCES lt_ocupacion_jefe_familia (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_socioeconomico ADD CONSTRAINT lt_nivel_instruccion_madre_detalle_socioeconomico_fk
FOREIGN KEY (lt_nivel_instruccion_madre)
REFERENCES lt_nivel_instruccion_madre (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_socioeconomico ADD CONSTRAINT lt_energia_electrica_detalle_socioeconomico_fk
FOREIGN KEY (lt_energia_electrica)
REFERENCES lt_energia_electrica (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_socioeconomico ADD CONSTRAINT lt_eliminacion_excretas_detalle_socioeconomico_fk
FOREIGN KEY (lt_eliminacion_excretas)
REFERENCES lt_eliminacion_excretas (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_socioeconomico ADD CONSTRAINT lt_consumo_agua_detalle_socioeconomico_fk
FOREIGN KEY (lt_consumo_agua)
REFERENCES lt_consumo_agua (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_socioeconomico ADD CONSTRAINT lt_tenencia_vivienda_detalle_socioeconomico_fk
FOREIGN KEY (lt_tenencia_vivienda)
REFERENCES lt_tenencia_vivienda (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_socioeconomico ADD CONSTRAINT lt_grupo_familiar_detalle_socioeconomico_fk
FOREIGN KEY (lt_grupo_familiar)
REFERENCES lt_grupo_familiar (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_socioeconomico ADD CONSTRAINT lt_estado_civil_detalle_socioeconomico_fk
FOREIGN KEY (lt_estado_civil)
REFERENCES lt_estado_civil (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE persona ADD CONSTRAINT lt_estado_civil_persona_fk
FOREIGN KEY (lt_estado_civil)
REFERENCES lt_estado_civil (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_familia ADD CONSTRAINT lt_permanencia_detalle_familia_fk
FOREIGN KEY (lt_permanencia)
REFERENCES lt_permanencia (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_familia ADD CONSTRAINT lt_programa_social_detalle_familia_fk
FOREIGN KEY (lt_programa_social)
REFERENCES lt_programa_social (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE parto ADD CONSTRAINT recien_nacido_parto_fk
FOREIGN KEY (id_recien_nacido)
REFERENCES recien_nacido (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE gestacion ADD CONSTRAINT recien_nacido_gestacion_fk
FOREIGN KEY (id_recien_nacido)
REFERENCES recien_nacido (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE antecedentes_patologias ADD CONSTRAINT recien_nacido_antecedentes_patologias_fk
FOREIGN KEY (id_recien_nacido)
REFERENCES recien_nacido (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE nacimiento ADD CONSTRAINT recien_nacido_nacimiento_fk
FOREIGN KEY (id_recien_nacido)
REFERENCES recien_nacido (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE paciente ADD CONSTRAINT persona_paciente_fk
FOREIGN KEY (id_persona)
REFERENCES persona (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE medico ADD CONSTRAINT persona_medico_fk
FOREIGN KEY (id_persona)
REFERENCES persona (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE usuario ADD CONSTRAINT persona_usuario_fk
FOREIGN KEY (id_persona)
REFERENCES persona (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE consulta ADD CONSTRAINT medico_consulta_fk
FOREIGN KEY (id_medico)
REFERENCES medico (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE examen_auxiliar ADD CONSTRAINT medico_examen_auxiliar_fk
FOREIGN KEY (id)
REFERENCES medico (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE parto ADD CONSTRAINT medico_parto_fk
FOREIGN KEY (id_medico)
REFERENCES medico (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE parto ADD CONSTRAINT medico_parto_fk1
FOREIGN KEY (id_obstetriz)
REFERENCES medico (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE historia_clinica ADD CONSTRAINT paciente_historia_clinica_fk
FOREIGN KEY (id_paciente)
REFERENCES paciente (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE atencion_integral ADD CONSTRAINT paciente_atencion_integral_fk
FOREIGN KEY (id_paciente)
REFERENCES paciente (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE problema ADD CONSTRAINT paciente_problema_fk
FOREIGN KEY (id_paciente)
REFERENCES paciente (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE establecimiento_salud ADD CONSTRAINT red_salud_establecimiento_salud_fk
FOREIGN KEY (lt_red_salud)
REFERENCES lt_red_salud (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE establecimiento_salud ADD CONSTRAINT categoria_establecimiento_salud_fk
FOREIGN KEY (lt_categoria)
REFERENCES lt_categoria (codigo)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE ficha_familiar ADD CONSTRAINT establecimiento_salud_ficha_familiar_fk
FOREIGN KEY (id_establecimiento_salud)
REFERENCES establecimiento_salud (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE alta ADD CONSTRAINT establecimiento_salud_alta_fk
FOREIGN KEY (id_establecimiento_destino)
REFERENCES establecimiento_salud (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE indicador_riesgo ADD CONSTRAINT ficha_familiar_indicador_riesgo_fk
FOREIGN KEY (id_ficha_familiar)
REFERENCES ficha_familiar (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE seguimiento ADD CONSTRAINT ficha_familiar_seguimiento_fk
FOREIGN KEY (id_ficha_familiar)
REFERENCES ficha_familiar (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_familia ADD CONSTRAINT ficha_familiar_detalle_familia_fk
FOREIGN KEY (id_ficha_familiar)
REFERENCES ficha_familiar (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_socioeconomico ADD CONSTRAINT ficha_familiar_detalle_socioeconomico_fk
FOREIGN KEY (id_ficha_familiar)
REFERENCES ficha_familiar (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE educacion_familiar ADD CONSTRAINT ficha_familiar_educacion_familiar_fk
FOREIGN KEY (id_ficha_familiar)
REFERENCES ficha_familiar (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE vivienda ADD CONSTRAINT ficha_familiar_vivienda_fk
FOREIGN KEY (id_ficha_familiar)
REFERENCES ficha_familiar (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE historia_clinica ADD CONSTRAINT ficha_familiar_historia_clinica_fk
FOREIGN KEY (id_ficha_familiar)
REFERENCES ficha_familiar (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE alta ADD CONSTRAINT historia_clinica_alta_fk
FOREIGN KEY (id_historia_clinica)
REFERENCES historia_clinica (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE consulta ADD CONSTRAINT historia_clinica_consulta_fk
FOREIGN KEY (id_historia_clinica)
REFERENCES historia_clinica (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE examen_fisico ADD CONSTRAINT consulta_examen_fisico_fk
FOREIGN KEY (id_consulta)
REFERENCES consulta (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE diagnostico ADD CONSTRAINT consulta_diagnostico_fk
FOREIGN KEY (id_consulta)
REFERENCES consulta (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tratamiento ADD CONSTRAINT consulta_tratamiento_fk
FOREIGN KEY (id_consulta)
REFERENCES consulta (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE examen_auxiliar ADD CONSTRAINT consulta_examen_auxiliar_fk
FOREIGN KEY (id_consulta)
REFERENCES consulta (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE examen_auxiliar_detalle ADD CONSTRAINT examen_auxiliar_examen_auxiliar_detalle_fk
FOREIGN KEY (id_examen_auxiliar)
REFERENCES examen_auxiliar (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE detalle_vivienda ADD CONSTRAINT vivienda_detalle_vivienda_fk
FOREIGN KEY (id_vivienda)
REFERENCES vivienda (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE adicional_vivienda ADD CONSTRAINT vivienda_adicional_vivienda_fk
FOREIGN KEY (id_vivienda)
REFERENCES vivienda (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE tenencia_bienes ADD CONSTRAINT vivienda_tenencia_bienes_fk
FOREIGN KEY (id_vivienda)
REFERENCES vivienda (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION;
